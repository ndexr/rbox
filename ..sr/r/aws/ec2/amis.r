#' @export
ui_amis <- function(id = "amis") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  tags$div(
    class = "row",
    tags$div(
      class = "col-12",
      shiny$uiOutput(ns("ui"))
    ),
    shiny$uiOutput(ns("images"))
  )
}

#' @export
server_amis <- function(id = "amis", ns_common_store_user, credentials) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / .. / connections / redis)
    box::use(.. / .. / connections / postgres)
    box::use(.. / client)
    box::use(. / instance_manage)
    box::use(purrr)
    box::use(dplyr)
    box::use(stats)
    box::use(. / amis)
  }




  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      ec2boto <- client$client("ec2", ns_common_store_user = ns_common_store_user)
      instances <- instance_manage$describe_instances(
        ns_common_store_user = ns_common_store_user,
        state_keep = c("running", "stopping", "starting", "stopped", "pending")
      )
      if (nrow(instances)) {
        output$ui <- shiny$renderUI({
          box::use(jsonlite)
          tags$div(
            tags$table(
              class = "table",
              tags$thead(
                tags$tr(
                  tags$th(
                    scope = "col", "Key Name"
                  ),
                  tags$th(
                    scope = "col", "Group Name"
                  ),
                  tags$th(
                    scope = "col", "Public IP Address"
                  ),
                  tags$th(
                    scope = "col", "State"
                  ),
                  tags$th(
                    scope = "col", "Backup"
                  )
                ),
                purrr$map(
                  split(instances, 1:nrow(instances)),
                  function(x) {
                    idd <- uuid::UUIDgenerate()
                    out <- amis$ui_ami_instance(ns(idd))
                    amis$server_ami_instance(idd, x, ns_common_store_user, credentials)
                    out
                  }
                )
              )
            ),
            tags$div(
              class = "row",
              tags$div(
                class = "col-12 d-flex justify-content-end my-2",
                shiny$actionButton(ns("refreshImages"), "Refresh", class = "btn-primary")
              )
            )
          )
        })

        output$images <- shiny$renderUI({
          input$refreshImages
          ec2boto <- client$client("ec2", ns_common_store_user = ns_common_store_user)
          box::use(dplyr)
          con <- postgres$table_get("amis") |> dplyr$filter(
            email %in% credentials$email
          )

          mainTable <- tags$table(
            class = "table",
            tags$thead(
              tags$tr(
                tags$th(
                  scope = "col", "Name"
                ),
                tags$th(
                  scope = "col", "Description"
                ),
                tags$th(
                  scope = "col", "Image Id"
                ),
                tags$th(
                  scope = "col", "Created At"
                ),
                tags$th(
                  scope = "col", "Delete"
                )
              ),
              purrr$map(
                split(con, 1:nrow(con)),
                function(x) {
                  idd <- uuid::UUIDgenerate()
                  out <- amis$ui_ami_manage(ns(idd))
                  amis$server_ami_manage(idd, x, ns_common_store_user, credentials)
                  out
                }
              )
            )
          )

          if (nrow(con) == 0) {
            tags$div(
              "Images are displayed here when they exist."
            )
          } else {
            mainTable
          }
        })
      } else {
        output$ui <- shiny$renderUI({
          tags$p(
            "When you launch a server, you will be able to clone it here."
          )
        })
      }
    }
  )
}


#' @export
ui_ami_instance <- function(id = "ami_instance") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"), container = function(...) tags$tr(...))
}

#' @export
server_ami_instance <- function(id = "ami_instance", instance, ns_common_store_user, credentials) {
  box::use(shiny = shiny[tags])
  box::use(.. / .. / connections / postgres)
  box::use(.. / .. / inputs / inputs)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        box::use(jsonlite)
        SecurityGroups <- jsonlite$fromJSON(instance$SecurityGroups)

        shiny$tagList(
          tags$td(instance$KeyName),
          tags$td(SecurityGroups$GroupName),
          tags$td(instance$PublicIpAddress),
          tags$td(instance$state),
          tags$td(
            shiny$actionButton(ns("backup"), "Run", class = "btn-info")
          )
        )
      })


      shiny$observeEvent(
        input$backup,
        {
          shiny$showModal(
            inputs$modalDialog(
              tags$div(
                class = "container",
                shiny$textInput(ns("imageName"), "Image Name"),
                shiny$textInput(ns("description"), "Description", "ndexr backup"),
                shiny$actionButton(ns("createImage"), "Create Image", class = "btn-primary")
              )
            )
          )
        }
      )




      shiny$observeEvent(
        input$createImage,
        {
          shiny$removeModal()

          tryCatch(
            {
              box::use(.. / client)
              ec2boto <- client$client("ec2", ns_common_store_user = ns_common_store_user)
              # ns_common_store_user <- shiny::NS('drennanfreddy@gmail.com')
              imageData <- ec2boto$create_image(
                InstanceId = instance$InstanceId,
                Name = input$imageName,
                Description = input$description,
                NoReboot = TRUE
              )

              amis <- data.frame(
                CreatedFromId = instance$ImageId,
                imageid = imageData$ImageId,
                Name = input$imageName,
                description = input$description,
                email = credentials$email,
                datetime_created = Sys.time()
              )
              # postgres$table_drop('amis')
              postgres$table_create_or_upsert(amis, where_cols = "imageid")
              shiny$showNotification("Success!")
            },
            error = function(err) {
              shiny$showModal(
                inputs$modalDialog(
                  tags$pre(as.character(err))
                )
              )
            }
          )
        }
      )
    }
  )
}

#' @export
ui_ami_manage <- function(id = "ami_manage") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"), container = function(...) tags$tr(...))
}

#' @export
server_ami_manage <- function(id = "ami_manage", ami, ns_common_store_user, credentials) {
  box::use(shiny = shiny[tags])
  box::use(jsonlite)
  box::use(.. / .. / connections / postgres)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        shiny$tagList(
          tags$td(ami$Name),
          tags$td(ami$description),
          tags$td(ami$imageid),
          tags$td(ami$datetime_created),
          tags$td(
            shiny$actionButton(ns("delete"), "Delete", class = "btn-warning")
          )
        )
      })

      shiny$observeEvent(
        input$delete,
        {
          box::use(.. / client)
          ec2boto <- client$client("ec2", ns_common_store_user = ns_common_store_user)
          ec2boto$deregister_image(ImageId = ami$imageid)
          postgres$tables_row_remove(
            where_cols = "ImageId",
            id = ami$imageid,
            table = "amis"
          )
          shiny$showNotification("Removed")
        }
      )
    }
  )
}
