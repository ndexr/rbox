#' #' @export
#' ui_s3 <- function(id = "s3", ns_common_store_user) {
#'   box::use(shiny = shiny[tags, HTML])
#'   box::use(shinyjs)
#'   ns <- shiny$NS(id)
#'
#'
#' }

#' #' @export
#' server_s3 <- function(id = "s3", ns_common_store_user = NULL, LocationConstraint = "us-east-2") {
#'
#'   # box::use(.. / .. / state / setDefault[setDefault])
#'
#'   shiny$moduleServer(
#'     id,
#'     function(input, output, session) {
#'       ns <- session$ns
#'
#'
#'   )
#' }
#'

#' @export
s3_upload_proj <- function(bucketname = "ndexrapp",
                           from,
                           to,
                           ns_common_store_user = NULL) {
  {
    box::use(fs)
    box::use(shiny)
    box::use(.. / .. / modal / modal)
    box::use(.. / client)
    box::use(glue[glue])
    box::use(readr)
  }
  s3 <- client$client("s3", ns_common_store_user = ns_common_store_user)

  # con

  # shiny$showNotification('s3 upload off')
  shiny$withProgress(
    message = "Uploading files to your S3 bucket",
    detail = "This will take a moment.",
    value = 0,
    {
      for (i in seq_along(from)) {
        s3$put_object(
          ACL = "public-read",
          Body = readr$read_file(from[i]),
          Bucket = bucketname,
          Key = to[i]
        )
        shiny$incProgress(1 / length(from))
        #
        shiny$setProgress(detail = as.character(to[i]))
      }
    }
  )
}

#' @export
s3_upload_file <- function(bucketname = "ndexrapp",
                           from,
                           to,
                           ns_common_store_user = NULL) {
  {
    box::use(fs)
    box::use(shiny)
    box::use(.. / .. / modal / modal)
    box::use(.. / client)
    box::use(glue[glue])
    box::use(readr)
  }
  s3 <- client$client("s3", ns_common_store_user = ns_common_store_user)

  con

  s3$put_object(
    ACL = "public-read",
    Body = readr$read_file(from),
    Bucket = bucketname,
    Key = to
  )
}


#' @export
s3_download_file <- function(bucketname = "ndexrapp",
                             from,
                             to,
                             ns_common_store_user = NULL) {
  {
    box::use(fs)
    box::use(shiny)
    box::use(.. / .. / modal / modal)
    box::use(.. / client)
    box::use(glue[glue])
    box::use(purrr)
    box::use(readr)
  }

  s3 <- client$client("s3", ns_common_store_user = ns_common_store_user)

  objects <- s3$list_objects(Bucket = bucketname, Prefix = "ssh")

  out <- s3$get_object(
    ACL = "public-read",
    Bucket = bucketname,
    Key = to
  )
}


#' @export
s3_list_objects <- function(bucketname, ns_common_store_user = NULL) {
  {
    box::use(fs)
    box::use(shiny)
    box::use(purrr)
    box::use(.. / client)
  }

  s3 <- client$client("s3", ns_common_store_user = ns_common_store_user)

  objects <- s3$list_objects(Bucket = bucketname, Prefix = "ssh")

  out <- s3$list_objects(
    Bucket = bucketname
  )

  purrr$map_dfr(
    out$Contents,
    function(x) {
      data.frame(
        Key = x$Key,
        LastModified = as.character(x$LastModified)
      )
    }
  )
}
