#' @export
ui_newsletter <- function(id = "newsletter") {
  box::use(shiny = shiny[tags, HTML])
  box::use(.. / inputs / inputs)
  ns <- shiny$NS(id)

  tags$div(
    class = "row",
    inputs$card_collapse(
      ns,
      title = "Newsletter",
      body = shiny$uiOutput(ns("newsletter")),
      collapsed = TRUE
    )
  )
}

#' @export
server_newsletter <- function(id = "newsletter") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(. / console_users)
    box::use(.. / connections / postgres)
    box::use(.. / connections / redis)
    box::use(.. / connections / ssh)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns



      output$newsletter <- shiny$renderUI({
        newsletter <- postgres$table_get("newsletter")

        tags$div(
          class = "container",
          tags$table(
            class = "table table-striped",
            tags$thead(
              tags$tr(
                tags$th(
                  scope = "col",
                  "email"
                ),
                tags$th(
                  scope = "col",
                  "info"
                )
              )
            ),
            tags$tbody(
              purrr::map(
                split(newsletter, 1:nrow(newsletter)), function(x) {
                  tags$tr(
                    tags$td(x$email),
                    tags$td(x$info)
                  )
                }
              )
            )
          )
        )
      })
    }
  )
}
