#' @export
ui_homepage <- function(id) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(. / head / head)
  }
  ns <- shiny$NS(id)
  tags$div(
    shiny$uiOutput(ns("mainUI"))
  )
}



#' @export
server_homepage <- function(id) {
  {
    box::use(shiny = shiny[withTags, tags])
    # box::use(. / contact / form_contact)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns



      header_1 <- function() {
        tags$figure(
          class = "text-end",
          tags$blockquote(
            class = "blockquote",
            tags$p(
              "so what is ndexr?"
            )
          ),
          tags$figcaption(
            class = "blockquote-footer",
            "me",
            tags$cite(
              title = "Source Title",
              "irl"
            )
          )
        )
      }


      # form_contact$server_form_contact("form_contact")
    }
  )
}
