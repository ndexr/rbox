#' @export
ui_management <- function(id = "management") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(. / console_users)
    box::use(. / newsletter)
    box::use(. / app_session_start)
    box::use(. / waitlist)
    box::use(glue)
    box::use(.. / aws / costs)
    box::use(.. / users / ip_address)
    box::use(.. / ticketing / ticketing)
    box::use(.. / aws / settings / settings)
    box::use(.. / aws / secrets / secrets)
    box::use(.. / aws / costs)
    box::use(.. / aws / secrets / user_secrets)
    box::use(.. / gpt / gpt)
  }


  ns <- shiny$NS(id)
  box::use(.. / payments / manage_payments)

  tags$div(
    class = "rounded my-2",
    shiny$uiOutput(ns("paymentmanagement")),
    shiny$uiOutput(ns("uiaccountmanagement")),
    shiny$uiOutput(ns("uimanagement"))
  )
}

#' @export
server_management <- function(id = "management", login, admin = FALSE, live = getOption("docker", FALSE)) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(. / console_users)
    box::use(. / newsletter)
    box::use(. / app_session_start)
    box::use(. / waitlist)
    box::use(glue)
    box::use(.. / aws / costs)
    box::use(.. / gpt / gpt)
    box::use(.. / aws / secrets / secrets)
    box::use(.. / aws / costs)
    box::use(.. / ticketing / ticketing)
    box::use(.. / payments / manage_payments)
    box::use(.. / usaa / import)
  }



  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      ns_common_store_user <- shiny$NS(login$email)

      output$paymentmanagement <- shiny$renderUI({
        uuid <- uuid::UUIDgenerate()
        out <- manage_payments$ui_manage_payments(ns(uuid))
        manage_payments$server_manage_payments(uuid, login$email, live = live)

        out
      })


      shiny$observe({
        has_secret <- secrets$secret_exists(ns_common_store_user)

        if (!has_secret) {
          shiny$invalidateLater(5000)
          shiny$showNotification("Tool Panel Waiting for AWS login.")
        } else {
          shiny$showNotification("Loading Tool Panel")
          output$uiaccountmanagement <- shiny$renderUI({
            out <- tags$div(
              tags$h5("Tools"),
              tags$div(class = "p-1 bg-secondary rounded"),
              class = glue$glue("mb-3"),
              costs$ui_aws_costs(ns("aws_costs")),
              ticketing$ui_ticketing(ns("ticketingUser"), admin = FALSE)
            )
            costs$server_aws_costs("aws_costs", ns_common_store_user)
            ticketing$server_ticketing("ticketingUser")

            out
          })

          output$uimanagement <- shiny$renderUI({
            out <-
              tags$div(
                class = "row",
                {
                  if (admin) {
                    out <- tags$div(
                      tags$h5("Administrator Panel"),
                      tags$div(class = "p-1 bg-secondary rounded"),
                      class = glue$glue("col-12 mb-3"),
                      console_users$ui_console_users(ns("console_users")),
                      gpt$ui_gpt(ns("gpt")),
                      app_session_start$ui_app_session_start(ns("app_session_start")),
                      ticketing$ui_ticketing(ns("ticketingAdmin"), admin = TRUE),
                      import$ui_amounts(ns("amounts"))
                    )
                    gpt$server_gpt("gpt", ns_common_store_user)
                    console_users$server_console_users("console_users")
                    ticketing$server_ticketing("ticketingAdmin")
                    app_session_start$server_app_session_start("app_session_start")
                    out
                  }
                }
              )

            import$server_amounts("amounts")




            out
          })
        }
      })
    }
  )
}
