#' @export
ui_team <- function(original_class = "col-lg-4 col-sm-6 mb-4") {
  box::use(shiny = shiny[withTags])
  withTags(
    section(
      class = "page-section bg-light", id = "team",
      div(
        class = "",
        div(
          class = "text-center",
          h2(class = "section-heading text-uppercase", "our team"),
          # h3(class = "section-subheading text-muted", "currently looking for ")
        ),
        div(
          class = "row",
          div(
            class = original_class,
            div(
              class = "team-member",
              img(class = "mx-auto rounded-circle", src = "homepage/img/team/1.jpg", alt = "..."),
              h4("Freddy Drennan"),
              p(class = "text-muted", "Lead Developer"),
              a(
                class = "btn btn-dark btn-social mx-2",
                href = "https://www.linkedin.com/in/freddydrennan",
                `aria-label` = "Freddy Drennan LinkedIn Profile",
                i(class = "fab fa-linkedin-in")
              ),
              a(
                class = "btn btn-dark btn-social mx-2",
                href = "https://github.com/fdrennan",
                `aria-label` = "Freddy Drennan Github Profile",
                i(class = "fab fa-github")
              ),
              a(
                class = "btn btn-dark btn-social mx-2",
                href = "https://gitlab.com/fdrennan",
                `aria-label` = "Freddy Drennan Gitlab Profile",
                i(class = "fab fa-gitlab")
              )
            )
          )
        )
      )
    )
  )
}
