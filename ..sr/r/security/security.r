#' @export
ui_security <- function(id = "security") {
  box::use(shiny = shiny[tags])
  box::use(.. / aws / secrets / user_secrets)

  ns <- shiny$NS(id)

  tags$div(
    class = "scrollmenu",
    user_secrets$ui_user_secrets(ns("user_secrets")),
    shiny$uiOutput(ns("sshandvpnmenu"))
  )
}

#' @export
server_security <- function(id = "security", ns_common_store_user) {
  box::use(shiny = shiny[tags])
  box::use(.. / aws / ec2 / security_groups)
  box::use(. / security)
  box::use(purrr)
  box::use(uuid)
  box::use(.. / inputs / inputs)
  box::use(.. / aws / secrets / user_secrets)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns



      user_secrets$server_user_secrets("user_secrets", ns_common_store_user)

      shiny$observe({
        # input$refreshAll
        box::use(.. / users / ip_address)
        box::use(.. / aws / settings / settings)
        box::use(.. / aws / secrets / secrets)
        # securityGroups <- security_groups$list_security_groups(ns_common_store_user)

        has_secret <- secrets$secret_exists(ns_common_store_user)
        if (has_secret) {
          output$sshandvpnmenu <- shiny$renderUI({
            ip_address$server_user_ip("user_ip")
            settings$server_aws_settings("aws_settings", ns_common_store_user, FALSE)
            settings$ui_aws_settings(ns("aws_settings"))
          })
        } else {
          shiny$showNotification("Ready to launch. Waiting on AWS Credentials. Trying again in 15 seconds.")
          shiny$invalidateLater(15000)
        }
      })
    }
  )
}

#' @export
ui_sgmanage <- function(id = "sgmanage") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  tags$div(
    class = "row",
    shiny$uiOutput(ns("ui"), container = function(...) {
      tags$div(
        class = "col-12", ...
      )
    })
  )
}

#' @export
server_sgmanage <- function(id = "sgmanage", GroupName, ns_common_store_user) {
  box::use(shiny = shiny[tags])
  box::use(.. / aws / ec2 / security_groups)
  box::use(. / security)
  box::use(.. / inputs / inputs)
  box::use(purrr)
  box::use(uuid)
  box::use(jsonlite)
  box::use(glue)
  box::use(tibble)
  box::use(dplyr)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        tags$div(
          class = "",
          tags$div(
            class = "row",
            tags$div(
              class = "col-12",
              inputs$card_collapse(
                ns,
                title = GroupName,
                body = {
                  tags$div(
                    tags$div(
                      class = "row",
                      tags$div(
                        class = "col-12 d-flex justify-content-end",
                        shiny$actionButton(ns("refresh"), tags$i(
                          class = "bi fa-lg bi-arrow-down"
                        ), class = "btn-info me-2 mb-2"),
                        shiny$actionButton(ns("deleteSg"), tags$i(
                          class = "bi fa-lg bi-x"
                        ), class = "btn-danger mb-2")
                      )
                    ),
                    shiny$uiOutput(ns("sgUI"))
                  )
                }
              )
            )
          )
        )
      })

      output$sgUI <- shiny$renderUI({
        input$refresh
        permissions <- security_groups$list_security_groups(
          full = TRUE, ns_common_store_user = ns_common_store_user
        )

        permissions <- dplyr$filter(permissions, GroupName == !!GroupName)


        containerId <- ns("sgList")
        #

        GroupName <- permissions$GroupName[[1]]
        GroupId <- permissions$GroupId[[1]]

        id <- uuid$UUIDgenerate()
        envokeUI <- security$ui_sgmanage_port(ns(id))

        security$server_sgmanage_port(id, ns_common_store_user, GroupName, GroupId, 22, "0.0.0.0", revoke = FALSE)



        revokeUI <-
          purrr$map(split(permissions, 1:nrow(permissions)), function(x) {
            id <- uuid$UUIDgenerate()
            out <- security$ui_sgmanage_port(ns(id))

            security$server_sgmanage_port(
              id,
              ns_common_store_user, GroupName, GroupId, x$FromPort, x$IpRanges,
              revoke = TRUE
            )
            out
          })




        sgUI <- append(
          list(envokeUI), revokeUI
        )
      })
      shiny$observeEvent(input$deleteSg, {
        shiny$showNotification(glue$glue("Deleting {GroupName}"))
        tryCatch(
          {
            security_groups$manage_security_groups(
              GroupName,
              ns_common_store_user = ns_common_store_user,
              delete = T
            )
            shiny$showNotification("Poof!")
          },
          error = function(err) {
            shiny$showNotification(tags$pre(as.character(err)))
          }
        )
      })
    }
  )
}


#' @export
ui_sgmanage_port <- function(id = "sgmanage_port") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_sgmanage_port <- function(
    id = "sgmanage_port", ns_common_store_user, GroupName, GroupId, FromPort, IpRanges, revoke = TRUE) {
  box::use(shiny = shiny[tags])
  box::use(.. / aws / ec2 / security_groups)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        if (revoke) {
          actionType <- shiny$actionButton(ns("revoke"), "Revoke", class = "btn-warning")
        } else {
          actionType <- shiny$actionButton(ns("envoke"), "Envoke", class = "btn-primary")
          FromPort <- ""
          IpRanges <- ""
        }
        tags$div(
          id = ns("mainUI"),
          class = "row mb-1 border-top p-1",
          tags$div(
            class = "col-md-5",
            shiny$textInput(ns("fromPort"), "Port", FromPort)
          ),
          tags$div(
            class = "col-md-5",
            shiny$textInput(ns("ipRanges"), "IP", IpRanges)
          ),
          tags$div(
            class = "col-sm-12 col-lg-2 d-flex justify-content-end",
            actionType
          )
        )
      })


      shiny$observeEvent(input$revoke, {
        tryCatch(
          {
            security_groups$security_group_revoke(
              GroupName = GroupName, ports = input$fromPort,
              ips = input$ipRanges,
              ns_common_store_user = ns_common_store_user
            )
            shiny::removeUI(selector = paste0("#", ns("mainUI")))
            shiny$showNotification("Poof")
          },
          error = function(err) {
            shiny$showNotification(as.character(err))
          }
        )
      })

      shiny$observeEvent(input$envoke, {
        tryCatch(
          {
            box::use(uuid)
            box::use(. / security)
            security_groups$security_group_envoke(
              GroupName = GroupName, input$fromPort,
              ips = input$ipRanges,
              ns_common_store_user = ns_common_store_user
            )

            elementId <- uuid$UUIDgenerate()
            out <- security$ui_sgmanage_port(ns(elementId))
            security$server_sgmanage_port(
              elementId,
              ns_common_store_user, GroupName,
              GroupId, input$FromPort, input$ipRanges
            )


            shiny::insertUI(selector = paste0("#", id), ui = out, where = "afterEnd")
            shiny$showNotification("Created")
          },
          error = function(err) {
            shiny$showNotification(as.character(err))
          }
        )
      })
    }
  )
}
