#' @export
ui_ipaddress <- function(id = "ipaddress") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  tags$div(
    class='row',
    tags$div(
      class='col-12 d-flex justify-content-end my-2',
      shiny$actionButton(ns('update'), 'Refresh', class='btn-primary')
    ),
    tags$div(
      class='col-12',
      shiny$uiOutput(ns("ui"))
    )
  )
}

#' @export
server_ipaddress <- function(id = "ipaddress", credentials, ns_common_store_user) {
  box::use(shiny = shiny[tags])
  box::use(.. / connections / redis)
  box::use(.. / connections / postgres)
  box::use(purrr)
  # box::use(.. / ec2 / aws_security_group_manager)
  box::use(.. / aws / ec2 / security_groups)
  box::use(dplyr)
  # box::use(.. / .. / inputs / inputs)

  # box::use(.. / .. / users / ip_address)
  # securityGroups <- list_security_groups(ns_common_store_user = ns_common_store_user)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      shiny$observeEvent(
        input$update, {
          output$ui <- shiny$renderUI({

            #
            # if (!getOption('docker')) ns_common_store_user <- shiny$NS('fdrennan')
            securitygroups <- security_groups$list_security_groups(
              ns_common_store_user = ns_common_store_user, full = T
            )


            knownips <- unique(securitygroups$IpRanges)
            iplookup <- data.frame(
              id = paste0(credentials$userdata$email,"-", knownips),
              email = credentials$userdata$email,
              user = '',
              location = '',
              ipaddress = knownips
            )
            if (!postgres$table_exists('iplookup')) {

              knownips <- postgres$table_create_or_upsert(iplookup, 'id')
            } else {


              

              knownips <- postgres$tables_row_retrieve(
                'email', credentials$userdata$email, 'iplookup'
              )

              iplookup <-
                iplookup[!iplookup$ipaddress %in% knownips$ipaddress]
              knownips <- dplyr$bind_rows(iplookup, knownips)
            }


            knownips <- knownips[!is.na(knownips$ipaddress),]

            knownips <- split(knownips, 1:nrow(knownips))
            tags$div(
              class = "container bg-light p-4 border-top",
              tags$div(
                class = "row",
                tags$div(
                  class = "col-12",
                  tags$table(
                    class = "table table-striped",
                    tags$thead(
                      tags$th(scope = "col", "Ip Address"),
                      tags$th(scope = "col", "Username"),
                      tags$th(scope = "col", "Location"),
                      tags$th(scope = "col", "")
                    ),
                    purrr$map(
                      knownips,
                      function(x) {
                        uuid <- uuid::UUIDgenerate()
                        box::use(./ipaddress)
                        out <- ipaddress$ui_useripaddress(ns(uuid))
                        ipaddress$server_useripaddress(uuid, credentials, x)
                        out
                      }
                    )
                  )
                  # shiny$uiOutput(ns('refactorInfo'))
                )
              )
            )




          })
        }
      )
    }
  )
}


#' @export
ui_useripaddress <- function(id = "useripaddress") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"), container = function(...) tags$tr(...))
}

#' @export
server_useripaddress <- function(id = "useripaddress", credentials,  x) {
  box::use(shiny = shiny[tags])
  box::use(../inputs/inputs)
  box::use(../connections/postgres)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        #
        shiny::tagList(
          tags$td(
            x$ipaddress
          ),
          tags$td(
            inputs$textInput(ns('user'), 'User', x$user)
          ),
          tags$td(
            inputs$textInput(ns('location'), 'Location Name', x$location)
          ),
          tags$td(
            shiny$actionButton(ns('submit'), 'Update', class='btn-primary' )
          )
        )
      })

      shiny$observeEvent(
        input$submit, {

          iplookup <- data.frame(
            id = paste0(credentials$userdata$email,"-", x$ipaddress),
            email = credentials$userdata$email,
            user = input$user,
            location = input$location,
            ipaddress = x$ipaddress
          )
          postgres$table_create_or_upsert(data = iplookup, where_cols = 'id')

          shiny$showNotification('Updated')
        }
      )

    }
  )
}
