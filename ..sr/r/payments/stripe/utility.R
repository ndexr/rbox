#' @export
idempotency <- function(seed = 1234567) {
  set.seed(seed)
  paste(sample(c(LETTERS, letters, 0:9), 15, TRUE), collapse = "")
}

#' @export
is.error <- function(test_me) {
  inherits(test_me, "try-error")
}

#' @export
error.message <- function(test_me) {
  if (is.error(test_me)) attr(test_me, "condition")$message
}

#' @export
make_meta <- function(metadata) {
  stopifnot(
    max(nchar(names(metadata))) <= 40,
    max(nchar(metadata)) <= 500,
    length(metadata) <= 20
  )

  names(metadata) <- paste0("metadata[", names(metadata), "]")

  metadata
}
