#' @export
ui_terminal <- function(id, text = "#hello", mode = "bash", header = "") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(shinyAce)
    box::use(. / terminal)
  }
  ns <- shiny$NS(id)

  tags$div(
    class = "",
    tags$div(
      class = "row", style = "overflow-y: auto;",
      tags$div(
        class = "col-12",
        shiny$textInput(ns("filePath"), "File Path", header),
        shiny$actionButton(ns("submitFile"), "Submit File", class = "btn  text-end"),
        shinyAce$aceEditor(
          value = text,
          outputId = ns("code"),
          minLines = 10,
          mode = mode,
          theme = "crimson_editor",
          height = "100%",
          autoComplete = "live",
          # autoCompleters = "rlang",
          vimKeyBinding = TRUE,
          showLineNumbers = TRUE,
          hotkeys = list(
            runKey = list(
              win = "Ctrl-Enter",
              mac = "CMD-ENTER"
            )
          )
        )
      )
    )
  )
}

#' @export
server_terminal <- function(id, ec2data, keyfile, command = NULL, user = "ubuntu") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(shinyAce)
    box::use(jsonlite)
    box::use(knitr)
    box::use(ssh_utils = .. / connections / ssh)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      shiny$observeEvent(
        input$submitFile,
        {
          ssh_utils$ssh_upload_text_to_file(ec2data, keyfile, text = input$code, filepath = input$filePath, user = "ubuntu")
        }
      )
      # shiny$observe({
      #   input
      #   #
      # })
      # shinyAce$aceAutocomplete("code")

      # output$codeOutput <- shiny$renderPrint({
      #   if (is.null(input)) {
      #     return("Execute [R] chunks with Ctrl/Cmd-Enter")
      #   } else {
      #     tryCatch(expr = {
      #       rlang$eval_tidy(rlang$parse_expr(input$code_runKey$selection))
      #     }, error = function(err) {
      #       as.character(err)
      #     })
      #   }
      # })
    }
  )
}
