#' @export
ui_ip_labeler <- function(id = "ip_labeler", ns_common_store_user) {
  box::use(shiny = shiny[tags, HTML])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_ip_labeler <- function(id = "ip_labeler", ns_common_store_user) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(jsonlite)
    box::use(.. / ec2 / security_groups[list_security_groups])
    box::use(.. / .. / inputs / inputs)
    box::use(dplyr)
    box::use(purrr)
  }


  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      # redux <- redis$get_state(ns_common_store_user("ec2_settings"))

      securityGroups <- list_security_groups(full = T, ns_common_store_user = ns_common_store_user)

      securityGroups <-
        securityGroups |>
        dplyr$select(
          GroupName, FromPort, IpRanges,
        ) |>
        dplyr$arrange(FromPort, GroupName, IpRanges)


      output$ui <- shiny$renderUI({
        tags$div(
          class = "container",
          tags$table(
            class = "table table-striped",
            tags$thead(
              tags$tr(
                tags$th("Security Group"),
                tags$th("Port"),
                tags$th("IP Address")
              )
            ),
            tags$tbody(
              tags$tr(
                purrr$map(
                  split(securityGroups, 1:nrow(securityGroups)),
                  function(x) {
                    tags$tr(
                      tags$td(x$GroupName),
                      tags$td(x$FromPort),
                      tags$td(x$IpRanges)
                    )
                  }
                )
              )
            )
          )
        )
      })
    }
  )
}
