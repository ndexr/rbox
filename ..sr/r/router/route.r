#' @export
route <- function(path, ui, server = NA) {
  box::use(. / route)
  if (!missing(server)) {
    warning("`server` argument in `route` is deprecated.\n      It will not be used when `route` is called inside `router_ui`.")
  }
  out <- list()
  out[[path]] <- route$callback_mapping(path, ui, server)
  out
}


#' @export
callback_mapping <- function(path, ui, server = NA) {
  box::use(. / route)
  server <- if (is.function(server)) {
    if ("..." %in% names(formals(server))) {
      server
    } else {
      formals(server) <- append(formals(server), alist(... = ))
      server
    }
  } else {
    function(input, output, session, ...) {
    }
  }
  out <- list()
  ui <- route$attach_attribs(ui, path)
  out[["ui"]] <- ui
  out[["server"]] <- server
  out
}
#' @export
attach_attribs <- function(ui, path) {
  if ("shiny.tag" %in% class(ui)) {
    ui$attribs$`data-path` <- path
    ui$attribs$class <- paste("router router-hidden", ui$attribs$class)
  }
  if ("shiny.tag.list" %in% class(ui)) {
    container <- shiny::div(ui)
    container$attribs$`data-path` <- path
    container$attribs$class <- "router router-hidden"
    ui <- container
  }
  ui
}
