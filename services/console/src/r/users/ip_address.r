#' @export
ui_user_ip <- function(id = "user_ip") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_user_ip <- function(id = "user_ip") {
  box::use(shiny = shiny[tags, HTML])
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        tags$div(
          class = "row",
          tags$div(
            class = "col",
            tags$p(
              class = "me-3",
              "Your IP address is:"
            ),
            tags$p(
              tags$code(
                id = ns("ipaddress")
              )
            )
          ),
          tags$script(
            HTML(
              paste0("async function getIpAddress() {
      const response = await fetch('https://api.ipify.org?format=json');
      const data = await response.json();
      return data.ip;
    }

    // Get the IP address and fill it into the code tag
    getIpAddress()
      .then(ipAddress => {
        const codeTag = document.getElementById('", ns("ipaddress"), "');
        codeTag.innerHTML = ipAddress;
      })
      .catch(err => console.log(err));")
            )
          )
        )
      })
    }
  )
}

#' @export
ui_ip_input <- function(id = "ip_input") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_ip_input <- function(id = "ip_input") {
  box::use(shiny = shiny[tags])
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        tags$div(
          class = "row",
          tags$div(
            class = "col-12 d-flex justify-content-start",
            tags$div(
              class = "",
              tags$label(
                `for` = ns("ipaddress"),
                class = "form-label",
                "IP Address:"
              ),
              tags$input(
                type = "text",
                id = ns("ipaddress"),
                name = ns("ipaddress"),
                class = "form-control"
              )
            ),
            tags$script(
              HTML(
                paste0("fetch('https://api.ipify.org/?format=json')
      .then(response => response.json())
      .then(data => {
        // Set the input value to the user's IP address
        document.getElementById('", ns("ipaddress"), "').value = data.ip;
      });")
              )
            )
          )
        )
      })
    }
  )
}
