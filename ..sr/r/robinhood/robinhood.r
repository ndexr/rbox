#' @export
ui_robinhood <- function(id = "robinhood") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_robinhood <- function(id = "robinhood") {
  box::use(shiny = shiny[tags])
  box::use(reticulate)
  rs <- reticulate$import("robin_stocks")

  rscon <-
    rs$robinhood$login(
      username = "drennanfreddy@gmail.com",
      password = "AeliaJames2022!",
      expiresIn = 86400L,
      by_sms = TRUE
    )

  profile <- rs$robinhood$load_account_profile()


  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({

      })
    }
  )
}
