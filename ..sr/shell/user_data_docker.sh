#!/bin/bash

mkdir -p /var/log/ndexr/ssh

LOGFILE='/var/log/ndexr/build.log'
savelog "$LOGFILE"
exec &> >(tee "$LOGFILE")

echo "NDEXR_TYPE_DOCKER"
echo "NDEXR_STARTED"

apt update -qq
apt install --no-install-recommends software-properties-common dirmngr whois

apt install -y apt-transport-https ca-certificates curl nginx python3-pip python3-bottle \
  gnupg lsb-release unzip \
  make nginx certbot python3-certbot-nginx

ufw allow 'Nginx Full'
systemctl restart nginx

echo "NGINX_COMPLETE"

# ADD R Repo
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/" -y

apt install -y gdebi r-base r-base-dev libssl-dev zsh libcurl4-openssl-dev libgit2-dev \
  libxml2-dev cmake libcairo2-dev dirmngr libcurl4-openssl-dev libssh2-1-dev \
  zlib1g-dev make git-core libcurl4-openssl-dev libpq-dev cmake libsodium-dev

mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt update
apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker}
usermod -aG docker ubuntu

echo "DOCKER_COMPLETE"
