options(docker = FALSE)
options(ndexr_site = "ndexrio")
rstudioapi::restartSession(
  'options(shiny.launch.browser = .rs.invokeShinyPaneViewer);shiny::runApp("src")'
)
