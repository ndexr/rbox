#' @export
ui_docker <- function(id = "docker", ec2data) {
  box::use(shiny = shiny[tags, HTML])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_docker <- function(id = "docker", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(glue[glue])
    box::use(.. / .. / .. / connections / ssh)
    box::use(.. / .. / .. / connections / redis)
    box::use(.. / .. / .. / inputs / inputs)
    box::use(jsonlite)
    box::use(. / docker)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      ns_common_store_user <- ec2data$ns_common_store_user

      elastic_ips <- ec2data$elastic_ips
      hosted_zones <- ec2data$hosted_zones

      isRunning <- ec2data$instance$state == "running"
      if (is.na(ec2data$instance$PublicIpAddress)) {
        iptag <- "No IP"
        has_elastic <- FALSE
      } else {
        has_elastic <- ec2data$instance$PublicIpAddress %in% elastic_ips$PublicIp
        if (has_elastic) {
          iptag <- glue("Elastic IP: {ec2data$instance$PublicIpAddress}")
        } else {
          iptag <- ec2data$instance$PublicIpAddress
        }
      }


      output$ui <- shiny$renderUI({
        box::use(.. / .. / .. / inputs / inputs)

        box::use(sys)
        box::use(stringr)
        box::use(purrr)
        box::use(glue)
        box::use(uuid)
        box::use(dplyr)
        box::use(. / docker)

        id <- uuid$UUIDgenerate()

        composefiles <- ssh$ssh_execute(ec2data, "docker compose ls")
        composefiles <-
          readr::read_table(composefiles[[2]])$CONFIG



        containercols <-
          c("CONTAINER ID", "IMAGE", "COMMAND", "CREATED", "STATUS", "PORTS", "NAMES")

        containers <- ssh$ssh_execute(ec2data, "docker container ls")[[2]]
        #
        colstart <- stringr$str_locate(containers, containercols)[, 1]
        colend <- dplyr$lead(colstart - 1)

        lengthOfString <- max(purrr$map_dbl(strsplit(containers[1], "\n")[[1]], stringr$str_length))
        colend[is.na(colend)] <- lengthOfString
        columns_to_collect <- data.frame(
          containercols = containercols,
          colstart = colstart,
          colend = colend
        )


        containers <- purrr$map_dfr(
          stringr$str_split(containers, "\n")[[1]],
          function(x) {
            purrr$map_dfc(
              split(columns_to_collect, 1:nrow(columns_to_collect)),
              function(y) {
                value <- stringr$str_sub(
                  x, y$colstart, y$colend
                )
                value <- stringr$str_trim(value)
                out <- data.frame(value)
                names(out) <- y$containercols
                out
              }
            )
          }
        )

        containers <- containers[containers$IMAGE != "", ]
        containers <- containers[containers$IMAGE != "IMAGE", ]


        if (length(composefiles)) {
          composefiles <- purrr$map(composefiles, function(x) {
            container_id <- paste0(id, x)
            out <- tryCatch(
              {
                out <- docker$ui_docker_compose(ns(container_id))
                docker$server_docker_compose(container_id, x, ec2data)
                out
              },
              error = function(err) ""
            )
            out
          })
        } else {
          composefiles <- tags$div("Compose apps will appear here once started")
        }

        if (nrow(containers)) {
          containers <- purrr$map(
            split(containers, 1:nrow(containers)), function(x) {
              container_id <- paste0(id, x$`CONTAINER ID`)
              out <- docker$ui_docker_container(ns(container_id))
              docker$server_docker_container(container_id, x, ec2data)
              out
            }
          )
        } else {
          containers <- tags$div("Containers will appear here once started")
        }

        body <-
          tags$div(
            tags$h5("Start an Application"),
            docker$ui_docker_application(ns("docker_application")),
            tags$h5("Scale Application (if applicable)"),
            composefiles,
            tags$h5("Current Containers"),
            containers
          )
        docker$server_docker_application("docker_application", ns_common_store_user, ec2data)

        inputs$card_collapse(ns, "Docker", body = body, footer = NULL)
      })
    }
  )
}

#' @export
ui_docker_compose_service <- function(id = "docker_compose_service") {
  box::use(shiny = shiny[tags])

  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_docker_compose_service <- function(id = "docker_compose_service", service, serviceName, filepath, ec2data) {
  box::use(shiny = shiny[tags])
  box::use(.. / .. / .. / inputs / inputs)

  box::use(sys)
  box::use(stringr)
  box::use(purrr)
  box::use(glue)
  box::use(uuid)
  box::use(jsonlite)
  box::use(. / docker)
  box::use(.. / .. / .. / connections / ssh)
  box::use(stringr)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        #
        portRequired <- strsplit(service$ports, ":")[[1]][[1]]
        if (stringr$str_detect(portRequired, "-")) {
          portRequired <- strsplit(portRequired, "-")[[1]]
          portRequired <- as.numeric(portRequired)
          portRequired <- portRequired[2] - portRequired[1]
        } else {
          portRequired <- 1
        }
        tags$div(
          class = "row",
          tags$p(service$image),
          tags$p(service$restart),
          tags$p(service$ports),
          tags$p(service$profiles),
          tags$pre(
            jsonlite$toJSON(service, pretty = T)
          ),
          tags$div(
            class = "col-12",
            shiny$numericInput(ns("scaleAmount"), "Scale", portRequired, 1, portRequired),
            shiny$actionButton(ns("scale"), "Scale", class = "btn-primary")
          )
        )
      })

      shiny$observeEvent(
        input$scale,
        {
          cmd <- glue$glue("sudo docker compose --profile={service$profiles} --file={filepath} up -d --scale {serviceName}={input$scaleAmount}")
          shiny$showNotification(cmd)
          inspect <- tryCatch(
            ssh$ssh_execute(ec2data, cmd),
            error = function(err) {
              as.character(err)
            }
          )
          shiny$showNotification(paste0(inspect, collapse = ""))
        }
      )
    }
  )
}

#' @export
ui_docker_compose <- function(id = "docker_compose") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_docker_compose <- function(id = "docker_compose", filepath, ec2data) {
  box::use(shiny = shiny[tags])
  box::use(shiny = shiny[tags, HTML])
  box::use(glue)
  box::use(.. / .. / .. / connections / ssh)
  box::use(jsonlite)
  box::use(purrr)
  box::use(. / docker)
  box::use(.. / .. / .. / inputs / inputs)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      dockercompose <- shiny$reactive({
        shiny$req(filepath)

        inspect <- tryCatch(
          ssh$ssh_execute(ec2data, glue$glue("sudo cat {filepath}")),
          error = function(err) {
            as.character(err)
          }
        )

        dockercompose <- yaml::read_yaml(text = inspect[2])

        dockercompose
      })

      output$ui <- shiny$renderUI({
        shiny$req(dockercompose())

        purrr$imap(
          dockercompose()$services, function(x, y) {
            uuid <- uuid::UUIDgenerate()
            container_id <- paste0(uuid, y)
            inputs$card_collapse(
              ns,
              title = y,
              body = {
                out <- docker$ui_docker_compose_service(ns(container_id))
                docker$server_docker_compose_service(container_id, x, y, filepath, ec2data)
                out
              }
            )
          }
        )
      })

      shiny$observeEvent(
        input$scaleModify,
        {
          inspect <- tryCatch(
            ssh$ssh_execute(
              ec2data,
              glue$glue("docker compose --profile=local up -d --scale console=11")
            ),
            error = function(err) {
              as.character(err)
            }
          )
        }
      )
    }
  )
}

#' @export
ui_docker_container <- function(id) {
  box::use(shiny = shiny[tags, HTML])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_docker_container <- function(id, container, ec2data) {
  box::use(shiny = shiny[tags])

  box::use(shiny = shiny[tags, HTML])
  box::use(glue)
  box::use(.. / .. / .. / connections / ssh)
  box::use(.. / .. / .. / inputs / inputs)
  box::use(jsonlite)
  box::use(purrr)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      container_id <- container$`CONTAINER ID`
      names <- container$NAMES
      image <- container$IMAGE
      created <- container$CREATED
      command <- container$COMMAND

      output$ui <- shiny$renderUI({
        shiny$showNotification(glue$glue("Pulling {names}"))
        inspect <- ssh$ssh_execute(ec2data, glue$glue("docker inspect {container_id}"))
        inspect <- paste0(inspect[2], collapse = "\n")
        inspect <- jsonlite$fromJSON(inspect)
        tags$div(
          class = "row mt-2 mb-5 border-top",
          tags$p(names),
          tags$div(
            class = "col-12 d-flex justify-content-between align-items-center mb-3",
            tags$button(
              id = ns("inspectButton"),
              type = "button",
              class = "btn btn-primary action-button",
              "Inspect"
            ),
            tags$button(
              id = ns("logsButton"),
              type = "button",
              class = "btn btn-primary action-button",
              "Logs"
            )
          ),
          tags$div(
            class = "col-12 d-flex justify-content-start align-items-center scrollmenu mb-3",
            purrr$imap(container, function(x, y) {
              tags$div(
                class = "row", tags$b(paste0(y, ": ")), tags$code(x)
              )
            })
          ),
          tags$div(
            class = "col-12 d-flex justify-content-between align-items-center",
            tags$button(
              id = ns("startButton"),
              type = "button",
              class = "btn btn-success action-button",
              "Start"
            ),
            tags$button(
              id = ns("stopButton"),
              type = "button",
              class = "btn btn-danger action-button",
              "Stop"
            ),
            tags$button(
              type = "button",
              id = ns("rmButton"),
              class = "btn btn-warning action-button",
              "Remove"
            ),
            tags$button(
              id = ns("restartButton"),
              type = "button",
              class = "btn btn-warning action-button",
              "Restart"
            )
          ),
          tags$div(
            class = "col-12",
            shiny$uiOutput(ns("logPanel"))
          )
        )
      })

      shiny$observeEvent(input$startButton, {
        shiny$showNotification(glue$glue("Starting {container$NAMES}"))
        logs <- tryCatch(
          {
            ssh$ssh_execute(ec2data, glue$glue("docker container start {container$NAMES}"))
          },
          error = function(err) {
            shiny$showModal(
              inputs$modalDialog(
                tags$pre(as.character(err))
              )
            )
            shiny$req(FALSE)
          }
        )
        shiny$showNotification(glue$glue("Started {container$NAMES}"))
      })
      shiny$observeEvent(input$rmButton, {
        shiny$showNotification(glue$glue("Removing {container$NAMES}"))
        logs <- tryCatch(
          {
            ssh$ssh_execute(ec2data, glue$glue("docker container rm {container$NAMES}"))
          },
          error = function(err) {
            shiny$showModal(
              inputs$modalDialog(
                tags$pre(as.character(err))
              )
            )
            shiny$req(FALSE)
          }
        )
        shiny$showNotification(glue$glue("Removed {container$NAMES}"))
      })


      shiny$observeEvent(input$stopButton, {
        shiny$showNotification(glue$glue("Stopping {container$NAMES}"))
        logs <- tryCatch(
          {
            ssh$ssh_execute(ec2data, glue$glue("docker container stop {container$NAMES}"))
          },
          error = function(err) {
            shiny$showModal(
              inputs$modalDialog(
                tags$pre(as.character(err))
              )
            )
            shiny$req(FALSE)
          }
        )
        shiny$showNotification(glue$glue("Stopped {container$NAMES}"))
      })

      shiny$observeEvent(input$restartButton, {
        shiny$showNotification(glue$glue("Restarting {container$NAMES}"))
        logs <- tryCatch(
          {
            ssh$ssh_execute(ec2data, glue$glue("docker container restart {container$NAMES}"))
          },
          error = function(err) {
            shiny$showModal(
              inputs$modalDialog(
                tags$pre(as.character(err))
              )
            )
            shiny$req(FALSE)
          }
        )
        shiny$showNotification(glue$glue("Restarted {container$NAMES}"))
      })


      shiny$observeEvent(input$logsButton, {
        logs <- tryCatch(
          {
            ssh$ssh_execute(ec2data, glue$glue("docker logs {container$NAMES} --tail=100"))
          },
          error = function(err) {
            shiny$showModal(
              inputs$modalDialog(
                tags$pre(as.character(err))
              )
            )
            shiny$req(FALSE)
          }
        )
        output$logPanel <- shiny$renderUI({
          tags$pre(
            style = "height: 300px; overflow: auto;",
            paste0(logs, collapse = "\n")
          )
        })
      })

      shiny$observeEvent(input$inspectButton, {
        shiny$showNotification(glue$glue("Pulling {container$NAMES}"))
        inspect <- tryCatch(
          {
            ssh$ssh_execute(ec2data, glue$glue("docker inspect {container$NAMES}"))
          },
          error = function(err) {
            shiny$showModal(
              inputs$modalDialog(
                tags$pre(as.character(err))
              )
            )
            shiny$req(FALSE)
          }
        )
        inspect <- paste0(inspect[2], collapse = "\n")
        inspect <- jsonlite$fromJSON(inspect)
        shiny$showModal(
          inputs$modalDialog(
            tags$pre(
              jsonlite$toJSON(inspect, pretty = TRUE)
            )
          )
        )
      })
    }
  )
}


#' @export
ui_docker_application <- function(id = "docker_application") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_docker_application <- function(id = "docker_application", ns_common_store_user, ec2data) {
  box::use(shiny = shiny[tags])
  box::use(shiny = shiny[tags])
  box::use(glue)
  box::use(.. / .. / .. / connections / ssh)
  box::use(jsonlite)
  box::use(purrr)
  box::use(. / docker)
  box::use(.. / .. / .. / inputs / inputs)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        tags$div(
          class = "row",
          tags$div(
            class = "col-12"
          ),
          tags$div(
            class = "col-md-6",
            tags$h5("Postgres"),
            shiny$passwordInput(ns("POSTGRES_USER"), "Postgres Admin Username"),
            shiny$passwordInput(ns("POSTGRES_PASSWORD"), "Postgres Admin Password"),
            shiny$passwordInput(ns("POSTGRES_DB"), "Default Database"),
            shiny$actionButton(ns("postgres"), "Start")
          ),
          tags$div(
            class = "col-md-6",
            tags$h5("Postgres"),
            shiny$actionButton(ns("redis"), "Start")
          )
        )
      })


      shiny$observeEvent(
        input$postgres,
        {
          #
          shiny$showNotification("Launching Postgres")
          cat(ssh$ssh_execute(ec2data, "sudo mkdir -p /srv/ndexr/postgres"))
          envfile <- paste0(
            c(
              paste0("POSTGRES_USER=", input$POSTGRES_USER),
              paste0("POSTGRES_PASSWORD=", input$POSTGRES_PASSWORD),
              paste0("POSTGRES_DB=", input$POSTGRES_DB)
            ),
            collapse = "\n"
          )

          box::use(readr)
          tf <- tempfile()
          readr$write_file(envfile, tf)
          ssh$ssh_upload_file(
            ec2data, tf, "/srv/ndexr/postgres/.env"
          )
          ssh$ssh_upload_file(
            ec2data = ec2data,
            "./docker/postgres/docker-compose.yaml",
            "/srv/ndexr/postgres/docker-compose.yaml"
          )
          cat(ssh$ssh_execute(ec2data, "sudo docker compose -f /srv/ndexr/postgres/docker-compose.yaml up -d"))
          shiny$showNotification("Postgres Launched")
        }
      )

      shiny$observeEvent(
        input$redis,
        {
          shiny$showNotification("Launching Redis")
          cat(ssh$ssh_execute(ec2data, "sudo mkdir -p /srv/ndexr/redis"))
          ssh$ssh_upload_file(
            ec2data = ec2data,
            "./docker/redis/docker-compose.yaml",
            "/srv/ndexr/redis/docker-compose.yaml"
          )
          cat(ssh$ssh_execute(ec2data, "sudo docker compose -f /srv/ndexr/redis/docker-compose.yaml up -d"))
          shiny$showNotification("Redis Launched")
        }
      )
    }
  )
}
