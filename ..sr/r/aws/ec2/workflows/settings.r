#' @export
ui_settings <- function(id = "settings", ec2data) {
  box::use(shiny = shiny[tags, HTML])
  box::use(.. / .. / .. / inputs / inputs)



  ns <- shiny$NS(id)

  shiny$uiOutput(ns("instanceDataTable"))
}

#' @export
server_settings <- function(id = "settings", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(glue[glue])
    box::use(.. / .. / .. / connections / ssh)
    box::use(.. / .. / .. / connections / redis)
    box::use(.. / .. / .. / inputs / inputs)
    box::use(jsonlite)
    box::use(purrr)
  }


  hash <- function(id) paste0("#", id)


  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      ns_common_store_user <- ec2data$ns_common_store_user


      output$instanceDataTable <- shiny$renderUI({
        instance <- ec2data$instance
        instance <- as.list(instance)
        purrr$imap(
          instance, function(x, y) {
            box::use(shiny = shiny[tags])
            tags$div(
              class = "row",
              tags$div(
                class = "col-12 d-flex justify-content-start align-items-center",
                tags$b(y, class = "me-1"), tags$code(x, class = "text-dark")
              )
            )
          }
        )
      })
    }
  )
}
