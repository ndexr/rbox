#' #' @export
#' ui_admin <- function(id = "admin", credentials = list(user = "default")) {
#'   {
#'     box::use(shiny = shiny[tags, HTML])
#'     box::use(dplyr)
#'     box::use(purrr)
#'     box::use(glue)
#'     box::use(shinyjs)
#'     box::use(readr[read_file])
#'   }
#'
#'   ns_common_store_user <- shiny$NS(credentials)
#'   ns <- shiny$NS(id)
#'
#'
#'   shiny$uiOutput(ns("ui"))
#' }
#'
#'
#'
#' #' @export
#' server_admin <- function(id = "admin", ns_common_store_user) {
#'   {
#'     box::use(shiny = shiny[tags, HTML])
#'     box::use(dplyr)
#'     box::use(purrr)
#'     box::use(stringr)
#'     box::use(shinyjs)
#'     box::use(utils)
#'     box::use(utils)
#'     box::use(purrr)
#'     box::use(fs)
#'     box::use(.. / aws / client)
#'     box::use(.. / connections / redis)
#'     box::use(DT)
#'     box::use(.. / state / setDefault[setDefault])
#'     box::use(stats)
#'     box::use(. / postgres / postgres)
#'   }
#'
#'
#'
#'   shiny$moduleServer(
#'     id,
#'     function(input, output, session) {
#'       ns <- session$ns
#'       output$ui <- shiny$renderUI({
#'         tags$div(
#'           class = " pb-5 mb-4",
#'           tags$div(
#'             class = "row",
#'             tags$div(
#'               class = "col-12",
#'               postgres$ui_postgres_table(ns("instance_state"), "instance_state"),
#'               postgres$ui_postgres_table(ns("ec2users"), "ec2users")
#'             )
#'           )
#'         )
#'       })
#'
#'       postgres$server_postgres_table("instance_state", "instance_state")
#'       postgres$server_postgres_table("ec2users", "ec2users")
#'     }
#'   )
#' }
