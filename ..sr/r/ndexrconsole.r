#' @export
ui_ndexrconsole <- function(id) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(shinyjs)
    box::use(. / sidebar / sidebar)
    box::use(. / footer / footer)
    box::use(. / users / subscribe)
    box::use(. / sidebar / site_sidebar)
    box::use(. / cognito / cognito)
    box::use(bs4Dash)
    box::use(. / bs4dash / bs4dash)
    box::use(. / aws / aws)
    box::use(. / login / user_login)
    box::use(. / login / user_create)
    box::use(. / homepage / homepage)
    box::use(. / users / subscribe)
    box::use(. / users / management)
    box::use(. / security / security)
    box::use(shiny = shiny[tags])
    box::use(. / head / head)
    box::use(. / payments / stripe)
  }

  ns <- shiny$NS(id)

  shiny$addResourcePath(prefix = "javascript", directoryPath = "./javascript")
  shiny$addResourcePath(prefix = "css", directoryPath = "./css")
  shiny$addResourcePath(prefix = "images", directoryPath = "./images")

  tags$html(
    lang = "en",
    head$ui_head(),
    tags$body(
      id = "page-top",
      class = "bg-light",
      bs4dash$bs4_dashboard(
        dark = TRUE,
        header = tags$div(),
        sidebar = bs4Dash$dashboardSidebar(
          collapsed = TRUE, disable = TRUE
        ),
        body = shiny$uiOutput(ns("mainDashboard")),
        footer = bs4dash$bs4_dashboard_footer()
      ),
      {
        if (getOption("docker")) {
          tags$div(
            class = "container my-3",
            cognito$ui_cognito(ns("cognito"))
          )
        }
      },
      footer$ui_footer(ns("footer")),
      shiny$includeScript("../node_modules/@popperjs/core/dist/umd/popper.min.js"),
      shiny$includeScript("../node_modules/bootstrap/dist/js/bootstrap.min.js"),
      shiny$includeScript("../node_modules/js-cookie/dist/js.cookie.min.js"),
      shiny$includeScript("./javascript/cookies.js"),
      shiny$includeScript("./javascript/inittooltip.js"),
      tags$link(
        rel = "stylesheet",
        href = "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css"
      ),
      shiny$includeCSS("./css/styles.css", rel = "stylesheet")
    )
  )
}

#' @export
server_ndexrconsole <- function(id) {
  {
    box::use(shiny = shiny[withTags, tags])
    box::use(. / terminal / terminal)
    box::use(. / sidebar / sidebar)
    box::use(. / aws / aws)
    box::use(. / git / git)
    box::use(. / admin / admin)
    box::use(. / login / user_login)
    box::use(. / login / user_create)
    box::use(. / homepage / homepage)
    box::use(. / footer / footer)
    box::use(. / users / subscribe)
    box::use(. / users / management)
    box::use(. / aws / secrets / secrets)
    box::use(. / users / universal)
    box::use(. / aws / secrets / user_secrets)
    box::use(. / users / ip_address)
    box::use(. / aws / settings / settings)
    box::use(. / gmail / gmail)
    box::use(. / ndexrconsole)
    box::use(. / security / security)
    box::use(. / cognito / cognito)
    box::use(. / sidebar / site_sidebar)
    box::use(. / bs4dash / bs4dash)
    box::use(. / debug / boxupdater)
    box::use(. / payments / stripe)
    box::use(. / connections / postgres)
    box::use(. / inputs / inputs)
    box::use(. / users / ipaddress)
    box::use(bs4Dash)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      if (getOption("docker")) {
        login <- cognito$server_cognito("cognito")
      } else {
        login <- shiny::reactive({
          list(
            userdata = list(
              email = "drennanfreddy@gmail.com",
              username = "fdrennan"
            )
            # userdata = list(
            #   email = "fdrennan@ndexr.io",
            #   username = "ndexr"
            # )
          )
        })
      }

      output$mainDashboard <- shiny$renderUI({
        bs4dash$bs4_dashboard_body(
          sidebar$ui_sidebar(ns("sidebar")),
          shiny$uiOutput(ns("bodyui"))
        )
      })


      output$bodyui <- shiny$renderUI({
        tags$div(
          class = "container-fluid",
          tags$div(
            class = "row",
            tags$div(
              class = "col-md-12",
              {
                out <- subscribe$ui_subscribe(ns("subscribe"), frontpage = FALSE)
                subscribe$server_subscribe("subscribe", frontpage = FALSE)
                out
              }
            ),
            tags$div(
              class = "col-md-5",
              management$ui_management(ns("management")),
              shiny$uiOutput(ns("sendMessage"))
            ),
            tags$div(
              class = "col-md-7",
              tags$div(
                class = "row",
                inputs$card_collapse(
                  ns, 'IP List',
                  ipaddress$ui_ipaddress(ns('ipaddress'))
                ),
                aws$ui_aws(ns("aws")),
                tags$div(
                  class = "mb-3",
                  tags$h5("Access Management"),
                  tags$div(class = "p-1 bg-secondary rounded")
                ),
                security$ui_security(ns("security"))
              )
            )
          )
        )
      })

      output$sendMessage <- shiny$renderUI({
        box::use(. / gmail / gmail)
        output$sendMessage <- shiny$renderUI({
          tags$div(
            class = "row",
            tags$div(
              class = "col-12",
              inputs$textAreaInput(ns("message"), "Feel free to reach out!")
            ),
            tags$div(
              class = "p-3 col-12 d-flex justify-content-end",
              shiny$actionButton(ns("submitMessage"), "Submit", class = "btn-primary")
            )
          )
        })
      })

      shiny$observeEvent(input$submitMessage, {
        gmail$send_email(to = "drennanfreddy@gmail.com", subject = paste0(c(login$email, input$message, collapse = "\n")))
      })



      shiny$observeEvent(
        login,
        {

          if (is.function(login)) {
            shiny$req(login()$userdata)
            login <- login()$userdata
          } else {
            shiny$req(login$userdata)
            login <- login$userdata
          }


          # browser()

          app_session_start <- data.frame(email = login$email,
                                          session_id = uuid::UUIDgenerate(),
                                          time = as.integer(Sys.time()))

          postgres$table_append(data = app_session_start)



          print(login)
          box::use(shiny.router)
          box::use(. / aws / sns / sns)
          if (login$email == "drennanfreddy@gmail.com") {
            admin <- TRUE
          } else {
            admin <- FALSE
          }
          ns_common_store_admin <- shiny$NS("fdrennan")
          ns_common_store_user <- shiny$NS(login$email)
          security$server_security("security", ns_common_store_user)

          has_secret <- secrets$secret_exists(ns_common_store_user)

          customer_ids <- stripe$stripe_customer_ids(emails = login$email)
          customer_subscriptions <- postgres$tables_row_retrieve("customer_id", customer_ids$customer_id, "subscription", showNotification = FALSE, dbname = "stripe")
          subscription_active <- nrow(customer_subscriptions) > 0

          management$server_management("management", login = login, admin = admin)

          if (has_secret & subscription_active) {
            aws$server_aws("aws", credentials = login)
            ipaddress$server_ipaddress('ipaddress', credentials = login(), ns_common_store_user)
            gmail$send_email(to = "drennanfreddy@gmail.com", subject = ns_common_store_user("has_secret"))
          } else {
            shiny$observe({
              has_secret <- secrets$secret_exists(ns_common_store_user)
              customer_ids <- stripe$stripe_customer_ids(emails = login$email)
              customer_subscriptions <- postgres$tables_row_retrieve("customer_id", customer_ids$customer_id, "subscription", showNotification = FALSE, dbname = "stripe")
              subscription_active <- nrow(customer_subscriptions) > 0
              if (!subscription_active) {
                shiny$invalidateLater(8000)
                shiny$showNotification("Waiting on payment.")
                gmail$send_email(to = "drennanfreddy@gmail.com", subject = ns_common_store_user("no_payment"))

                shiny$req(FALSE)
              }
              if (!has_secret) {
                shiny$invalidateLater(8000)
                shiny$showNotification("Please submit AWS Credentials.")
                gmail$send_email(to = "drennanfreddy@gmail.com", subject = ns_common_store_user("no_secret"))

                shiny$req(FALSE)
              }

              management$server_management("management", login = login, admin = admin)
              aws$server_aws("aws", credentials = login)
              gmail$send_email(to = "drennanfreddy@gmail.com", subject = ns_common_store_user("has_secret"))
            })

            user_secrets$server_user_secrets("user_secrets", ns_common_store_user)
            ip_address$server_user_ip("user_ip")
            settings$server_aws_settings("aws_settings", ns_common_store_user, show_rsa = FALSE)
          }
        }
      )

      homepage$server_homepage("homepage")
      footer$server_footer("footer")
      subscribe$server_subscribe("home")
      user_create$server_user_create("user_create")
      sidebar$server_sidebar("sidebar")
    }
  )
}
