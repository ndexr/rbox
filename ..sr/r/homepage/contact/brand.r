#' @export
ui_brand <- function() {
  box::use(shiny = shiny[withTags])
  withTags(
    div(
      class = "py-5",
      div(
        class = "",
        div(
          class = "row align-items-center",
          div(
            class = "col-md-3 col-sm-6 my-3",
            a(href = "#!", img(class = "img-fluid img-brand d-block mx-auto", src = "homepage/img/logos/microsoft.svg", alt = "...", `aria-label` = "Microsoft Logo")),
          ),
          div(
            class = "col-md-3 col-sm-6 my-3",
            a(href = "#!", img(class = "img-fluid img-brand d-block mx-auto", src = "homepage/img/logos/google.svg", alt = "...", `aria-label` = "Google Logo")),
          ),
          div(
            class = "col-md-3 col-sm-6 my-3",
            a(href = "#!", img(class = "img-fluid img-brand d-block mx-auto", src = "homepage/img/logos/facebook.svg", alt = "...", `aria-label` = "Facebook Logo")),
          ),
          div(
            class = "col-md-3 col-sm-6 my-3",
            a(href = "#!", img(class = "img-fluid img-brand d-block mx-auto", src = "homepage/img/logos/ibm.svg", alt = "...", `aria-label` = "IBM Logo")),
          )
        )
      )
    )
  )
}
