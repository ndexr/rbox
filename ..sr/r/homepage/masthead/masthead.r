#' @export
ui_masthead <- function(heading, subheading, href_id, href_nm) {
  box::use(shiny[withTags])
  withTags(
    header(
      class = "masthead",
      div(
        class = "",
        div(class = "masthead-subheading", subheading),
        div(class = "masthead-heading text-uppercase", heading),
        a(class = "btn  btn-xl text-uppercase", href = href_id, href_nm)
      )
    )
  )
}
