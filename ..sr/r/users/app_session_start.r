#' @export
ui_app_session_start <- function(id = "app_session_start") {
  box::use(shiny = shiny[tags, HTML])
  box::use(. / console_users)
  ns <- shiny$NS(id)
  box::use(.. / inputs / inputs)
  box::use(plotly)

  shiny$uiOutput(ns("ui"))
}

#' @export
server_app_session_start <- function(id = "app_session_start") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(. / console_users)
    box::use(.. / connections / postgres)
    box::use(.. / connections / redis)
    box::use(.. / connections / ssh)
    box::use(lubridate)
    box::use(dplyr)
    box::use(ggplot2)
    box::use(plotly)
    box::use(stringr)
    box::use(forcats)
    box::use(.. / inputs / inputs)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$app_session_start <- shiny$renderPlot({
        browser()
        app_session_start <- postgres$table_get("app_session_start")
        app_session_start$time <- lubridate$with_tz(
          lubridate$as_datetime(app_session_start$time),
          "America/Denver"
        )

        app_session_start <- dplyr$filter(
          app_session_start,
          !stringr$str_detect(app_session_start$email, "fdrennan"),
          !stringr$str_detect(app_session_start$email, "fr904103"),
          !stringr$str_detect(app_session_start$email, "drennanfreddy"),
          app_session_start$email != "",
          app_session_start$time >= Sys.Date() - 7
        )
        app_session_start$time <- lubridate$floor_date(app_session_start$time, "hour")

        app_session_start <- dplyr$group_by(app_session_start, email)
        app_session_start <- dplyr$summarise(app_session_start, n = dplyr$n())
        app_session_start <- dplyr$arrange(app_session_start, n)
        app_session_start <- dplyr$mutate(
          app_session_start,
          email = forcats$as_factor(email)
        )
        g <-
          ggplot2$ggplot(app_session_start) +
          ggplot2$aes(email, n, fill = email) +
          ggplot2$geom_col() +
          ggplot2$coord_flip() +
          ggplot2$theme(text = ggplot2$element_text(size = 20)) +
          ggplot2$theme(legend.position = "none")
        g
      })
      output$ui <- shiny$renderUI({
        inputs$card_collapse(
          ns,
          title = "Logins",
          body = shiny$plotOutput(ns("app_session_start"))
        )
      })
    }
  )
}
