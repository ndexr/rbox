#' @export
ui_aws_settings <- function(id = "aws_settings") {
  {
    box::use(shiny = shiny[tags, HTML])
  }

  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_aws_settings <- function(id = "aws_settings", ns_common_store_user, show_rsa = TRUE) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / .. / inputs / inputs)
    box::use(. / manage_key_file)
    box::use(. / manage_security_group)
    box::use(.. / secrets / secrets)
  }


  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      shiny$observe({
        has_secret <- secrets$secret_exists(ns_common_store_user)
        if (has_secret) {
          output$ui <- shiny$renderUI({
            tags$div(
              manage_key_file$ui_manage_key_file(ns("manage_key_file")),
              manage_security_group$ui_manage_security_group(ns("manage_security_group"), ns_common_store_user)
            )
          })
        } else {
          shiny$invalidateLater(5000)
        }
      })
      manage_key_file$server_manage_key_file("manage_key_file", ns_common_store_user, show_rsa = show_rsa)
      manage_security_group$server_manage_security_group("manage_security_group", ns_common_store_user)
    }
  )
}
