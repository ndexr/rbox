#' @export
secret_create <- function(Name, SecretString) {
  {
    box::use(.. / client)
    box::use(cli)
  }
  cli$cli_alert_info("creating secret")
  client <- client$client("secretsmanager")
  secret <- client$create_secret(Name = Name, SecretString = SecretString)
}

#' @export
secret_update <- function(Name, SecretString) {
  box::use(.. / client)
  box::use(cli)
  cli$cli_alert_info("updating secret")
  client <- client$client("secretsmanager")
  client$update_secret(SecretId = Name, SecretString = SecretString)
}


#' @export
secret_get <- function(Name) {
  box::use(.. / client)
  box::use(cli)
  box::use(glue)
  cli$cli_alert_info(glue$glue("secret {Name}"))
  client <- client$client("secretsmanager")
  client$get_secret_value(SecretId = Name)$SecretString
}

#' @export
secret_delete <- function(Name) {
  box::use(.. / client)
  box::use(cli)

  cli$cli_alert_info("deleting secret")
  client <- client$client("secretsmanager")

  client$delete_secret(
    SecretId = Name,
    ForceDeleteWithoutRecovery = TRUE
  )
}

#' @export
secret_describe <- function(Name) {
  box::use(.. / client)
  box::use(cli)

  cli$cli_alert_info("describe secret")
  client <- client$client("secretsmanager")
  client$describe_secret(SecretId = Name)
}

#' @export
secret_password_create <- function(PasswordLength = 15L) {
  box::use(.. / client)
  box::use(cli)

  cli$cli_alert_info("create secret")
  client <- client$client("secretsmanager")
  client$get_random_password(PasswordLength = PasswordLength)
}

#' @export
secrets_list <- function() {
  {
    box::use(.. / client)
    box::use(purrr)
    box::use(cli)
  }

  client <- client$client("secretsmanager")
  secrets <- client$list_secrets()
  cli$cli_alert_info("list secret")
  out <- purrr$map_dfr(
    secrets$SecretList,
    function(x) {
      data.frame(
        Name = x$Name,
        LastChangedDate = as.character(x$LastChangedDate)
      )
    }
  )

  out
}


#' @export
secret_exists <- function(ns_common_store_user) {
  box::use(. / secrets)
  secretReturn <- tryCatch(
    {
      secrets$secret_get(ns_common_store_user("master"))
      FALSE
    },
    error = function(err) {
      "none"
    }
  )

  ifelse(secretReturn == "none", FALSE, TRUE)
}
