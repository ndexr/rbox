#' @export
send_email <- function(from = "fdrennan@ndexr.com",
                       to = "fdrennan@ndexr.com",
                       subject = "Welcome to ndexr",
                       body = NULL,
                       key = NULL,
                       data_list = NULL,
                       stripe_env = "STRIPE_LIVE_SK") {
  {
    box::use(gmailr)
    box::use(readr)
    box::use(jsonlite)
    box::use(glue[glue])
  }


  gmailr$gm_auth_configure(path = "../.google/credentials.json")
  gmailr$gm_auth(email = TRUE, cache = "../.secret")

  test_email <-
    gmailr$gm_mime() |>
    gmailr$gm_to(to) |>
    gmailr$gm_from(from) |>
    gmailr$gm_subject(subject) |>
    gmailr$gm_html_body(body)
  gmailr$gm_send_message(test_email)
}

if (FALSE) {
  # https://gmailr.r-lib.org/
  {
    box::use(. / gmail)
    box::use(shiny = shiny[tags, HTML])
    box::reload(gmail)
    box::use(gmailr)
    box::use(uuid)
    box::use(.. / payments / stripe)
    box::use(jsonlite)
  }

  customers <- stripe$stripe_customers(live = TRUE)
  gmail$send_email(
    from = "fdrennan@ndexr.com",
    to = "fdrennan@ndexr.com",
    subject = "Welcome to ndexr",
    body = paste0(as.character(tags$div(
      shiny$tags$h2("Thank for for signing up for the waitlist! Here's what happens next."),
      shiny$tags$h4("You now have access to ndexr, a tool for deploying and managing R Applications on AWS.")
    ))),
    key = NULL,
    data_list = NULL
  )
}
