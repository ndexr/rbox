#' @export
ui_theme <- function(id) {
  box::use(shiny = shiny[tags, HTML])
  # tags$div(
  #   class = "container",
  #   tags$div(
  #     class = "bs-docs-section clearfix",
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$div(
  #           class = "page-header",
  #           tags$h1(
  #             id = "navbars",
  #             "Navbars"
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$nav(
  #             class = "navbar navbar-expand-lg navbar-dark bg-primary",
  #             tags$div(
  #               class = "",
  #               tags$a(
  #                 class = "navbar-brand",
  #                 href = "#",
  #                 "Navbar"
  #               ),
  #               tags$button(
  #                 class = "navbar-toggler",
  #                 type = "button",
  #                 `data-bs-toggle` = "collapse",
  #                 `data-bs-target` = "#navbarColor01",
  #                 `aria-controls` = "navbarColor01",
  #                 `aria-expanded` = "false",
  #                 `aria-label` = "Toggle navigation",
  #                 tags$span(
  #                   class = "navbar-toggler-icon"
  #                 )
  #               ),
  #               tags$div(
  #                 class = "collapse navbar-collapse",
  #                 id = "navbarColor01",
  #                 tags$ul(
  #                   class = "navbar-nav me-auto",
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link active",
  #                       href = "#",
  #                       "Home",
  #                       tags$span(
  #                         class = "visually-hidden",
  #                         "(current)"
  #                       )
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link",
  #                       href = "#",
  #                       "Features"
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link",
  #                       href = "#",
  #                       "Pricing"
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link",
  #                       href = "#",
  #                       "About"
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item dropdown",
  #                     tags$a(
  #                       class = "nav-link dropdown-toggle",
  #                       `data-bs-toggle` = "dropdown",
  #                       href = "#",
  #                       role = "button",
  #                       `aria-haspopup` = "true",
  #                       `aria-expanded` = "false",
  #                       "Dropdown"
  #                     ),
  #                     tags$div(
  #                       class = "dropdown-menu",
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Action"
  #                       ),
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Another action"
  #                       ),
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Something else here"
  #                       ),
  #                       tags$div(
  #                         class = "dropdown-divider"
  #                       ),
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Separated link"
  #                       )
  #                     )
  #                   )
  #                 ),
  #                 tags$form(
  #                   class = "d-flex",
  #                   tags$input(
  #                     class = "form-control me-sm-2",
  #                     type = "search",
  #                     placeholder = "Search"
  #                   ),
  #                   tags$button(
  #                     class = "btn btn-secondary my-2 my-sm-0",
  #                     type = "submit",
  #                     "Search"
  #                   )
  #                 )
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$nav(
  #             class = "navbar navbar-expand-lg navbar-dark bg-dark",
  #             tags$div(
  #               class = "",
  #               tags$a(
  #                 class = "navbar-brand",
  #                 href = "#",
  #                 "Navbar"
  #               ),
  #               tags$button(
  #                 class = "navbar-toggler",
  #                 type = "button",
  #                 `data-bs-toggle` = "collapse",
  #                 `data-bs-target` = "#navbarColor02",
  #                 `aria-controls` = "navbarColor02",
  #                 `aria-expanded` = "false",
  #                 `aria-label` = "Toggle navigation",
  #                 tags$span(
  #                   class = "navbar-toggler-icon"
  #                 )
  #               ),
  #               tags$div(
  #                 class = "collapse navbar-collapse",
  #                 id = "navbarColor02",
  #                 tags$ul(
  #                   class = "navbar-nav me-auto",
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link active",
  #                       href = "#",
  #                       "Home",
  #                       tags$span(
  #                         class = "visually-hidden",
  #                         "(current)"
  #                       )
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link",
  #                       href = "#",
  #                       "Features"
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link",
  #                       href = "#",
  #                       "Pricing"
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link",
  #                       href = "#",
  #                       "About"
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item dropdown",
  #                     tags$a(
  #                       class = "nav-link dropdown-toggle",
  #                       `data-bs-toggle` = "dropdown",
  #                       href = "#",
  #                       role = "button",
  #                       `aria-haspopup` = "true",
  #                       `aria-expanded` = "false",
  #                       "Dropdown"
  #                     ),
  #                     tags$div(
  #                       class = "dropdown-menu",
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Action"
  #                       ),
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Another action"
  #                       ),
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Something else here"
  #                       ),
  #                       tags$div(
  #                         class = "dropdown-divider"
  #                       ),
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Separated link"
  #                       )
  #                     )
  #                   )
  #                 ),
  #                 tags$form(
  #                   class = "d-flex",
  #                   tags$input(
  #                     class = "form-control me-sm-2",
  #                     type = "search",
  #                     placeholder = "Search"
  #                   ),
  #                   tags$button(
  #                     class = "btn btn-secondary my-2 my-sm-0",
  #                     type = "submit",
  #                     "Search"
  #                   )
  #                 )
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$nav(
  #             class = "navbar navbar-expand-lg navbar-light bg-light",
  #             tags$div(
  #               class = "",
  #               tags$a(
  #                 class = "navbar-brand",
  #                 href = "#",
  #                 "Navbar"
  #               ),
  #               tags$button(
  #                 class = "navbar-toggler",
  #                 type = "button",
  #                 `data-bs-toggle` = "collapse",
  #                 `data-bs-target` = "#navbarColor03",
  #                 `aria-controls` = "navbarColor03",
  #                 `aria-expanded` = "false",
  #                 `aria-label` = "Toggle navigation",
  #                 tags$span(
  #                   class = "navbar-toggler-icon"
  #                 )
  #               ),
  #               tags$div(
  #                 class = "collapse navbar-collapse",
  #                 id = "navbarColor03",
  #                 tags$ul(
  #                   class = "navbar-nav me-auto",
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link active",
  #                       href = "#",
  #                       "Home",
  #                       tags$span(
  #                         class = "visually-hidden",
  #                         "(current)"
  #                       )
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link",
  #                       href = "#",
  #                       "Features"
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link",
  #                       href = "#",
  #                       "Pricing"
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item",
  #                     tags$a(
  #                       class = "nav-link",
  #                       href = "#",
  #                       "About"
  #                     )
  #                   ),
  #                   tags$li(
  #                     class = "nav-item dropdown",
  #                     tags$a(
  #                       class = "nav-link dropdown-toggle",
  #                       `data-bs-toggle` = "dropdown",
  #                       href = "#",
  #                       role = "button",
  #                       `aria-haspopup` = "true",
  #                       `aria-expanded` = "false",
  #                       "Dropdown"
  #                     ),
  #                     tags$div(
  #                       class = "dropdown-menu",
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Action"
  #                       ),
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Another action"
  #                       ),
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Something else here"
  #                       ),
  #                       tags$div(
  #                         class = "dropdown-divider"
  #                       ),
  #                       tags$a(
  #                         class = "dropdown-item",
  #                         href = "#",
  #                         "Separated link"
  #                       )
  #                     )
  #                   )
  #                 ),
  #                 tags$form(
  #                   class = "d-flex",
  #                   tags$input(
  #                     class = "form-control me-sm-2",
  #                     type = "search",
  #                     placeholder = "Search"
  #                   ),
  #                   tags$button(
  #                     class = "btn btn-secondary my-2 my-sm-0",
  #                     type = "submit",
  #                     "Search"
  #                   )
  #                 )
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     )
  #   ),
  #   tags$div(
  #     class = "bs-docs-section",
  #     tags$div(
  #       class = "page-header",
  #       tags$div(
  #         class = "row",
  #         tags$div(
  #           class = "col-lg-12",
  #           tags$h1(
  #             id = "buttons",
  #             "Buttons"
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-7",
  #         tags$p(
  #           class = "bs-component",
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-primary",
  #             "Primary"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-secondary",
  #             "Secondary"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-success",
  #             "Success"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-info",
  #             "Info"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-warning",
  #             "Warning"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-danger",
  #             "Danger"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-light",
  #             "Light"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-dark",
  #             "Dark"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-link",
  #             "Link"
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$p(
  #           class = "bs-component",
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-primary disabled",
  #             "Primary"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-secondary disabled",
  #             "Secondary"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-success disabled",
  #             "Success"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-info disabled",
  #             "Info"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-warning disabled",
  #             "Warning"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-danger disabled",
  #             "Danger"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-light disabled",
  #             "Light"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-dark disabled",
  #             "Dark"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-link disabled",
  #             "Link"
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$p(
  #           class = "bs-component",
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-outline-primary",
  #             "Primary"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-outline-secondary",
  #             "Secondary"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-outline-success",
  #             "Success"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-outline-info",
  #             "Info"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-outline-warning",
  #             "Warning"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-outline-danger",
  #             "Danger"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-outline-light",
  #             "Light"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-outline-dark",
  #             "Dark"
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "btn-group",
  #             role = "group",
  #             `aria-label` = "Button group with nested dropdown",
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-primary",
  #               "Primary"
  #             ),
  #             tags$div(
  #               class = "btn-group",
  #               role = "group",
  #               tags$button(
  #                 id = "btnGroupDrop1",
  #                 type = "button",
  #                 class = "btn btn-primary dropdown-toggle",
  #                 `data-bs-toggle` = "dropdown",
  #                 `aria-haspopup` = "true",
  #                 `aria-expanded` = "false"
  #               ),
  #               tags$div(
  #                 class = "dropdown-menu",
  #                 `aria-labelledby` = "btnGroupDrop1",
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Dropdown link"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Dropdown link"
  #                 )
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "btn-group",
  #             role = "group",
  #             `aria-label` = "Button group with nested dropdown",
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-success",
  #               "Success"
  #             ),
  #             tags$div(
  #               class = "btn-group",
  #               role = "group",
  #               tags$button(
  #                 id = "btnGroupDrop2",
  #                 type = "button",
  #                 class = "btn btn-success dropdown-toggle",
  #                 `data-bs-toggle` = "dropdown",
  #                 `aria-haspopup` = "true",
  #                 `aria-expanded` = "false"
  #               ),
  #               tags$div(
  #                 class = "dropdown-menu",
  #                 `aria-labelledby` = "btnGroupDrop2",
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Dropdown link"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Dropdown link"
  #                 )
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "btn-group",
  #             role = "group",
  #             `aria-label` = "Button group with nested dropdown",
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-info",
  #               "Info"
  #             ),
  #             tags$div(
  #               class = "btn-group",
  #               role = "group",
  #               tags$button(
  #                 id = "btnGroupDrop3",
  #                 type = "button",
  #                 class = "btn btn-info dropdown-toggle",
  #                 `data-bs-toggle` = "dropdown",
  #                 `aria-haspopup` = "true",
  #                 `aria-expanded` = "false"
  #               ),
  #               tags$div(
  #                 class = "dropdown-menu",
  #                 `aria-labelledby` = "btnGroupDrop3",
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Dropdown link"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Dropdown link"
  #                 )
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "btn-group",
  #             role = "group",
  #             `aria-label` = "Button group with nested dropdown",
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-danger",
  #               "Danger"
  #             ),
  #             tags$div(
  #               class = "btn-group",
  #               role = "group",
  #               tags$button(
  #                 id = "btnGroupDrop4",
  #                 type = "button",
  #                 class = "btn btn-danger dropdown-toggle",
  #                 `data-bs-toggle` = "dropdown",
  #                 `aria-haspopup` = "true",
  #                 `aria-expanded` = "false"
  #               ),
  #               tags$div(
  #                 class = "dropdown-menu",
  #                 `aria-labelledby` = "btnGroupDrop4",
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Dropdown link"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Dropdown link"
  #                 )
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-primary btn-lg",
  #             "Large button"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-primary",
  #             "Default button"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-primary btn-sm",
  #             "Small button"
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-5",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "d-grid gap-2",
  #             tags$button(
  #               class = "btn btn-lg btn-primary",
  #               type = "button",
  #               "Block button"
  #             ),
  #             tags$button(
  #               class = "btn btn-lg btn-primary",
  #               type = "button",
  #               "Block button"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component mb-3",
  #           tags$div(
  #             class = "btn-group",
  #             role = "group",
  #             `aria-label` = "Basic checkbox toggle button group",
  #             tags$input(
  #               type = "checkbox",
  #               class = "btn-check",
  #               id = "btncheck1",
  #               checked = "",
  #               autocomplete = "off"
  #             ),
  #             tags$label(
  #               class = "btn btn-primary",
  #               `for` = "btncheck1",
  #               "Checkbox 1"
  #             ),
  #             tags$input(
  #               type = "checkbox",
  #               class = "btn-check",
  #               id = "btncheck2",
  #               autocomplete = "off"
  #             ),
  #             tags$label(
  #               class = "btn btn-primary",
  #               `for` = "btncheck2",
  #               "Checkbox 2"
  #             ),
  #             tags$input(
  #               type = "checkbox",
  #               class = "btn-check",
  #               id = "btncheck3",
  #               autocomplete = "off"
  #             ),
  #             tags$label(
  #               class = "btn btn-primary",
  #               `for` = "btncheck3",
  #               "Checkbox 3"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component mb-3",
  #           tags$div(
  #             class = "btn-group",
  #             role = "group",
  #             `aria-label` = "Basic radio toggle button group",
  #             tags$input(
  #               type = "radio",
  #               class = "btn-check",
  #               name = "btnradio",
  #               id = "btnradio1",
  #               autocomplete = "off",
  #               checked = ""
  #             ),
  #             tags$label(
  #               class = "btn btn-outline-primary",
  #               `for` = "btnradio1",
  #               "Radio 1"
  #             ),
  #             tags$input(
  #               type = "radio",
  #               class = "btn-check",
  #               name = "btnradio",
  #               id = "btnradio2",
  #               autocomplete = "off",
  #               checked = ""
  #             ),
  #             tags$label(
  #               class = "btn btn-outline-primary",
  #               `for` = "btnradio2",
  #               "Radio 2"
  #             ),
  #             tags$input(
  #               type = "radio",
  #               class = "btn-check",
  #               name = "btnradio",
  #               id = "btnradio3",
  #               autocomplete = "off",
  #               checked = ""
  #             ),
  #             tags$label(
  #               class = "btn btn-outline-primary",
  #               `for` = "btnradio3",
  #               "Radio 3"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "btn-group-vertical",
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-primary",
  #               "Button"
  #             ),
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-primary",
  #               "Button"
  #             ),
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-primary",
  #               "Button"
  #             ),
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-primary",
  #               "Button"
  #             ),
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-primary",
  #               "Button"
  #             ),
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-primary",
  #               "Button"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component mb-3",
  #           tags$div(
  #             class = "btn-group",
  #             role = "group",
  #             `aria-label` = "Basic example",
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-secondary",
  #               "Left"
  #             ),
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-secondary",
  #               "Middle"
  #             ),
  #             tags$button(
  #               type = "button",
  #               class = "btn btn-secondary",
  #               "Right"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component mb-3",
  #           tags$div(
  #             class = "btn-toolbar",
  #             role = "toolbar",
  #             `aria-label` = "Toolbar with button groups",
  #             tags$div(
  #               class = "btn-group me-2",
  #               role = "group",
  #               `aria-label` = "First group",
  #               tags$button(
  #                 type = "button",
  #                 class = "btn btn-secondary",
  #                 "1"
  #               ),
  #               tags$button(
  #                 type = "button",
  #                 class = "btn btn-secondary",
  #                 "2"
  #               ),
  #               tags$button(
  #                 type = "button",
  #                 class = "btn btn-secondary",
  #                 "3"
  #               ),
  #               tags$button(
  #                 type = "button",
  #                 class = "btn btn-secondary",
  #                 "4"
  #               )
  #             ),
  #             tags$div(
  #               class = "btn-group me-2",
  #               role = "group",
  #               `aria-label` = "Second group",
  #               tags$button(
  #                 type = "button",
  #                 class = "btn btn-secondary",
  #                 "5"
  #               ),
  #               tags$button(
  #                 type = "button",
  #                 class = "btn btn-secondary",
  #                 "6"
  #               ),
  #               tags$button(
  #                 type = "button",
  #                 class = "btn btn-secondary",
  #                 "7"
  #               )
  #             ),
  #             tags$div(
  #               class = "btn-group",
  #               role = "group",
  #               `aria-label` = "Third group",
  #               tags$button(
  #                 type = "button",
  #                 class = "btn btn-secondary",
  #                 "8"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     )
  #   ),
  #   tags$div(
  #     class = "bs-docs-section",
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$div(
  #           class = "page-header",
  #           tags$h1(
  #             id = "typography",
  #             "Typography"
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$h1(
  #             "Heading 1"
  #           ),
  #           tags$h2(
  #             "Heading 2"
  #           ),
  #           tags$h3(
  #             "Heading 3"
  #           ),
  #           tags$h4(
  #             "Heading 4"
  #           ),
  #           tags$h5(
  #             "Heading 5"
  #           ),
  #           tags$h6(
  #             "Heading 6"
  #           ),
  #           tags$h3(
  #             "Heading",
  #             tags$small(
  #               class = "text-muted",
  #               "with muted text"
  #             )
  #           ),
  #           tags$p(
  #             class = "lead",
  #             "Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor."
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$h2(
  #             "Example body text"
  #           ),
  #           tags$p(
  #             "Nullam quis risus eget",
  #             tags$a(
  #               href = "#",
  #               "urna mollis ornare"
  #             ),
  #             "vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula."
  #           ),
  #           tags$p(
  #             tags$small(
  #               "This line of text is meant to be treated as fine print."
  #             )
  #           ),
  #           tags$p(
  #             "The following is",
  #             tags$strong(
  #               "rendered as bold text"
  #             ),
  #             "."
  #           ),
  #           tags$p(
  #             "The following is",
  #             tags$em(
  #               "rendered as italicized text"
  #             ),
  #             "."
  #           ),
  #           tags$p(
  #             "An abbreviation of the word attribute is",
  #             tags$abbr(
  #               title = "attribute",
  #               "attr"
  #             ),
  #             "."
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$h2(
  #             "Emphasis classes"
  #           ),
  #           tags$p(
  #             class = "text-muted",
  #             "Fusce dapibus, tellus ac cursus commodo, tortor mauris nibh."
  #           ),
  #           tags$p(
  #             class = "text-primary",
  #             "Nullam id dolor id nibh ultricies vehicula ut id elit."
  #           ),
  #           tags$p(
  #             class = "text-secondary",
  #             "Pellentesque ornare sem lacinia quam venenatis vestibulum."
  #           ),
  #           tags$p(
  #             class = "text-warning",
  #             "Etiam porta sem malesuada magna mollis euismod."
  #           ),
  #           tags$p(
  #             class = "text-danger",
  #             "Donec ullamcorper nulla non metus auctor fringilla."
  #           ),
  #           tags$p(
  #             class = "text-success",
  #             "Duis mollis, est non commodo luctus, nisi erat porttitor ligula."
  #           ),
  #           tags$p(
  #             class = "text-info",
  #             "Maecenas sed diam eget risus varius blandit sit amet non magna."
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$h2(
  #           id = "type-blockquotes",
  #           "Blockquotes"
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$figure(
  #             tags$blockquote(
  #               class = "blockquote",
  #               tags$p(
  #                 class = "mb-0",
  #                 "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante."
  #               )
  #             ),
  #             tags$figcaption(
  #               class = "blockquote-footer",
  #               "Someone famous in",
  #               tags$cite(
  #                 title = "Source Title",
  #                 "Source Title"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$figure(
  #             class = "text-center",
  #             tags$blockquote(
  #               class = "blockquote",
  #               tags$p(
  #                 class = "mb-0",
  #                 "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante."
  #               )
  #             ),
  #             tags$figcaption(
  #               class = "blockquote-footer",
  #               "Someone famous in",
  #               tags$cite(
  #                 title = "Source Title",
  #                 "Source Title"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$figure(
  #             class = "text-end",
  #             tags$blockquote(
  #               class = "blockquote",
  #               tags$p(
  #                 class = "mb-0",
  #                 "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante."
  #               )
  #             ),
  #             tags$figcaption(
  #               class = "blockquote-footer",
  #               "Someone famous in",
  #               tags$cite(
  #                 title = "Source Title",
  #                 "Source Title"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     )
  #   ),
  #   tags$div(
  #     class = "bs-docs-section",
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$div(
  #           class = "page-header",
  #           tags$h1(
  #             id = "tables",
  #             "Tables"
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$table(
  #             class = "table table-hover",
  #             tags$thead(
  #               tags$tr(
  #                 tags$th(
  #                   scope = "col",
  #                   "Type"
  #                 ),
  #                 tags$th(
  #                   scope = "col",
  #                   "Column heading"
  #                 ),
  #                 tags$th(
  #                   scope = "col",
  #                   "Column heading"
  #                 ),
  #                 tags$th(
  #                   scope = "col",
  #                   "Column heading"
  #                 )
  #               )
  #             ),
  #             tags$tbody(
  #               tags$tr(
  #                 class = "table-active",
  #                 tags$th(
  #                   scope = "row",
  #                   "Active"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 )
  #               ),
  #               tags$tr(
  #                 tags$th(
  #                   scope = "row",
  #                   "Default"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 )
  #               ),
  #               tags$tr(
  #                 class = "table-primary",
  #                 tags$th(
  #                   scope = "row",
  #                   "Primary"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 )
  #               ),
  #               tags$tr(
  #                 class = "table-secondary",
  #                 tags$th(
  #                   scope = "row",
  #                   "Secondary"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 )
  #               ),
  #               tags$tr(
  #                 class = "table-success",
  #                 tags$th(
  #                   scope = "row",
  #                   "Success"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 )
  #               ),
  #               tags$tr(
  #                 class = "table-danger",
  #                 tags$th(
  #                   scope = "row",
  #                   "Danger"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 )
  #               ),
  #               tags$tr(
  #                 class = "table-warning",
  #                 tags$th(
  #                   scope = "row",
  #                   "Warning"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 )
  #               ),
  #               tags$tr(
  #                 class = "table-info",
  #                 tags$th(
  #                   scope = "row",
  #                   "Info"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 )
  #               ),
  #               tags$tr(
  #                 class = "table-light",
  #                 tags$th(
  #                   scope = "row",
  #                   "Light"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 )
  #               ),
  #               tags$tr(
  #                 class = "table-dark",
  #                 tags$th(
  #                   scope = "row",
  #                   "Dark"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 ),
  #                 tags$td(
  #                   "Column content"
  #                 )
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     )
  #   ),
  #   tags$div(
  #     class = "bs-docs-section",
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$div(
  #           class = "page-header",
  #           tags$h1(
  #             id = "forms",
  #             "Forms"
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-6",
  #         tags$div(
  #           class = "bs-component",
  #           tags$form(
  #             tags$fieldset(
  #               tags$legend(
  #                 "Legend"
  #               ),
  #               tags$div(
  #                 class = "form-group row",
  #                 tags$label(
  #                   `for` = "staticEmail",
  #                   class = "col-sm-2 col-form-label",
  #                   "Email"
  #                 ),
  #                 tags$div(
  #                   class = "col-sm-10",
  #                   tags$input(
  #                     type = "text",
  #                     readonly = "",
  #                     class = "form-control-plaintext",
  #                     id = "staticEmail",
  #                     value = "email@example.com"
  #                   )
  #                 )
  #               ),
  #               tags$div(
  #                 class = "form-group",
  #                 tags$label(
  #                   `for` = "exampleInputEmail1",
  #                   "Email address"
  #                 ),
  #                 tags$input(
  #                   type = "email",
  #                   class = "form-control",
  #                   id = "exampleInputEmail1",
  #                   `aria-describedby` = "emailHelp",
  #                   placeholder = "Enter email"
  #                 ),
  #                 tags$small(
  #                   id = "emailHelp",
  #                   class = "form-text text-muted",
  #                   "We'll never share your email with anyone else."
  #                 )
  #               ),
  #               tags$div(
  #                 class = "form-group",
  #                 tags$label(
  #                   `for` = "exampleInputPassword1",
  #                   "Password"
  #                 ),
  #                 tags$input(
  #                   type = "password",
  #                   class = "form-control",
  #                   id = "exampleInputPassword1",
  #                   placeholder = "Password"
  #                 )
  #               ),
  #               tags$div(
  #                 class = "form-group",
  #                 tags$label(
  #                   `for` = "exampleSelect1",
  #                   "Example select"
  #                 ),
  #                 tags$select(
  #                   class = "form-select",
  #                   id = "exampleSelect1",
  #                   tags$option(
  #                     "1"
  #                   ),
  #                   tags$option(
  #                     "2"
  #                   ),
  #                   tags$option(
  #                     "3"
  #                   ),
  #                   tags$option(
  #                     "4"
  #                   ),
  #                   tags$option(
  #                     "5"
  #                   )
  #                 )
  #               ),
  #               tags$div(
  #                 class = "form-group",
  #                 tags$label(
  #                   `for` = "exampleSelect2",
  #                   "Example multiple select"
  #                 ),
  #                 tags$select(
  #                   multiple = "",
  #                   class = "form-select",
  #                   id = "exampleSelect2",
  #                   tags$option(
  #                     "1"
  #                   ),
  #                   tags$option(
  #                     "2"
  #                   ),
  #                   tags$option(
  #                     "3"
  #                   ),
  #                   tags$option(
  #                     "4"
  #                   ),
  #                   tags$option(
  #                     "5"
  #                   )
  #                 )
  #               ),
  #               tags$div(
  #                 class = "form-group",
  #                 tags$label(
  #                   `for` = "exampleTextarea",
  #                   "Example textarea"
  #                 ),
  #                 tags$textarea(
  #                   class = "form-control",
  #                   id = "exampleTextarea",
  #                   rows = "3"
  #                 )
  #               ),
  #               tags$div(
  #                 class = "form-group",
  #                 tags$label(
  #                   `for` = "formFile",
  #                   class = "form-label",
  #                   "Default file input example"
  #                 ),
  #                 tags$input(
  #                   class = "form-control",
  #                   type = "file",
  #                   id = "formFile"
  #                 )
  #               ),
  #               tags$fieldset(
  #                 class = "form-group",
  #                 tags$legend(
  #                   class = "mt-4",
  #                   "Radio buttons"
  #                 ),
  #                 tags$div(
  #                   class = "form-check",
  #                   tags$input(
  #                     class = "form-check-input",
  #                     type = "radio",
  #                     name = "optionsRadios",
  #                     id = "optionsRadios1",
  #                     value = "option1",
  #                     checked = ""
  #                   ),
  #                   tags$label(
  #                     class = "form-check-label",
  #                     `for` = "optionsRadios1",
  #                     "Option one is this and that—be sure to include why it's great"
  #                   )
  #                 ),
  #                 tags$div(
  #                   class = "form-check",
  #                   tags$input(
  #                     class = "form-check-input",
  #                     type = "radio",
  #                     name = "optionsRadios",
  #                     id = "optionsRadios2",
  #                     value = "option2"
  #                   ),
  #                   tags$label(
  #                     class = "form-check-label",
  #                     `for` = "optionsRadios2",
  #                     "Option two can be something else and selecting it will deselect option one"
  #                   )
  #                 ),
  #                 tags$div(
  #                   class = "form-check disabled",
  #                   tags$input(
  #                     class = "form-check-input",
  #                     type = "radio",
  #                     name = "optionsRadios",
  #                     id = "optionsRadios3",
  #                     value = "option3",
  #                     disabled = ""
  #                   ),
  #                   tags$label(
  #                     class = "form-check-label",
  #                     `for` = "optionsRadios3",
  #                     "Option three is disabled"
  #                   )
  #                 )
  #               ),
  #               tags$fieldset(
  #                 class = "form-group",
  #                 tags$legend(
  #                   "Checkboxes"
  #                 ),
  #                 tags$div(
  #                   class = "form-check",
  #                   tags$input(
  #                     class = "form-check-input",
  #                     type = "checkbox",
  #                     value = "",
  #                     id = "flexCheckDefault"
  #                   ),
  #                   tags$label(
  #                     class = "form-check-label",
  #                     `for` = "flexCheckDefault",
  #                     "Default checkbox"
  #                   )
  #                 ),
  #                 tags$div(
  #                   class = "form-check",
  #                   tags$input(
  #                     class = "form-check-input",
  #                     type = "checkbox",
  #                     value = "",
  #                     id = "flexCheckChecked",
  #                     checked = ""
  #                   ),
  #                   tags$label(
  #                     class = "form-check-label",
  #                     `for` = "flexCheckChecked",
  #                     "Checked checkbox"
  #                   )
  #                 )
  #               ),
  #               tags$fieldset(
  #                 class = "form-group",
  #                 tags$legend(
  #                   class = "mt-4",
  #                   "Switches"
  #                 ),
  #                 tags$div(
  #                   class = "form-check form-switch",
  #                   tags$input(
  #                     class = "form-check-input",
  #                     type = "checkbox",
  #                     id = "flexSwitchCheckDefault"
  #                   ),
  #                   tags$label(
  #                     class = "form-check-label",
  #                     `for` = "flexSwitchCheckDefault",
  #                     "Default switch checkbox input"
  #                   )
  #                 ),
  #                 tags$div(
  #                   class = "form-check form-switch",
  #                   tags$input(
  #                     class = "form-check-input",
  #                     type = "checkbox",
  #                     id = "flexSwitchCheckChecked",
  #                     checked = ""
  #                   ),
  #                   tags$label(
  #                     class = "form-check-label",
  #                     `for` = "flexSwitchCheckChecked",
  #                     "Checked switch checkbox input"
  #                   )
  #                 )
  #               ),
  #               tags$fieldset(
  #                 class = "form-group",
  #                 tags$legend(
  #                   "Ranges"
  #                 ),
  #                 tags$label(
  #                   `for` = "customRange1",
  #                   class = "form-label",
  #                   "Example range"
  #                 ),
  #                 tags$input(
  #                   type = "range",
  #                   class = "form-range",
  #                   id = "customRange1"
  #                 ),
  #                 tags$label(
  #                   `for` = "disabledRange",
  #                   class = "form-label",
  #                   "Disabled range"
  #                 ),
  #                 tags$input(
  #                   type = "range",
  #                   class = "form-range",
  #                   id = "disabledRange",
  #                   disabled = ""
  #                 ),
  #                 tags$label(
  #                   `for` = "customRange3",
  #                   class = "form-label",
  #                   "Example range"
  #                 ),
  #                 tags$input(
  #                   type = "range",
  #                   class = "form-range",
  #                   min = "0",
  #                   max = "5",
  #                   step = "0.5",
  #                   id = "customRange3"
  #                 )
  #               ),
  #               tags$button(
  #                 type = "submit",
  #                 class = "btn btn-primary",
  #                 "Submit"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4 offset-lg-1",
  #         tags$form(
  #           class = "bs-component",
  #           tags$div(
  #             class = "form-group",
  #             tags$fieldset(
  #               disabled = "",
  #               tags$label(
  #                 class = "form-label",
  #                 `for` = "disabledInput",
  #                 "Disabled input"
  #               ),
  #               tags$input(
  #                 class = "form-control",
  #                 id = "disabledInput",
  #                 type = "text",
  #                 placeholder = "Disabled input here...",
  #                 disabled = ""
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "form-group",
  #             tags$fieldset(
  #               tags$label(
  #                 class = "form-label",
  #                 `for` = "readOnlyInput",
  #                 "Readonly input"
  #               ),
  #               tags$input(
  #                 class = "form-control",
  #                 id = "readOnlyInput",
  #                 type = "text",
  #                 placeholder = "Readonly input here...",
  #                 readonly = ""
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "form-group has-success",
  #             tags$label(
  #               class = "form-label",
  #               `for` = "inputValid",
  #               "Valid input"
  #             ),
  #             tags$input(
  #               type = "text",
  #               value = "correct value",
  #               class = "form-control is-valid",
  #               id = "inputValid"
  #             ),
  #             tags$div(
  #               class = "valid-feedback",
  #               "Success! You've done it."
  #             )
  #           ),
  #           tags$div(
  #             class = "form-group has-danger",
  #             tags$label(
  #               class = "form-label",
  #               `for` = "inputInvalid",
  #               "Invalid input"
  #             ),
  #             tags$input(
  #               type = "text",
  #               value = "wrong value",
  #               class = "form-control is-invalid",
  #               id = "inputInvalid"
  #             ),
  #             tags$div(
  #               class = "invalid-feedback",
  #               "Sorry, that username's taken. Try another?"
  #             )
  #           ),
  #           tags$div(
  #             class = "form-group",
  #             tags$label(
  #               class = "col-form-label col-form-label-lg",
  #               `for` = "inputLarge",
  #               "Large input"
  #             ),
  #             tags$input(
  #               class = "form-control form-control-lg",
  #               type = "text",
  #               placeholder = ".form-control-lg",
  #               id = "inputLarge"
  #             )
  #           ),
  #           tags$div(
  #             class = "form-group",
  #             tags$label(
  #               class = "col-form-label",
  #               `for` = "inputDefault",
  #               "Default input"
  #             ),
  #             tags$input(
  #               type = "text",
  #               class = "form-control",
  #               placeholder = "Default input",
  #               id = "inputDefault"
  #             )
  #           ),
  #           tags$div(
  #             class = "form-group",
  #             tags$label(
  #               class = "col-form-label col-form-label-sm",
  #               `for` = "inputSmall",
  #               "Small input"
  #             ),
  #             tags$input(
  #               class = "form-control form-control-sm",
  #               type = "text",
  #               placeholder = ".form-control-sm",
  #               id = "inputSmall"
  #             )
  #           ),
  #           tags$div(
  #             class = "form-group",
  #             tags$label(
  #               class = "form-label mt-4",
  #               "Input addons"
  #             ),
  #             tags$div(
  #               class = "form-group",
  #               tags$div(
  #                 class = "input-group mb-3",
  #                 tags$span(
  #                   class = "input-group-text",
  #                   "$"
  #                 ),
  #                 tags$input(
  #                   type = "text",
  #                   class = "form-control",
  #                   `aria-label` = "Amount (to the nearest dollar)"
  #                 ),
  #                 tags$span(
  #                   class = "input-group-text",
  #                   ".00"
  #                 )
  #               ),
  #               tags$div(
  #                 class = "input-group mb-3",
  #                 tags$input(
  #                   type = "text",
  #                   class = "form-control",
  #                   placeholder = "Recipient's username",
  #                   `aria-label` = "Recipient's username",
  #                   `aria-describedby` = "button-addon2"
  #                 ),
  #                 tags$button(
  #                   class = "btn btn-primary",
  #                   type = "button",
  #                   id = "button-addon2",
  #                   "Button"
  #                 )
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "form-group",
  #             tags$label(
  #               class = "form-label mt-4",
  #               "Floating labels"
  #             ),
  #             tags$div(
  #               class = "form-floating mb-3",
  #               tags$input(
  #                 type = "email",
  #                 class = "form-control",
  #                 id = "floatingInput",
  #                 placeholder = "name@example.com"
  #               ),
  #               tags$label(
  #                 `for` = "floatingInput",
  #                 "Email address"
  #               )
  #             ),
  #             tags$div(
  #               class = "form-floating",
  #               tags$input(
  #                 type = "password",
  #                 class = "form-control",
  #                 id = "floatingPassword",
  #                 placeholder = "Password"
  #               ),
  #               tags$label(
  #                 `for` = "floatingPassword",
  #                 "Password"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     )
  #   ),
  #   tags$div(
  #     class = "bs-docs-section",
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$div(
  #           class = "page-header",
  #           tags$h1(
  #             id = "navs",
  #             "Navs"
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row mb-5",
  #       tags$div(
  #         class = "col-lg-6",
  #         tags$h2(
  #           id = "nav-tabs",
  #           "Tabs"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$ul(
  #             class = "nav nav-tabs",
  #             role = "tablist",
  #             tags$li(
  #               class = "nav-item",
  #               role = "presentation",
  #               tags$a(
  #                 class = "nav-link active",
  #                 `data-bs-toggle` = "tab",
  #                 href = "#home",
  #                 `aria-selected` = "true",
  #                 role = "tab",
  #                 "Home"
  #               )
  #             ),
  #             tags$li(
  #               class = "nav-item",
  #               role = "presentation",
  #               tags$a(
  #                 class = "nav-link",
  #                 `data-bs-toggle` = "tab",
  #                 href = "#profile",
  #                 `aria-selected` = "false",
  #                 tabindex = "-1",
  #                 role = "tab",
  #                 "Profile"
  #               )
  #             ),
  #             tags$li(
  #               class = "nav-item",
  #               role = "presentation",
  #               tags$a(
  #                 class = "nav-link disabled",
  #                 href = "#",
  #                 `aria-selected` = "false",
  #                 tabindex = "-1",
  #                 role = "tab",
  #                 "Disabled"
  #               )
  #             ),
  #             tags$li(
  #               class = "nav-item dropdown",
  #               tags$a(
  #                 class = "nav-link dropdown-toggle",
  #                 `data-bs-toggle` = "dropdown",
  #                 href = "#",
  #                 role = "button",
  #                 `aria-haspopup` = "true",
  #                 `aria-expanded` = "false",
  #                 "Dropdown"
  #               ),
  #               tags$div(
  #                 class = "dropdown-menu",
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Action"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Another action"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Something else here"
  #                 ),
  #                 tags$div(
  #                   class = "dropdown-divider"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Separated link"
  #                 )
  #               )
  #             )
  #           ),
  #           tags$div(
  #             id = "myTabContent",
  #             class = "tab-content",
  #             tags$div(
  #               class = "tab-pane fade show active",
  #               id = "home",
  #               role = "tabpanel",
  #               tags$p(
  #                 "Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui."
  #               )
  #             ),
  #             tags$div(
  #               class = "tab-pane fade",
  #               id = "profile",
  #               role = "tabpanel",
  #               tags$p(
  #                 "Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit."
  #               )
  #             ),
  #             tags$div(
  #               class = "tab-pane fade",
  #               id = "dropdown1",
  #               tags$p(
  #                 "Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork."
  #               )
  #             ),
  #             tags$div(
  #               class = "tab-pane fade",
  #               id = "dropdown2",
  #               tags$p(
  #                 "Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater."
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-6",
  #         tags$h2(
  #           id = "nav-pills",
  #           "Pills"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$ul(
  #             class = "nav nav-pills",
  #             tags$li(
  #               class = "nav-item",
  #               tags$a(
  #                 class = "nav-link active",
  #                 href = "#",
  #                 "Active"
  #               )
  #             ),
  #             tags$li(
  #               class = "nav-item dropdown",
  #               tags$a(
  #                 class = "nav-link dropdown-toggle",
  #                 `data-bs-toggle` = "dropdown",
  #                 href = "#",
  #                 role = "button",
  #                 `aria-haspopup` = "true",
  #                 `aria-expanded` = "false",
  #                 "Dropdown"
  #               ),
  #               tags$div(
  #                 class = "dropdown-menu",
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Action"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Another action"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Something else here"
  #                 ),
  #                 tags$div(
  #                   class = "dropdown-divider"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Separated link"
  #                 )
  #               )
  #             ),
  #             tags$li(
  #               class = "nav-item",
  #               tags$a(
  #                 class = "nav-link",
  #                 href = "#",
  #                 "Link"
  #               )
  #             ),
  #             tags$li(
  #               class = "nav-item",
  #               tags$a(
  #                 class = "nav-link disabled",
  #                 href = "#",
  #                 "Disabled"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$br(),
  #         tags$div(
  #           class = "bs-component",
  #           tags$ul(
  #             class = "nav nav-pills flex-column",
  #             tags$li(
  #               class = "nav-item",
  #               tags$a(
  #                 class = "nav-link active",
  #                 href = "#",
  #                 "Active"
  #               )
  #             ),
  #             tags$li(
  #               class = "nav-item dropdown",
  #               tags$a(
  #                 class = "nav-link dropdown-toggle",
  #                 `data-bs-toggle` = "dropdown",
  #                 href = "#",
  #                 role = "button",
  #                 `aria-haspopup` = "true",
  #                 `aria-expanded` = "false",
  #                 "Dropdown"
  #               ),
  #               tags$div(
  #                 class = "dropdown-menu",
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Action"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Another action"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Something else here"
  #                 ),
  #                 tags$div(
  #                   class = "dropdown-divider"
  #                 ),
  #                 tags$a(
  #                   class = "dropdown-item",
  #                   href = "#",
  #                   "Separated link"
  #                 )
  #               )
  #             ),
  #             tags$li(
  #               class = "nav-item",
  #               tags$a(
  #                 class = "nav-link",
  #                 href = "#",
  #                 "Link"
  #               )
  #             ),
  #             tags$li(
  #               class = "nav-item",
  #               tags$a(
  #                 class = "nav-link disabled",
  #                 href = "#",
  #                 "Disabled"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-6",
  #         tags$h2(
  #           id = "nav-breadcrumbs",
  #           "Breadcrumbs"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$ol(
  #             class = "breadcrumb",
  #             tags$li(
  #               class = "breadcrumb-item active",
  #               "Home"
  #             )
  #           ),
  #           tags$ol(
  #             class = "breadcrumb",
  #             tags$li(
  #               class = "breadcrumb-item",
  #               tags$a(
  #                 href = "#",
  #                 "Home"
  #               )
  #             ),
  #             tags$li(
  #               class = "breadcrumb-item active",
  #               "Library"
  #             )
  #           ),
  #           tags$ol(
  #             class = "breadcrumb",
  #             tags$li(
  #               class = "breadcrumb-item",
  #               tags$a(
  #                 href = "#",
  #                 "Home"
  #               )
  #             ),
  #             tags$li(
  #               class = "breadcrumb-item",
  #               tags$a(
  #                 href = "#",
  #                 "Library"
  #               )
  #             ),
  #             tags$li(
  #               class = "breadcrumb-item active",
  #               "Data"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-6",
  #         tags$h2(
  #           id = "pagination",
  #           "Pagination"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             tags$ul(
  #               class = "pagination",
  #               tags$li(
  #                 class = "page-item disabled",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "«"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item active",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "1"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "2"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "3"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "4"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "5"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "»"
  #                 )
  #               )
  #             )
  #           ),
  #           tags$div(
  #             tags$ul(
  #               class = "pagination pagination-lg",
  #               tags$li(
  #                 class = "page-item disabled",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "«"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item active",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "1"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "2"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "3"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "4"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "5"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "»"
  #                 )
  #               )
  #             )
  #           ),
  #           tags$div(
  #             tags$ul(
  #               class = "pagination pagination-sm",
  #               tags$li(
  #                 class = "page-item disabled",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "«"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item active",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "1"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "2"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "3"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "4"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "5"
  #                 )
  #               ),
  #               tags$li(
  #                 class = "page-item",
  #                 tags$a(
  #                   class = "page-link",
  #                   href = "#",
  #                   "»"
  #                 )
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     )
  #   ),
  #   tags$div(
  #     class = "bs-docs-section",
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$div(
  #           class = "page-header",
  #           tags$h1(
  #             id = "indicators",
  #             "Indicators"
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$h2(
  #           "Alerts"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "alert alert-dismissible alert-warning",
  #             tags$button(
  #               type = "button",
  #               class = "btn-close",
  #               `data-bs-dismiss` = "alert"
  #             ),
  #             tags$h4(
  #               class = "alert-heading",
  #               "Warning!"
  #             ),
  #             tags$p(
  #               class = "mb-0",
  #               "Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna,",
  #               tags$a(
  #                 href = "#",
  #                 class = "alert-link",
  #                 "vel scelerisque nisl consectetur et"
  #               ),
  #               "."
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "alert alert-dismissible alert-danger",
  #             tags$button(
  #               type = "button",
  #               class = "btn-close",
  #               `data-bs-dismiss` = "alert"
  #             ),
  #             tags$strong(
  #               "Oh snap!"
  #             ),
  #             tags$a(
  #               href = "#",
  #               class = "alert-link",
  #               "Change a few things up"
  #             ),
  #             "and try submitting again."
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "alert alert-dismissible alert-success",
  #             tags$button(
  #               type = "button",
  #               class = "btn-close",
  #               `data-bs-dismiss` = "alert"
  #             ),
  #             tags$strong(
  #               "Well done!"
  #             ),
  #             "You successfully read",
  #             tags$a(
  #               href = "#",
  #               class = "alert-link",
  #               "this important alert message"
  #             ),
  #             "."
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "alert alert-dismissible alert-info",
  #             tags$button(
  #               type = "button",
  #               class = "btn-close",
  #               `data-bs-dismiss` = "alert"
  #             ),
  #             tags$strong(
  #               "Heads up!"
  #             ),
  #             "This",
  #             tags$a(
  #               href = "#",
  #               class = "alert-link",
  #               "alert needs your attention"
  #             ),
  #             ", but it's not super important."
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "alert alert-dismissible alert-primary",
  #             tags$button(
  #               type = "button",
  #               class = "btn-close",
  #               `data-bs-dismiss` = "alert"
  #             ),
  #             tags$strong(
  #               "Oh snap!"
  #             ),
  #             tags$a(
  #               href = "#",
  #               class = "alert-link",
  #               "Change a few things up"
  #             ),
  #             "and try submitting again."
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "alert alert-dismissible alert-secondary",
  #             tags$button(
  #               type = "button",
  #               class = "btn-close",
  #               `data-bs-dismiss` = "alert"
  #             ),
  #             tags$strong(
  #               "Well done!"
  #             ),
  #             "You successfully read",
  #             tags$a(
  #               href = "#",
  #               class = "alert-link",
  #               "this important alert message"
  #             ),
  #             "."
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "alert alert-dismissible alert-light",
  #             tags$button(
  #               type = "button",
  #               class = "btn-close",
  #               `data-bs-dismiss` = "alert"
  #             ),
  #             tags$strong(
  #               "Heads up!"
  #             ),
  #             "This",
  #             tags$a(
  #               href = "#",
  #               class = "alert-link",
  #               "alert needs your attention"
  #             ),
  #             ", but it's not super important."
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       tags$h2(
  #         "Badges"
  #       ),
  #       tags$div(
  #         class = "bs-component mb-4",
  #         tags$span(
  #           class = "badge bg-primary",
  #           "Primary"
  #         ),
  #         tags$span(
  #           class = "badge bg-secondary",
  #           "Secondary"
  #         ),
  #         tags$span(
  #           class = "badge bg-success",
  #           "Success"
  #         ),
  #         tags$span(
  #           class = "badge bg-danger",
  #           "Danger"
  #         ),
  #         tags$span(
  #           class = "badge bg-warning",
  #           "Warning"
  #         ),
  #         tags$span(
  #           class = "badge bg-info",
  #           "Info"
  #         ),
  #         tags$span(
  #           class = "badge bg-light",
  #           "Light"
  #         ),
  #         tags$span(
  #           class = "badge bg-dark",
  #           "Dark"
  #         ),
  #         tags$button(
  #           class = "source-button btn btn-primary btn-xs",
  #           type = "button",
  #           tabindex = "0",
  #           tags$i(
  #             class = "bi bi-code"
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "bs-component",
  #         tags$span(
  #           class = "badge rounded-pill bg-primary",
  #           "Primary"
  #         ),
  #         tags$span(
  #           class = "badge rounded-pill bg-secondary",
  #           "Secondary"
  #         ),
  #         tags$span(
  #           class = "badge rounded-pill bg-success",
  #           "Success"
  #         ),
  #         tags$span(
  #           class = "badge rounded-pill bg-danger",
  #           "Danger"
  #         ),
  #         tags$span(
  #           class = "badge rounded-pill bg-warning",
  #           "Warning"
  #         ),
  #         tags$span(
  #           class = "badge rounded-pill bg-info",
  #           "Info"
  #         ),
  #         tags$span(
  #           class = "badge rounded-pill bg-light",
  #           "Light"
  #         ),
  #         tags$span(
  #           class = "badge rounded-pill bg-dark",
  #           "Dark"
  #         ),
  #         tags$button(
  #           class = "source-button btn btn-primary btn-xs",
  #           type = "button",
  #           tabindex = "0",
  #           tags$i(
  #             class = "bi bi-code"
  #           )
  #         )
  #       )
  #     )
  #   ),
  #   tags$div(
  #     class = "bs-docs-section",
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$div(
  #           class = "page-header",
  #           tags$h1(
  #             id = "progress",
  #             "Progress"
  #           )
  #         ),
  #         tags$h3(
  #           id = "progress-basic",
  #           "Basic"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar",
  #               role = "progressbar",
  #               style = "width: 25%;",
  #               `aria-valuenow` = "25",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$h3(
  #           id = "progress-alternatives",
  #           "Contextual alternatives"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar bg-success",
  #               role = "progressbar",
  #               style = "width: 25%;",
  #               `aria-valuenow` = "25",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar bg-info",
  #               role = "progressbar",
  #               style = "width: 50%;",
  #               `aria-valuenow` = "50",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar bg-warning",
  #               role = "progressbar",
  #               style = "width: 75%;",
  #               `aria-valuenow` = "75",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar bg-danger",
  #               role = "progressbar",
  #               style = "width: 100%;",
  #               `aria-valuenow` = "100",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$h3(
  #           id = "progress-multiple",
  #           "Multiple bars"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar",
  #               role = "progressbar",
  #               style = "width: 15%;",
  #               `aria-valuenow` = "15",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             ),
  #             tags$div(
  #               class = "progress-bar bg-success",
  #               role = "progressbar",
  #               style = "width: 30%;",
  #               `aria-valuenow` = "30",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             ),
  #             tags$div(
  #               class = "progress-bar bg-info",
  #               role = "progressbar",
  #               style = "width: 20%;",
  #               `aria-valuenow` = "20",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$h3(
  #           id = "progress-striped",
  #           "Striped"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar progress-bar-striped",
  #               role = "progressbar",
  #               style = "width: 10%;",
  #               `aria-valuenow` = "10",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar progress-bar-striped bg-success",
  #               role = "progressbar",
  #               style = "width: 25%;",
  #               `aria-valuenow` = "25",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar progress-bar-striped bg-info",
  #               role = "progressbar",
  #               style = "width: 50%;",
  #               `aria-valuenow` = "50",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar progress-bar-striped bg-warning",
  #               role = "progressbar",
  #               style = "width: 75%;",
  #               `aria-valuenow` = "75",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar progress-bar-striped bg-danger",
  #               role = "progressbar",
  #               style = "width: 100%;",
  #               `aria-valuenow` = "100",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$h3(
  #           id = "progress-animated",
  #           "Animated"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "progress",
  #             tags$div(
  #               class = "progress-bar progress-bar-striped progress-bar-animated",
  #               role = "progressbar",
  #               `aria-valuenow` = "75",
  #               `aria-valuemin` = "0",
  #               `aria-valuemax` = "100",
  #               style = "width: 75%;"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     )
  #   ),
  #   tags$div(
  #     class = "bs-docs-section",
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$div(
  #           class = "page-header",
  #           tags$h1(
  #             id = "containers",
  #             "Containers"
  #           )
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "jumbotron",
  #             tags$h1(
  #               class = "display-3",
  #               "Hello, world!"
  #             ),
  #             tags$p(
  #               class = "lead",
  #               "This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information."
  #             ),
  #             tags$hr(
  #               class = "my-4"
  #             ),
  #             tags$p(
  #               "It uses utility classes for typography and spacing to space content out within the larger container."
  #             ),
  #             tags$p(
  #               class = "lead",
  #               tags$a(
  #                 class = "btn btn-primary btn-lg",
  #                 href = "#",
  #                 role = "button",
  #                 "Learn more"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$h2(
  #           "List groups"
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$ul(
  #             class = "list-group",
  #             tags$li(
  #               class = "list-group-item d-flex justify-content-between align-items-center",
  #               "Cras justo odio",
  #               tags$span(
  #                 class = "badge bg-primary badge-pill",
  #                 "14"
  #               )
  #             ),
  #             tags$li(
  #               class = "list-group-item d-flex justify-content-between align-items-center",
  #               "Dapibus ac facilisis in",
  #               tags$span(
  #                 class = "badge bg-primary badge-pill",
  #                 "2"
  #               )
  #             ),
  #             tags$li(
  #               class = "list-group-item d-flex justify-content-between align-items-center",
  #               "Morbi leo risus",
  #               tags$span(
  #                 class = "badge bg-primary badge-pill",
  #                 "1"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "list-group",
  #             tags$a(
  #               href = "#",
  #               class = "list-group-item list-group-item-action active",
  #               "Cras justo odio"
  #             ),
  #             tags$a(
  #               href = "#",
  #               class = "list-group-item list-group-item-action",
  #               "Dapibus ac facilisis in"
  #             ),
  #             tags$a(
  #               href = "#",
  #               class = "list-group-item list-group-item-action disabled",
  #               "Morbi leo risus"
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "list-group",
  #             tags$a(
  #               href = "#",
  #               class = "list-group-item list-group-item-action flex-column align-items-start active",
  #               tags$div(
  #                 class = "d-flex w-100 justify-content-between",
  #                 tags$h5(
  #                   class = "mb-1",
  #                   "List group item heading"
  #                 ),
  #                 tags$small(
  #                   "3 days ago"
  #                 )
  #               ),
  #               tags$p(
  #                 class = "mb-1",
  #                 "Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit."
  #               ),
  #               tags$small(
  #                 "Donec id elit non mi porta."
  #               )
  #             ),
  #             tags$a(
  #               href = "#",
  #               class = "list-group-item list-group-item-action flex-column align-items-start",
  #               tags$div(
  #                 class = "d-flex w-100 justify-content-between",
  #                 tags$h5(
  #                   class = "mb-1",
  #                   "List group item heading"
  #                 ),
  #                 tags$small(
  #                   class = "text-muted",
  #                   "3 days ago"
  #                 )
  #               ),
  #               tags$p(
  #                 class = "mb-1",
  #                 "Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit."
  #               ),
  #               tags$small(
  #                 class = "text-muted",
  #                 "Donec id elit non mi porta."
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$h2(
  #           "Cards"
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "card text-white bg-primary mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Primary card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card bg-secondary mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Secondary card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card text-white bg-success mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Success card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card text-white bg-danger mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Danger card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card text-white bg-warning mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Warning card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card text-white bg-info mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Info card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card bg-light mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Light card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card text-white bg-dark mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Dark card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "card border-primary mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Primary card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card border-secondary mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Secondary card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card border-success mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Success card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card border-danger mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Danger card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card border-warning mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Warning card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card border-info mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Info card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card border-light mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Light card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$div(
  #             class = "card border-dark mb-3",
  #             style = "max-width: 20rem;",
  #             tags$div(
  #               class = "card-header",
  #               "Header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Dark card title"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "card mb-3",
  #             tags$h3(
  #               class = "card-header",
  #               "Card header"
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$h5(
  #                 class = "card-title",
  #                 "Special title treatment"
  #               ),
  #               tags$h6(
  #                 class = "card-subtitle text-muted",
  #                 "Support card subtitle"
  #               )
  #             ),
  #             tags$svg(
  #               xmlns = "http://www.w3.org/2000/svg",
  #               class = "d-block user-select-none",
  #               width = "100%",
  #               height = "200",
  #               `aria-label` = "Placeholder: Image cap",
  #               focusable = "false",
  #               role = "img",
  #               preserveAspectRatio = "xMidYMid slice",
  #               viewBox = "0 0 318 180",
  #               style = "font-size:1.125rem;text-anchor:middle",
  #               tags$rect(
  #                 width = "100%",
  #                 height = "100%",
  #                 fill = "#868e96"
  #               ),
  #               tags$text(
  #                 x = "50%",
  #                 y = "50%",
  #                 fill = "#dee2e6",
  #                 dy = ".3em",
  #                 "Image cap"
  #               )
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               )
  #             ),
  #             tags$ul(
  #               class = "list-group list-group-flush",
  #               tags$li(
  #                 class = "list-group-item",
  #                 "Cras justo odio"
  #               ),
  #               tags$li(
  #                 class = "list-group-item",
  #                 "Dapibus ac facilisis in"
  #               ),
  #               tags$li(
  #                 class = "list-group-item",
  #                 "Vestibulum at eros"
  #               )
  #             ),
  #             tags$div(
  #               class = "card-body",
  #               tags$a(
  #                 href = "#",
  #                 class = "card-link",
  #                 "Card link"
  #               ),
  #               tags$a(
  #                 href = "#",
  #                 class = "card-link",
  #                 "Another link"
  #               )
  #             ),
  #             tags$div(
  #               class = "card-footer text-muted",
  #               "2 days ago"
  #             )
  #           ),
  #           tags$div(
  #             class = "card",
  #             tags$div(
  #               class = "card-body",
  #               tags$h4(
  #                 class = "card-title",
  #                 "Card title"
  #               ),
  #               tags$h6(
  #                 class = "card-subtitle mb-2 text-muted",
  #                 "Card subtitle"
  #               ),
  #               tags$p(
  #                 class = "card-text",
  #                 "Some quick example text to build on the card title and make up the bulk of the card's content."
  #               ),
  #               tags$a(
  #                 href = "#",
  #                 class = "card-link",
  #                 "Card link"
  #               ),
  #               tags$a(
  #                 href = "#",
  #                 class = "card-link",
  #                 "Another link"
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row mt-5",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$h2(
  #           "Accordions"
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-4",
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "accordion",
  #             id = "accordionExample",
  #             tags$div(
  #               class = "accordion-item",
  #               tags$h2(
  #                 class = "accordion-header",
  #                 id = "headingOne",
  #                 tags$button(
  #                   class = "accordion-button",
  #                   type = "button",
  #                   `data-bs-toggle` = "collapse",
  #                   `data-bs-target` = "#collapseOne",
  #                   `aria-expanded` = "true",
  #                   `aria-controls` = "collapseOne",
  #                   "Accordion Item #1"
  #                 )
  #               ),
  #               tags$div(
  #                 id = "collapseOne",
  #                 class = "accordion-collapse collapse show",
  #                 `aria-labelledby` = "headingOne",
  #                 `data-bs-parent` = "#accordionExample",
  #                 tags$div(
  #                   class = "accordion-body",
  #                   tags$strong(
  #                     "This is the first item's accordion body."
  #                   ),
  #                   "It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the",
  #                   tags$code(
  #                     ".accordion-body"
  #                   ),
  #                   ", though the transition does limit overflow."
  #                 )
  #               )
  #             ),
  #             tags$div(
  #               class = "accordion-item",
  #               tags$h2(
  #                 class = "accordion-header",
  #                 id = "headingTwo",
  #                 tags$button(
  #                   class = "accordion-button collapsed",
  #                   type = "button",
  #                   `data-bs-toggle` = "collapse",
  #                   `data-bs-target` = "#collapseTwo",
  #                   `aria-expanded` = "false",
  #                   `aria-controls` = "collapseTwo",
  #                   "Accordion Item #2"
  #                 )
  #               ),
  #               tags$div(
  #                 id = "collapseTwo",
  #                 class = "accordion-collapse collapse",
  #                 `aria-labelledby` = "headingTwo",
  #                 `data-bs-parent` = "#accordionExample",
  #                 tags$div(
  #                   class = "accordion-body",
  #                   tags$strong(
  #                     "This is the second item's accordion body."
  #                   ),
  #                   "It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the",
  #                   tags$code(
  #                     ".accordion-body"
  #                   ),
  #                   ", though the transition does limit overflow."
  #                 )
  #               )
  #             ),
  #             tags$div(
  #               class = "accordion-item",
  #               tags$h2(
  #                 class = "accordion-header",
  #                 id = "headingThree",
  #                 tags$button(
  #                   class = "accordion-button collapsed",
  #                   type = "button",
  #                   `data-bs-toggle` = "collapse",
  #                   `data-bs-target` = "#collapseThree",
  #                   `aria-expanded` = "false",
  #                   `aria-controls` = "collapseThree",
  #                   "Accordion Item #3"
  #                 )
  #               ),
  #               tags$div(
  #                 id = "collapseThree",
  #                 class = "accordion-collapse collapse",
  #                 `aria-labelledby` = "headingThree",
  #                 `data-bs-parent` = "#accordionExample",
  #                 tags$div(
  #                   class = "accordion-body",
  #                   tags$strong(
  #                     "This is the third item's accordion body."
  #                   ),
  #                   "It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the",
  #                   tags$code(
  #                     ".accordion-body"
  #                   ),
  #                   ", though the transition does limit overflow."
  #                 )
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     )
  #   ),
  #   tags$div(
  #     class = "bs-docs-section",
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-12",
  #         tags$div(
  #           class = "page-header",
  #           tags$h1(
  #             id = "dialogs",
  #             "Dialogs"
  #           )
  #         )
  #       )
  #     ),
  #     tags$div(
  #       class = "row",
  #       tags$div(
  #         class = "col-lg-6",
  #         tags$h2(
  #           "Modals"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "modal",
  #             tags$div(
  #               class = "modal-dialog",
  #               role = "document",
  #               tags$div(
  #                 class = "modal-content",
  #                 tags$div(
  #                   class = "modal-header",
  #                   tags$h5(
  #                     class = "modal-title",
  #                     "Modal title"
  #                   ),
  #                   tags$button(
  #                     type = "button",
  #                     class = "btn-close",
  #                     `data-bs-dismiss` = "modal",
  #                     `aria-label` = "Close",
  #                     tags$span(
  #                       `aria-hidden` = "true"
  #                     )
  #                   )
  #                 ),
  #                 tags$div(
  #                   class = "modal-body",
  #                   tags$p(
  #                     "Modal body text goes here."
  #                   )
  #                 ),
  #                 tags$div(
  #                   class = "modal-footer",
  #                   tags$button(
  #                     type = "button",
  #                     class = "btn btn-primary",
  #                     "Save changes"
  #                   ),
  #                   tags$button(
  #                     type = "button",
  #                     class = "btn btn-secondary",
  #                     `data-bs-dismiss` = "modal",
  #                     "Close"
  #                   )
  #                 )
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$h2(
  #           "Offcanvas"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$a(
  #             class = "btn btn-primary",
  #             `data-bs-toggle` = "offcanvas",
  #             href = "#offcanvasExample",
  #             role = "button",
  #             `aria-controls` = "offcanvasExample",
  #             "Link with href"
  #           ),
  #           tags$button(
  #             class = "btn btn-primary",
  #             type = "button",
  #             `data-bs-toggle` = "offcanvas",
  #             `data-bs-target` = "#offcanvasExample",
  #             `aria-controls` = "offcanvasExample",
  #             "Button with data-bs-target"
  #           ),
  #           tags$div(
  #             class = "offcanvas offcanvas-start",
  #             tabindex = "-1",
  #             id = "offcanvasExample",
  #             `aria-labelledby` = "offcanvasExampleLabel",
  #             tags$div(
  #               class = "offcanvas-header",
  #               tags$h5(
  #                 class = "offcanvas-title",
  #                 id = "offcanvasExampleLabel",
  #                 "Offcanvas"
  #               ),
  #               tags$button(
  #                 type = "button",
  #                 class = "btn-close text-reset",
  #                 `data-bs-dismiss` = "offcanvas",
  #                 `aria-label` = "Close"
  #               )
  #             ),
  #             tags$div(
  #               class = "offcanvas-body",
  #               tags$div(
  #                 "Some text as placeholder. In real life you can have the elements you have chosen. Like, text, images, lists, etc."
  #               ),
  #               tags$div(
  #                 class = "dropdown mt-3",
  #                 tags$button(
  #                   class = "btn btn-secondary dropdown-toggle",
  #                   type = "button",
  #                   id = "dropdownMenuButton",
  #                   `data-bs-toggle` = "dropdown",
  #                   "Dropdown button"
  #                 ),
  #                 tags$ul(
  #                   class = "dropdown-menu",
  #                   `aria-labelledby` = "dropdownMenuButton",
  #                   tags$li(
  #                     tags$a(
  #                       class = "dropdown-item",
  #                       href = "#",
  #                       "Action"
  #                     )
  #                   ),
  #                   tags$li(
  #                     tags$a(
  #                       class = "dropdown-item",
  #                       href = "#",
  #                       "Another action"
  #                     )
  #                   ),
  #                   tags$li(
  #                     tags$a(
  #                       class = "dropdown-item",
  #                       href = "#",
  #                       "Something else here"
  #                     )
  #                   )
  #                 )
  #               )
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       ),
  #       tags$div(
  #         class = "col-lg-6",
  #         tags$h2(
  #           "Popovers"
  #         ),
  #         tags$div(
  #           class = "bs-component mb-5",
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-secondary",
  #             `data-bs-container` = "body",
  #             `data-bs-toggle` = "popover",
  #             `data-bs-placement` = "left",
  #             `data-bs-content` = "Vivamus sagittis lacus vel augue laoreet rutrum faucibus.",
  #             `data-bs-original-title` = "Popover Title",
  #             "Left"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-secondary",
  #             `data-bs-container` = "body",
  #             `data-bs-toggle` = "popover",
  #             `data-bs-placement` = "top",
  #             `data-bs-content` = "Vivamus sagittis lacus vel augue laoreet rutrum faucibus.",
  #             `data-bs-original-title` = "Popover Title",
  #             "Top"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-secondary",
  #             `data-bs-container` = "body",
  #             `data-bs-toggle` = "popover",
  #             `data-bs-placement` = "bottom",
  #             `data-bs-content` = "Vivamus sagittis lacus vel augue laoreet rutrum faucibus.",
  #             `data-bs-original-title` = "Popover Title",
  #             "Bottom"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-secondary",
  #             `data-bs-container` = "body",
  #             `data-bs-toggle` = "popover",
  #             `data-bs-placement` = "right",
  #             `data-bs-content` = "Vivamus sagittis lacus vel augue laoreet rutrum faucibus.",
  #             `data-bs-original-title` = "Popover Title",
  #             "Right"
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$h2(
  #           "Tooltips"
  #         ),
  #         tags$div(
  #           class = "bs-component mb-5",
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-secondary",
  #             `data-bs-toggle` = "tooltip",
  #             `data-bs-placement` = "left",
  #             `data-bs-original-title` = "Tooltip on left",
  #             "Left"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-secondary",
  #             `data-bs-toggle` = "tooltip",
  #             `data-bs-placement` = "top",
  #             `data-bs-original-title` = "Tooltip on top",
  #             "Top"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-secondary",
  #             `data-bs-toggle` = "tooltip",
  #             `data-bs-placement` = "bottom",
  #             `data-bs-original-title` = "Tooltip on bottom",
  #             "Bottom"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-secondary",
  #             `data-bs-toggle` = "tooltip",
  #             `data-bs-placement` = "right",
  #             `data-bs-original-title` = "Tooltip on right",
  #             "Right"
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         ),
  #         tags$h2(
  #           "Toasts"
  #         ),
  #         tags$div(
  #           class = "bs-component",
  #           tags$div(
  #             class = "toast show",
  #             role = "alert",
  #             `aria-live` = "assertive",
  #             `aria-atomic` = "true",
  #             tags$div(
  #               class = "toast-header",
  #               tags$strong(
  #                 class = "me-auto",
  #                 "Bootstrap"
  #               ),
  #               tags$small(
  #                 "11 mins ago"
  #               ),
  #               tags$button(
  #                 type = "button",
  #                 class = "btn-close ms-2 mb-1",
  #                 `data-bs-dismiss` = "toast",
  #                 `aria-label` = "Close",
  #                 tags$span(
  #                   `aria-hidden` = "true"
  #                 )
  #               )
  #             ),
  #             tags$div(
  #               class = "toast-body",
  #               "Hello, world! This is a toast message."
  #             )
  #           ),
  #           tags$button(
  #             class = "source-button btn btn-primary btn-xs",
  #             type = "button",
  #             tabindex = "0",
  #             tags$i(
  #               class = "bi bi-code"
  #             )
  #           )
  #         )
  #       )
  #     )
  #   ),
  #   tags$div(
  #     id = "source-modal",
  #     class = "modal fade",
  #     tabindex = "-1",
  #     tags$div(
  #       class = "modal-dialog modal-lg",
  #       tags$div(
  #         class = "modal-content",
  #         tags$div(
  #           class = "modal-header",
  #           tags$h4(
  #             class = "modal-title",
  #             "Source Code"
  #           ),
  #           tags$button(
  #             type = "button",
  #             class = "btn btn-primary btn-copy",
  #             tags$i(
  #               class = "bi bi-clipboard"
  #             ),
  #             "Copy Code"
  #           )
  #         ),
  #         tags$div(
  #           class = "modal-body",
  #           tags$pre(
  #             class = "language-html",
  #             tags$code()
  #           )
  #         )
  #       )
  #     )
  #   )
  # )


  tags$div(
    tags$h2(
      "Badges"
    ),
    tags$div(
      class = "bs-component mb-4",
      tags$span(
        class = "badge bg-primary",
        "Primary"
      ),
      tags$span(
        class = "badge bg-secondary",
        "Secondary"
      ),
      tags$span(
        class = "badge bg-success",
        "Success"
      ),
      tags$span(
        class = "badge bg-danger",
        "Danger"
      ),
      tags$span(
        class = "badge bg-warning",
        "Warning"
      ),
      tags$span(
        class = "badge bg-info",
        "Info"
      ),
      tags$span(
        class = "badge bg-light",
        "Light"
      ),
      tags$span(
        class = "badge bg-dark",
        "Dark"
      )
    ),
    tags$div(
      class = "bs-component",
      tags$span(
        class = "badge rounded-pill bg-primary",
        "Primary"
      ),
      tags$span(
        class = "badge rounded-pill bg-secondary",
        "Secondary"
      ),
      tags$span(
        class = "badge rounded-pill bg-success",
        "Success"
      ),
      tags$span(
        class = "badge rounded-pill bg-danger",
        "Danger"
      ),
      tags$span(
        class = "badge rounded-pill bg-warning",
        "Warning"
      ),
      tags$span(
        class = "badge rounded-pill bg-info",
        "Info"
      ),
      tags$span(
        class = "badge rounded-pill bg-light",
        "Light"
      ),
      tags$span(
        class = "badge rounded-pill bg-dark",
        "Dark"
      )
    )
  )
}
