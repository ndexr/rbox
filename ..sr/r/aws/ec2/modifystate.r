#' @export
ui_modifystate <- function(id = "modifystate", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(shinyWidgets)
    box::use(.. / ec2 / instance_types_get)
    box::use(.. / .. / inputs / inputs)
  }
  ns <- shiny$NS(id)
  InstanceTypes <- instance_types_get$instance_types_get()
  tags$div(
    class = "row",
    tags$div(
      class = "col-lg-8 my-2",
      inputs$actionButton(ns("start"), "Start", class = "btn-info mb-1"),
      inputs$actionButton(ns("stop"), "Stop", class = "btn-warning mb-1"),
      inputs$actionButton(ns("reboot"), "Reboot", class = "btn-warning mb-1"),
      inputs$actionButton(ns("terminate"), "Terminate", class = "btn-danger mb-1")
    ),
    tags$div(
      class = "col-lg-4 my-2",
      inputs$selectizeInput(ns("InstanceType"), NULL,
        InstanceTypes,
        ec2data$instance$InstanceType,
        multiple = FALSE
      ),
      tags$div(
        class = "text-end",
        inputs$actionButton(ns("modify"), "Modify", class = "btn-primary")
      )
    )
  )
}

#' @export
server_modifystate <- function(id = "modifystate", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / client)
    box::use(.. / .. / connections / postgres)
    box::use(. / instance_manage)
    box::use(glue)
  }


  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      shiny$observeEvent(input$terminate, {
        tryCatch(
          {
            ns_common_store_user <- ec2data$ns_common_store_user
            ec2con <- client$client("ec2", ns_common_store_user = ns_common_store_user)

            instance_state <- postgres$instance_state(
              InstanceType = ec2data$instance$InstanceType,
              InstanceId = ec2data$instance$InstanceId,
              status = ns_common_store_user("terminated")
            )

            shiny$showNotification("Terminating instance")
            ec2con$terminate_instances(InstanceIds = list(ec2data$instance$InstanceId))
            shiny$showNotification(shiny$p(shiny$icon("fire"), " wrekd"))
          },
          error = function(err) {
            shiny$showNotification(tags$pre(as.character(err)))
          }
        )
      })

      shiny$observeEvent(input$reboot, {
        tryCatch(
          {
            ns_common_store_user <- ec2data$ns_common_store_user
            ec2con <- client$client("ec2", ns_common_store_user = ns_common_store_user)


            instance_state <- postgres$instance_state(
              InstanceType = ec2data$instance$InstanceType,
              InstanceId = ec2data$instance$InstanceId,
              status = "rebooting"
            )
            ec2con$reboot_instances(InstanceIds = list(ec2data$instance$InstanceId))
            shiny$showNotification(shiny$p(shiny$icon("fire"), "momentarily wrekd"))
          },
          error = function(err) {
            shiny$showNotification(tags$pre(as.character(err)))
          }
        )
      })


      shiny$observeEvent(input$modify, {
        tryCatch(
          {
            ns_common_store_user <- ec2data$ns_common_store_user
            ec2con <- client$client("ec2", ns_common_store_user = ns_common_store_user)

            if ({
              inst <- instance_manage$describe_instances(ns_common_store_user)
              inst[inst$InstanceId == ec2data$instance$InstanceId, ]$state != "stopped"
            }) {
              ec2con$stop_instances(InstanceIds = list(ec2data$instance$InstanceId))
            }

            while ({
              inst <- instance_manage$describe_instances(ns_common_store_user)
              inst[inst$InstanceId == ec2data$instance$InstanceId, ]$state != "stopped"
            }) {
              shiny$showNotification("Waiting for instance to stop...")
              Sys.sleep(3)
            }

            resp <- ec2con$modify_instance_attribute(InstanceId = ec2data$instance$InstanceId, Attribute = "instanceType", Value = input$InstanceType)

            instance_state <- postgres$instance_state(InstanceType = input$InstanceType, InstanceId = ec2data$instance$InstanceId, status = "modified")

            ec2con$start_instances(InstanceIds = list(ec2data$instance$InstanceId))

            shiny$showNotification(glue$glue("Starting up with {input$InstanceType}"))
          },
          error = function(err) {
            shiny$showNotification(tags$p(tags$code(as.character(err))))
          }
        )
      })

      shiny$observeEvent(input$stop, {
        shiny$showNotification("Stopping instance")
        tryCatch(
          {
            ns_common_store_user <- ec2data$ns_common_store_user
            ec2con <- client$client("ec2", ns_common_store_user = ns_common_store_user)

            ec2con$stop_instances(InstanceIds = list(ec2data$instance$InstanceId))
            shiny$showNotification(shiny$p(
              shiny::icon("hand-peace"), "booped harder"
            ))

            instance_state <- postgres$instance_state(
              InstanceType = ec2data$instance$InstanceType,
              InstanceId = ec2data$instance$InstanceId,
              status = "stopping"
            )
          },
          error = function(err) {
            shiny$showNotification(tags$p(tags$code(as.character(err))))
          }
        )
      })

      shiny$observeEvent(input$start, {
        tryCatch(
          {
            shiny$showNotification("Starting instance")
            ns_common_store_user <- ec2data$ns_common_store_user
            ec2con <- client$client("ec2", ns_common_store_user = ns_common_store_user)


            ec2con$start_instances(InstanceIds = list(ec2data$instance$InstanceId))
            shiny$showNotification(shiny$p(
              shiny::icon("hand-peace"), "booped"
            ))
            instance_state <- postgres$instance_state(
              InstanceType = ec2data$instance$InstanceType,
              InstanceId = ec2data$instance$InstanceId,
              status = "starting"
            )
          },
          error = function(err) {
            shiny$showNotification(shiny$p(
              shiny::icon("face-grin-beam-sweat"), " - probably just need to wait unless you're trying to start a terminated instance"
            ))
          }
        )
      })
    }
  )
}
