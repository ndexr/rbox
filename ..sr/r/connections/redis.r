#' @export
connection_redis <- function() {
  box::use(redux)
  print("connecting to redis")
  con <- redux$hiredis(
    host = Sys.getenv("REDIS_HOST"),
    port = Sys.getenv("REDIS_PORT")
  )
  con
}

#' @export
setDefault <- function(value, default) {
  if (length(value) > 1) {
    return(value)
  }
  ifelse(is.null(value) | !length(nchar(value)), default, value)
}


#' @export
store_state <- function(id, data) {
  box::use(. / redis)
  box::use(redux)

  if (is.null(data)) {

  }
  con <- redis$connection_redis()
  data <- redux$object_to_bin(data)
  con$SET(key = id, value = data)
}

#' @export
get_state <- function(id) {
  box::use(. / redis)
  box::use(glue[glue])
  box::use(redux)
  box::use(shiny)
  box::use(storr)
  box::use(. / postgres)
  con <- redis$connection_redis()
  data <- con$GET(key = id)
  if (!is.null(data)) data <- redux$bin_to_object(data)
  if (shiny$is.reactivevalues(data)) {
    return(list())
  }
  data
}
