#' @export
ui_navbar <- function(nav_brand, nav_links = c("Services", "Portfolio", "About", "Team", "Contact")) {
  box::use(shiny[withTags, tags])
  # box::use(purrr)
  withTags(
    nav(
      class = "navbar navbar-expand-lg navbar-dark fixed-top bg-dark", id = "mainNav",
      div(
        class = "",
        a(class = "navbar-brand", href = "#page-top", nav_brand),
        button(
          class = "navbar-toggler", type = "button",
          `data-bs-toggle` = "collapse",
          `data-bs-target` = "#navbarResponsive",
          `aria-controls` = "navbarResponsive",
          `aria-expanded` = "false",
          `aria-label` = "Toggle navigation",
          "Menu",
          i(class = "fas fa-bars ms-1")
        ),
        div(
          class = "collapse navbar-collapse", id = "navbarResponsive",
          ul(
            class = "navbar-nav text-uppercase ms-auto py-4 py-lg-0",
            lapply(
              nav_links,
              function(nm) {
                tags$li(class = "nav-item", tags$a(class = "nav-link", href = paste0("#", tolower(gsub(" ", "", nm))), nm))
              }
            )
          )
        )
      )
    )
  )
}
