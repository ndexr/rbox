#' @export
stripe_connection <- function(live) {
  box::use(reticulate)
  if (live) env <- "STRIPE_LIVE_SK" else env <- "STRIPE_TEST_SK"
  stripe <- reticulate$import("stripe")
  stripe$api_key <- Sys.getenv(env)
  stripe
}

#' @export
stripe_customers <- function(live = FALSE) {
  {
    box::use(. / stripe)
    box::use(jsonlite)
    box::use(purrr)
    box::use(dplyr)
    box::use(.. / connections / postgres)
  }

  con <- stripe$stripe_connection(live)

  customers <- con$Customer$list(limit = 500L)
  customers <- purrr$map_dfr(
    customers$data,
    function(x) {
      data.frame(
        email = x$email,
        customer_id = x$id,
        live = live,
        date_created = Sys.Date(),
        datetime_created = Sys.time()
      )
    }
  )

  # postgres$table_drop('customers', dbname = 'stripe')
  postgres$table_create_or_upsert(customers, where_cols = "customer_id", dbname = "stripe")

  customers
}

#' @export
stripe_charges <- function(live = FALSE, customer = NULL) {
  {
    box::use(. / stripe)
    box::use(jsonlite)
    box::use(purrr)
    box::use(dplyr)
    box::use(.. / connections / postgres)
  }

  con <- stripe$stripe_connection(live)
  if (!is.null(customer)) {
    charges <- con$Charge$list(customer = customer, limit = 300L)
  } else {
    charges <- con$Charge$list(limit = 300L)
  }

  charges <- purrr$map_dfr(
    charges$data,
    function(x) {
      tryCatch(
        {
          out <- data.frame(
            amount = x$amount,
            created = x$created,
            customer = x$customer,
            description = x$description,
            # charge_id = x$id,
            live = live,
            payment_intent = ifelse(is.null(x$payment_intent), "", x$payment_intent)
          )

          out
        },
        error = function(err) data.frame()
      )
    }
  )

  # postgres$table_drop('subscriptions', dbname='stripe')
  postgres$table_create_or_upsert(charges, where_cols = "charge_id", dbname = "stripe")
  charges
}


#' @export
stripe_subscriptions <- function() {
  {
    box::use(. / stripe)
    box::use(jsonlite)
    box::use(purrr)
    box::use(dplyr)
    box::use(tibble)
  }
  live <- getOption("docker", FALSE)
  con <- stripe$stripe_connection(live)

  subscription <- con$Subscription$list(limit = 300L)

  # jsons <- as.character(reticulate::py_to_r(subscription))
  # subscriptions <- jsonlite$fromJSON(jsons, simplifyDataFrame = F)$data

  subscriptions <- purrr$map_dfr(
    subscription$data,
    function(x) {
      #
      data.frame(
        subscription_id = x$id,
        created = x$created,
        customer = x$customer,
        description = ifelse(is.null(x$description), "missing", x$description),
        live = live
      )
    }
  )
  # subscriptions
  postgres$table_create_or_upsert(subscriptions, where_cols = "subscription_id", dbname = "stripe")

  subscriptions
}


#' @export
stripe_plan_create <- function(amount = 5900, # The amount in cents
                               currency = "usd", # The currency
                               interval = "month", # The billing interval
                               interval_count = 1,
                               product = list(name = "basic"),
                               nickname = "ndexr console",
                               live = FALSE,
                               force = FALSE) {
  box::use(purrr)
  box::use(. / stripe)
  box::use(.. / connections / postgres)
  box::use(dplyr)
  con <- stripe$stripe_connection(live)

  plans <- con$Plan$create(
    amount = as.integer(amount), # The amount in cents
    currency = currency, # The currency
    interval = interval, # The billing interval
    interval_count = as.integer(interval_count),
    product = product,
    nickname = nickname # The name of the plan
  )

  plans <- data.frame(
    plan_id = plans$id,
    amount = plans$amount,
    nickname = plans$nickname,
    live = live,
    date_created = Sys.Date(),
    datetime_created = Sys.time()
  )
  # postgres$table_drop("plans", dbname = "stripe")
  postgres$table_create_or_upsert(plans, "plan_id", dbname = "stripe")
  plans
}

#' @export
stripe_customer_create <- function(email, live = FALSE) {
  box::use(. / stripe)
  box::use(.. / connections / postgres)
  box::use(purrr)
  box::use(dplyr)
  con <- stripe$stripe_connection(live)



  if (postgres$table_exists("customers", dbname = "stripe")) {
    customer <- postgres$table_get("customers", dbname = "stripe")
    customer <-
      customer |>
      dplyr$filter(email %in% !!email) |>
      dplyr$filter(live == !!live) |>
      dplyr$collect()

    if (nrow(customer)) {
      message("Returning existing customer")
      customer <- customer[customer$datetime_created == max(customer$datetime_created), ]
      return(customer)
    }
  }

  message("Creating a new customer")
  customer <- con$Customer$create(email = email)
  customers <- data.frame(
    email = email,
    customer_id = customer$id,
    live = live,
    date_created = Sys.Date(),
    datetime_created = Sys.time()
  )

  postgres$table_create_or_upsert(customers, "customer_id", dbname = "stripe")

  # if (postgres$table_exists("subscription", dbname = "stripe")) {
  #   subscriptions <- postgres$table_get("subscription", dbname = "stripe")
  #   customers <- dplyr$left_join(customers, subscriptions)
  # }
  dplyr$glimpse(customers)
  customers
}

#' @export
stripe_payment_method_attach <- function(
    customer_id,
    number = "4242424242424242",
    exp_month = 05L,
    exp_year = 2028L,
    cvc = "204",
    live = FALSE,
    force = FALSE) {
  box::use(. / stripe)
  box::use(dplyr)
  box::use(.. / connections / postgres)
  box::use(.. / inputs / inputs)
  box::use(purrr)
  box::use(shiny = shiny[tags])
  # shiny$showModal(
  #   inputs$modalDialog(
  #     tags$pre(jsonlite::toJSON(ls()))
  #   )
  # )
  #
  con <- stripe$stripe_connection(live)

  if (!force & postgres$table_exists("payment_methods", dbname = "stripe")) {
    payment_methods <- postgres$table_get("payment_methods", dbname = "stripe") |>
      dplyr$filter(
        customer_id %in% !!customer_id,
        datetime_created == max(datetime_created)
      ) |>
      dplyr$filter(live == !live)


    if (nrow(payment_methods)) {
      message("Returning existing payment method")
      payment_methods <- payment_methods[payment_methods$datetime_created == max(payment_methods$datetime_created), ]
      return(payment_methods)
    }
  }

  message("Creating new  payment method")
  #
  payment_methods <- con$PaymentMethod$create(
    type = "card",
    card = list(
      "number" = as.character(number),
      "exp_month" = as.integer(exp_month),
      "exp_year" = as.integer(exp_year),
      "cvc" = as.character(cvc)
    )
  )
  payment_methods <- con$PaymentMethod$attach(payment_methods$id, customer = customer_id)
  payment_methods <- data.frame(
    payment_method_id = payment_methods$id,
    customer_id = customer_id,
    live = live,
    date_created = Sys.Date(),
    datetime_created = Sys.time()
  )
  postgres$table_create_or_upsert(payment_methods, "payment_method_id", dbname = "stripe")
  payment_methods
}

#' @export
stripe_one_time_payment <- function(customer_id, payment_method_id, amount = 100, live = FALSE) {
  box::use(. / stripe)
  box::use(.. / connections / postgres)
  box::use(purrr)
  box::use(dplyr)
  con <- stripe$stripe_connection(live)


  payment_intent <- con$PaymentIntent$create(
    customer = customer_id,
    amount = as.integer(amount),
    currency = "usd",
    payment_method = payment_method_id
  )

  # Confirm the payment intent to process the payment
  payment_intent$confirm()
}

#' @export
stripe_subscription_start <- function(customer_id, plan_id, payment_method_id, live = FALSE, trial_period_days = 30) {
  box::use(. / stripe)
  box::use(.. / connections / postgres)
  box::use(purrr)
  box::use(dplyr)
  con <- stripe$stripe_connection(live)

  subscription <- con$Subscription$create(
    customer = customer_id,
    items = list(list("plan" = plan_id)),
    default_payment_method = payment_method_id,
    trial_period_days = as.integer(trial_period_days)
  )
  subscription <- data.frame(
    subscription_id = subscription$id,
    customer_id = customer_id,
    live = live,
    date_created = Sys.Date(),
    datetime_created = Sys.time()
  )
  postgres$table_create_or_upsert(subscription, "customer_id", dbname = "stripe")

  subscription
}


#' @export
stripe_subscription_stop <- function(subscription_id) {
  box::use(. / stripe)
  box::use(dplyr)
  box::use(.. / connections / postgres)
  box::use(purrr)
  con <- stripe$stripe_connection(live)

  con$Subscription$delete(subscription_id)
  con <- postgres$connection_postgres(dbname = "stripe")
  DBI::dbExecute(
    con, glue$glue("delete from subscription where id = '{subscription_id}'")
  )
}

#' @export
stripe_subscription_stop <- function(subscription_id, live = FALSE) {
  box::use(. / stripe)
  box::use(dplyr)
  box::use(.. / connections / postgres)
  box::use(purrr)
  con <- stripe$stripe_connection(live)

  con$Subscription$delete(subscription_id)
  con <- postgres$connection_postgres(dbname = "stripe")
  DBI::dbExecute(
    con, glue$glue("delete from subscription where id = '{subscription_id}'")
  )
}

#' @export
stripe_subscription_retrieve <- function(subscription_id, live = FALSE) {
  box::use(. / stripe)
  box::use(dplyr)
  box::use(.. / connections / postgres)
  box::use(purrr)
  con <- stripe$stripe_connection(live)
  con$Subscription$retrieve(subscription_id)
}

#' @export
stripe_subscription_stop <- function(subscription_id) {
  box::use(. / stripe)
  box::use(dplyr)
  box::use(.. / connections / postgres)
  box::use(purrr)
  con <- stripe$stripe_connection(live)

  con$Subscription$delete(subscription_id)
  con <- postgres$connection_postgres(dbname = "stripe")
  DBI::dbExecute(
    con, glue$glue("delete from subscription where id = '{subscription_id}'")
  )
}

#' @export
stripe_customer_ids <- function(emails, live = getOption("docker", FALSE)) {
  box::use(. / stripe)
  box::use(.. / aws / cognito / cognito)
  box::use(.. / users / user_data_aggregate)
  box::use(purrr)
  box::use(dplyr)

  cognito_users <- cognito$cognito_users()

  if (!is.null(emails)) {
    cognito_users <- dplyr$filter(cognito_users, email %in% emails)
  }

  customer_ids <- purrr$map_dfr(
    split(cognito_users, cognito_users$user),
    function(x) {
      box::use(dplyr)
      customer_id <- stripe$stripe_customer_create(
        email = x$email, live = live
      )
      dplyr$inner_join(x, customer_id)
    }
  )


  customer_ids
}



#' @export
stripe_lifecycle <- function() {
  box::use(.. / connections / postgres)
  box::use(. / stripe)
  box::reload(stripe)

  live <- getOption("docker", FALSE)
  customer_ids <- stripe$stripe_customer_ids(emails = "drennanfreddy@gmail.com")
  customer_subscriptions <- postgres$tables_row_retrieve("customer_id", customer_ids$customer_id, "subscription", showNotification = FALSE, dbname = "stripe")
  if (nrow(customer_subscriptions)) {
    con <- stripe$stripe_connection(live)
    con$Subscription$delete(customer_subscriptions$subscription_id)
    postgres$tables_row_remove("subscription_id", customer_subscriptions$subscription_id, "subscription", showNotification = FALSE, dbname = "stripe")
    print("Subscription Deleted")
  } else {
    # attach payment method given
    payment_method <- stripe$stripe_payment_method_attach(
      customer_id = customer_ids$customer_id,
      number = "4242424242424242",
      exp_month = 05L,
      exp_year = 2028L,
      cvc = "204",
      live = live
    )

    # create a subscription
    subscription <- stripe$stripe_subscription_start(
      customer_id = payment_method$customer_id,
      plan_id = "plan_NyWWRoiPBcwTEL", # 59.00 plan
      payment_method_id = payment_method$payment_method_id,
      live = live
    )

    print("Subscription Created")
  }

  intent <- con$PaymentIntent$create(
    customer = payment_method$customer_id,
    amount = 1000L,
    currency = "usd",
    payment_method = payment_method$payment_method_id,
    description = "Payment for serviced",
    receipt_email = "drennanfreddy@gmail.com",
    statement_descriptor = "ndexr"
    # price="price_1NE8qGIAcofnB5CRahRDtHsS"
  )

  intent$confirm()
  # subscriptions <- con$Subscription$list(customer=payment_method$customer_id)
  # currentSubscriptions <- as.character(subscriptions)
  # currentSubscriptions <- jsonlite$fromJSON(currentSubscriptions)





  # # make a one time payment
  # stripe$stripe_one_time_payment(
  #   customer_id = payment_method$customer_id,
  #   payment_method_id = payment_method$payment_method_id,
  #   amount = 100,
  #   live = live
  # )




  # con <- stripe$stripe_connection(live)

  # con$Sub

  # basicPlan
  debug(stripe$stripe_plan_create)
  basic_plan <- stripe$stripe_plan_create(
    amount = 2900,
    interval_count = 1,
    nickname = "ndexr basic access",
    product = list(name = "ndexr basic access"),
    live = TRUE,
    force = TRUE
  )

  basic_plan <- stripe$stripe_plan_create(
    amount = 2900,
    interval_count = 1,
    nickname = "ndexr basic access",
    product = list(name = "ndexr basic access"),
    live = TRUE,
    force = TRUE
  )

  #
  # basic_plan <- stripe$stripe_plan_create(
  #   amount = 75000,
  #   interval_count = 1,
  #   nickname = "ndexr team support",
  #   live = FALSE,
  #   product = list(name = "ndexr team support"),
  #   force = TRUE
  # )


  if (FALSE) {
    con <- stripe$stripe_connection(live)
    allCustomers <- con$Customer$list(limit = 100L)
    purrr::walk(
      allCustomers$data, function(x) {
        con$Customer$delete(x$id)
      }
    )
  }

  if (FALSE) {
    plans <- con$Plan$list(limit = 100L)
    plans <- jsonlite::fromJSON(as.character(plans))
    purrr::walk(
      plans$data, function(x) {
        con$Plan$delete(x$id)
      }
    )
  }

  if (FALSE) {
    postgres$table_drop("customers", dbname = "stripe")
    postgres$table_drop("payment_methods", dbname = "stripe")
    postgres$table_drop("plans", dbname = "stripe")
  }

  # stripe$stripe_one_time_payment(
  #   customer_id = payment_method$customer_id,
  #   payment_method_id = payment_method$id,
  #   amount = 100,
  #   live = live
  # )
  #
}
