#' @export
chat_gpt_r <- function(prompt, model = "gpt-3.5-turbo", id_session_chat, system_prompt) {
  {
    box::use(httr)
    box::use(. / chat_gpt_retrieve)
    box::use(tidyr)
    box::use(purrr)
    box::use(tidyr)
  }


  #
  history <- chat_gpt_retrieve$chat_gpt_retrieve(id_session_chat)


  if (nrow(history)) {
    #

    promptHistory <- history[, c("prompt", "chatGPT")]
    names(promptHistory) <- c("user", "assistant")

    promptHistory <- tidyr$pivot_longer(
      promptHistory,
      names_to = "role", values_to = "content", cols = c("user", "assistant")
    )
    prompt <- data.frame(
      role = "user",
      content = prompt
    )

    prompt <- rbind(
      promptHistory,
      prompt
    )
  } else {
    # system, user, assistant
    prompt <- data.frame(
      role = "user",
      content = prompt
    )
  }

  prompt <- rbind(
    data.frame(role = "system", content = system_prompt),
    prompt
  )

  response <- httr$POST(
    url = "https://api.openai.com/v1/chat/completions",
    httr$add_headers(Authorization = paste("Bearer", Sys.getenv("OPEN_API"))),
    httr$content_type("application/json"),
    encode = "json",
    body = list(
      model = model,
      messages = purrr$map2(prompt$role, prompt$content, function(role, content) {
        list(role = role, content = content)
      })
    )
  )
  # }


  if (httr$status_code(response) > 200) {
    result <- trimws(httr$content(response)$error$message)
  } else {
    result <- trimws(httr$content(response)$choices[[1]]$message$content)
  }

  return(result)
}

#' @export
execute_at_next_input <- function(expr, session = shiny::getDefaultReactiveDomain()) {
  box::use(shiny)
  shiny$observeEvent(
    once = TRUE,
    shiny$reactiveValuesToList(session$input),
    {
      force(expr)
    },
    ignoreInit = TRUE
  )
}
