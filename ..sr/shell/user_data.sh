#!/bin/bash

mkdir -p /var/log/ndexr/ssh

LOGFILE='/var/log/ndexr/build.log'
savelog "$LOGFILE"
exec &> >(tee "$LOGFILE")

echo "NDEXR_STARTED"

echo "alias tlog=\"tail -f /var/log/ndexr/build.log\"" >> /home/ubuntu/.bashrc

{
  echo "#!/bin/bash"
  echo "certbot --nginx -d ndexr.com -d www.ndexr.com"
  echo "make login"
  echo "docker compose up -d"
} >> /home/ubuntu/start

apt update -qq
apt install --no-install-recommends software-properties-common dirmngr whois

apt install -y apt-transport-https ca-certificates curl nginx python3-pip python3-bottle \
  gnupg lsb-release unzip \
  make nginx certbot python3-certbot-nginx


groupadd docker
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/" -y


apt install -y gdebi r-base r-base-dev libssl-dev zsh libcurl4-openssl-dev libgit2-dev \
  libxml2-dev cmake libcairo2-dev dirmngr libcurl4-openssl-dev libssh2-1-dev \
  zlib1g-dev make git libcurl4-openssl-dev libpq-dev cmake \
  libsodium-dev libsasl2-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev \
  libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev default-jre default-jdk python3.10-venv \
  libhiredis-dev libssh-dev npm awscli

ufw allow 'Nginx Full'
systemctl restart nginx

echo "NGINX_COMPLETE"

pip install glances

glances -w &

cat <<EOT >> /etc/systemd/system/glances.service
[Unit]
Description=Glances
After=network.target

[Service]
ExecStart=/usr/local/bin/glances -w
Restart=on-abort
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOT

systemctl enable glances.service
systemctl start glances.service


echo "GLANCES_COMPLETE"

mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt update
apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker}
usermod -aG docker ubuntu

echo "DOCKER_COMPLETE"

echo "deb http://security.ubuntu.com/ubuntu focal-security main" | tee /etc/apt/sources.list.d/focal-security.list

apt-get update
apt-get install libssl1.1

rm /etc/apt/sources.list.d/focal-security.list

apt-get install gdebi-core -y
apt-get install gdebi -y

# Install RStudio Server
wget https://download2.rstudio.org/server/jammy/amd64/rstudio-server-2023.03.0-386-amd64.deb
gdebi -n rstudio-server-2023.03.0-386-amd64.deb

echo "RSTUDIO_COMPLETE"
# # Installing Gitlab
# apt-get update
# apt-get install -y curl openssh-server ca-certificates tzdata perl
# curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | bash
# cat /etc/gitlab/initial_root_password
# cat 'apt-get install -y postfix'
#

wget https://github.com/cdr/code-server/releases/download/v3.3.1/code-server-3.3.1-linux-amd64.tar.gz
tar -xzvf code-server-3.3.1-linux-amd64.tar.gz
cp -r code-server-3.3.1-linux-amd64 /usr/lib/code-server
ln -s /usr/lib/code-server/bin/code-server /usr/bin/code-server
mkdir /var/lib/code-server

cat <<EOT >> /lib/systemd/system/code-server.service

[Unit]
Description=code-server
After=nginx.service

[Service]
Type=simple
Environment=PASSWORD=your_password
ExecStart=/usr/bin/code-server --bind-addr 127.0.0.1:8080 --user-data-dir /var/lib/code-server --auth password
Restart=always

[Install]
WantedBy=multi-user.target
EOT

echo "VSCODE_COMPLETE"

systemctl start code-server
systemctl enable code-server
echo "installation complete"


curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
nvm install 16
npm i -g yarn


sudo apt install fontconfig
cd ~
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Meslo.zip
mkdir -p .local/share/fonts
unzip Meslo.zip -d .local/share/fonts
cd .local/share/fonts
rm *Windows*
cd ~
rm Meslo.zip
fc-cache -fv
