#' @export
ui_user_universal <- function(id = "user_universal", credentials) {
  {
    box::use(shiny = shiny[tags])
    box::use(.. / users / console_users)
    box::use(.. / aws / secrets / user_secrets)
    box::use(.. / aws / costs)
    box::use(.. / inputs / inputs)
    box::use(glue)
  }
  ns <- shiny$NS(id)
  tags$div(
    class = "row",
    tags$div(
      class = glue$glue("col-12 mb-3"),
      tags$h5("ndexr"),
      tags$div(class = "p-1 bg-secondary rounded mb-3"),
      tags$div(
        class = "row",
        inputs$card_collapse(
          ns,
          title = "Account",
          body = console_users$ui_user_password_management(ns("user_password_management"), email = credentials$email, admin = FALSE, login = FALSE), collapsed = TRUE
        )
      )
    ),
    tags$div(
      class = glue$glue("col-12 mb-3"),
      tags$h5("aws"),
      tags$div(class = "p-1 bg-secondary rounded mb-3"),
      tags$div(class = "row", user_secrets$ui_user_secrets(ns("user_secrets"))),
      tags$div(class = "row", costs$ui_aws_costs(ns("aws_costs")))
    )
  )
}

#' @export
server_user_universal <- function(id = "user_universal", ns_common_store_user) {
  box::use(shiny = shiny[tags])
  box::use(.. / users / console_users)
  box::use(.. / inputs / inputs)
  box::use(glue)
  box::use(.. / aws / costs)
  box::use(.. / aws / secrets / user_secrets)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      console_users$server_console_users("user_password_management")
      costs$server_aws_costs("aws_costs", ns_common_store_user)
      user_secrets$server_user_secrets("user_secrets", ns_common_store_user)
    }
  )
}
