#' @export
ui_pass_aws <- function(id = "pass_aws") {
  box::use(shiny = shiny[tags, HTML])
  ns <- shiny$NS(id)
  tags$div(
    `data-bs-toggle` = "tooltip", `data-bs-placement` = "top",
    title = "Pass aws creds to server",
    shiny$actionButton(ns("passAWS"), shiny$icon("aws"), class = "btn btn-primary ")
  )
}

#' @export
server_pass_aws <- function(id = "pass_aws", ec2data) {
  box::use(shiny = shiny[tags, HTML])
  box::use(readr)
  box::use(glue)
  box::use(.. / .. / .. / connections / ssh)
  box::use(.. / .. / .. / connections / postgres)
  box::use(.. / .. / .. / inputs / inputs)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      shiny$observeEvent(
        input$passAWS,
        {
          shiny$showModal(
            inputs$modalDialog(
              tags$div(
                tags$div(
                  class = "row",
                  tags$div(
                    class = "col-12",
                    tags$div(
                      class = "card",
                      tags$div(class = "card-header", "Send AWS Creds")
                    ),
                    tags$div(
                      cass = "card-body",
                      shiny$selectizeInput(ns("users"), "Users", c("fdrennan", "ubuntu"), "fdrennan")
                    ),
                    tags$div(
                      class = "card-footer",
                      shiny$actionButton(ns("send"), "Upload")
                    )
                  )
                )
              )
            )
          )
        }
      )


      shiny$observeEvent(
        input$send,
        {
          # ec2data$username <- 'ubuntu'
          ssh$ssh_upload_file(ec2data, "~/.aws/config", ".aws")
          ssh$ssh_upload_file(ec2data, "~/.aws/credentials", ".aws")
          shiny$showNotification("Creds send")
        }
      )
    }
  )
}
