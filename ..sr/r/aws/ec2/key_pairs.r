#' @export
list_key_pair <- function(KeyName = NULL, delete = FALSE, ns_common_store_user = NULL) {
  box::use(purrr)
  box::use(.. / client)
  box::use(purrr[map_chr])
  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
  kp <- ec2$describe_key_pairs()
  purrr$map_chr(kp$KeyPairs, ~ .$KeyName)
}

#' @export
manage_key_pair <- function(KeyName = NULL, ns_common_store_user = NULL) {
  {
    box::use(.. / client)
    box::use(.. / secrets / secrets)
    box::use(.. / .. / connections / redis)
  }

  ec2 <- client$client("ec2", ns_common_store_user)
  KeyMaterial <- ec2$create_key_pair(KeyName = KeyName)$KeyMaterial
  secrets$secret_create(ns_common_store_user(KeyName), KeyMaterial)
  redis$store_state(ns_common_store_user(KeyName), KeyMaterial)
  KeyMaterial
}
