#' @export
ui_manage_security_group <- function(id = "manage_security_group", ns_common_store_user) {
  box::use(shiny = shiny[tags, HTML])
  box::use(.. / ec2 / aws_security_group_manager)
  box::use(.. / ec2 / security_groups[list_security_groups])
  box::use(.. / .. / inputs / inputs)

  box::use(.. / .. / users / ip_address)
  securityGroups <- list_security_groups(ns_common_store_user = ns_common_store_user)
  ns <- shiny$NS(id)
  inputs$card_collapse(
    ns,
    title = tags$div(
      "Security Groups",
      ip_address$server_user_ip("user_ip")
    ),
    body = tags$div(
      tags$div(
        class = "mb-3",
        tags$div(
          class = "mb-3",
          shiny$uiOutput(ns("portsSelected"))
        ),
        shiny$uiOutput(ns("existingSG"))
      ),
      shiny$uiOutput(ns("newSg"))
    )
  )
}

#' @export
server_manage_security_group <- function(id = "manage_security_group", ns_common_store_user) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / ec2 / aws_security_group_manager)
    box::use(.. / ec2 / security_groups[list_security_groups])
    box::use(.. / ec2 / security_groups)
    box::use(.. / s3 / s3)
    box::use(.. / client)
    box::use(.. / ec2 / key_pairs)
    box::use(glue)
    box::use(utils)
    box::use(uuid)
    box::use(purrr)
    box::use(.. / client)
    box::use(fs)
    box::use(cli)
    box::use(readr)
    box::use(.. / .. / connections / redis)
    box::use(.. / ec2 / ec2_backend)
    box::use(.. / secrets / secrets)
    box::use(.. / .. / inputs / inputs)
    box::use(. / manage_security_group_port)
    box::use(. / ip_labeler)
    box::use(.. / .. / users / ip_address)
  }


  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      redux <- redis$get_state(ns_common_store_user("ec2_settings"))

      aws_security_group_manager$server_aws_security_group_manager("aws_security_group_manager")
      try(ip_labeler$server_ip_labeler("ip_labeler", ns_common_store_user))

      ip_address$ui_user_ip(ns("user_ip"))

      output$portsSelected <- shiny$renderUI({
        input$refresh
        tags$div(
          tags$form(
            class = "row",
            tags$div(
              class = "col-6",
              shiny$textInput(ns("GroupName"), "Security Group Name", "ndexrgroup")
            ),
            tags$div(
              class = "col-6",
              tags$label(
                `for` = ns("ipaddress"),
                class = "form-label",
                "IP Address:"
              ),
              tags$input(
                type = "text",
                id = ns("ipaddress"),
                name = ns("ipaddress"),
                class = "form-control"
              ),
              tags$em(
                id = ns("ipaddressId"),
                name = ns("ipaddressId")
              )
            ),
            tags$div(
              class = "col-12 d-flex justify-content-end",
              tags$button(
                id = ns("importGroups"),
                type = "button",
                class = "btn btn-info action-button me-2",
                "Import Security Groups"
              ),
              tags$button(
                id = ns("makeSecurityGroup"),
                type = "button",
                class = "btn btn-primary action-button me-2 me-2",
                "Create"
              )
            )
          ),
          tags$script(
            HTML(
              paste0("fetch('https://api.ipify.org/?format=json')
              .then(response => response.json())
              .then(data => {
                document.getElementById('", ns("ipaddressId"), "').value ='Your IP address is: ' +  data.ip + '/32';
                document.getElementById('", ns("ipaddressId"), "').placeholder = data.ip + '/32';
              });")
            )
          ),
          shiny$uiOutput(ns("portsPanel"), container = function(...) tags$div(...))
        )
      })


      shiny$observeEvent(input$makeSecurityGroup, {
        shiny$showNotification("makeSecurityGroup")

        shiny$showNotification(glue$glue("Building for {input$ipaddress}"))
        securityGroups <- security_groups$list_security_groups(ns_common_store_user = ns_common_store_user)
        GroupName <- input$GroupName

        tryCatch(
          {
            if (!isTRUE(GroupName %in% securityGroups)) {
              security_groups$manage_security_groups(
                GroupName = input$GroupName,
                ns_common_store_user = ns_common_store_user
              )
            } else {
              shiny$showNotification(glue$glue("Updating {GroupName}"))
            }
          },
          error = function(err) {
            shiny$showNotification(as.character(err))
            shiny$req(FALSE)
          }
        )

        GroupName <- input$GroupName

        try({
          for (port in c(22, 80, 443)) {
            security_groups$security_group_envoke(
              ns_common_store_user = ns_common_store_user,
              GroupName = GroupName, ports = port, ips = "0.0.0.0/0"
            )
          }
        })


        for (port in c(5432, 6379, 8787, 8080, 61208)) {
          try({
            security_groups$security_group_envoke(
              ns_common_store_user = ns_common_store_user,
              GroupName = GroupName, ports = port, ips = glue$glue("{input$ipaddress}/32")
            )
          })
        }

        redis$store_state(
          ns_common_store_user("ec2_settings"),
          shiny$reactiveValuesToList(input)
        )

        shiny$showNotification("Security Group Created")
      })

      shiny$observeEvent(
        input$importGroups,
        {
          box::use(.. / .. / security / security)
          box::use(uuid)
          securityGroups <- security_groups$list_security_groups(ns_common_store_user)

          output$portsPanel <- shiny$renderUI({
            tags$div(
              tags$h5("Active Groups"),
              tags$div(class = "p-1 bg-secondary rounded mb-3"),
              purrr$map(securityGroups, function(x) {
                id <- uuid$UUIDgenerate()
                ns_id <- paste0(id, x)
                out <- security$ui_sgmanage(ns(ns_id))
                security$server_sgmanage(ns_id, x, ns_common_store_user)
                out
              })
            )
          })
        }
      )
      # output$existingSG <- shiny$renderUI({
      #   box::use(.. / .. / security / security)
      #   box::use(uuid)
      #   securityGroups <- security_groups$list_security_groups(ns_common_store_user)
      #
      #   output$portsPanel <- shiny$renderUI({
      #     tags$div(
      #       tags$h5("Active Groups"),
      #       tags$div(class = "p-1 bg-secondary rounded mb-3"),
      #       purrr$map(securityGroups, function(x) {
      #         id <- uuid$UUIDgenerate()
      #         ns_id <- paste0(id, x)
      #         out <- security$ui_sgmanage(ns(ns_id))
      #         security$server_sgmanage(ns_id, x, ns_common_store_user)
      #         out
      #       })
      #     )
      #   })
      # })
    }
  )
}



#' @export
ui_drop_elastic_ips <- function(id = "drop_elastic_ips") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_drop_elastic_ips <- function(id = "drop_elastic_ips") {
  box::use(shiny = shiny[tags])
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        shiny$div("drop_elastic_ips")
      })
    }
  )
}
