#' @export
ui_services <- function() {
  box::use(shiny = shiny[withTags])
  withTags(
    section(
      class = "page-section", id = "services",
      div(
        class = "",
        div(
          h2(class = "section-heading text-uppercase text-center", "About Us"),
          h3(
            class = "section-subheading text-muted text-left",
            h5("We build full-stack, scientific applications using modular, reusable design patterns.")
          ),
          p(
            class = "section-subheading text-left",
            "Software development as it currently exists is broken. The R language is well-positioned to be the best language for working on the web in the next decade."
          ),
          p(
            class = "section-subheading text-left",
            "Applications that are supercharged by Python and R have the best tools for minimizing the time between data and the consumer, with solid performance and esthetics. With rock-solid interoperability between the best languages in data science and web development, we can help you build a tech startup today, a modern dashboard tomorrow, or an online store - we provide the code architecture and guidance you need to deliver software to clients."
          )
        ),
        div(
          class = "row",
          div(
            class = "col-md-4 text-center",
            span(
              class = "fa-stack fa-4x",
              i(class = "fas fa-circle fa-stack-2x "),
              i(class = "fas fa-clock fa-stack-1x fa-inverse")
            ),
            h4(class = "my-3", "Your First Hire"),
            p(
              class = "text-muted text-justify",
              "The path to production should be more straightforward and visible to everyone at a lower cost, with added time for creativity - not for catch up. We work with the c-suite, local business owners, and developers to take projects from a proof of concept into production."
            )
          ),
          div(
            class = "col-md-4 text-center",
            span(
              class = "fa-stack fa-4x",
              i(class = "fas fa-circle fa-stack-2x "),
              i(class = "fas fa-user-astronaut fa-stack-1x fa-inverse"),
            ),
            h4(class = "my-3", "Modern Tools"),
            p(
              class = "text-muted text-justify",
              "A modern application has clear and defined methods for using the best features from languages like Rust, Python, and R. It must enhance creativity and minimize cognitive load for engineers."
            )
          ),
          div(
            class = "col-md-4 text-center",
            span(
              class = "fa-stack fa-4x",
              i(class = "fas fa-circle fa-stack-2x "),
              i(class = "fas fa-hot-tub-person fa-stack-1x fa-inverse")
            ),
            h4(class = "my-3", "Effective Design"),
            p(
              class = "text-muted text-justify",
              "We are building the smoothest experience for putting software on the web with total control and absolute visibility for everyone."
            )
          )
        )
      )
    )
  )
}
