#' @export
ui_current_servers <- function(id = "current_servers") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(shinyWidgets)
    box::use(.. / .. / inputs / inputs)
    box::use(bs4Dash)
  }
  ns <- shiny$NS(id)
  tags$div(
    tags$div(
      class = "d-flex justify-content-around align-items-center",
      shiny$actionButton(ns("pullServers"),
        tags$h5(
          "Refresh Server List"
        ),
        class = "btn-info mb-2"
      )
    ),
    shiny$uiOutput(ns("serverLanding"))
  )
}



#' @export
server_current_servers <- function(id = "current_servers", ns_common_store_user, credentials) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(. / ec2)
    box::use(dplyr)
    box::use(glue)
    box::use(uuid)
    box::use(. / instance_manage)
    box::use(.. / .. / connections / redis)
    box::use(dplyr)
    box::use(. / instance_types_get)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      InstanceTypes <- instance_types_get$instance_types_get()

      shiny$observeEvent(input$pullServers, {
        output$serverLanding <- shiny$renderUI({
          tags$div(id = ns("serverPanel"))
        })

        instances <- instance_manage$describe_instances(
          ns_common_store_user = ns_common_store_user,
          state_keep = c("running", "stopping", "starting", "stopped", "pending")
        )

        if (nrow(instances) == 0) {
          shiny$showNotification("No known instances available.")
          shiny$req(FALSE)
        }

        InstanceId <- instances$InstanceId

        shiny$removeUI(paste0("#", ns("servers")))
        shiny$insertUI(
          selector = paste0("#", ns("serverPanel")),
          ui = tags$div(id = ns("servers"), class = "row")
        )



        n_instances <- nrow(instances)

        for (id in InstanceId) {
          uuid_new <- uuid$UUIDgenerate()

          ui_id <- paste0(uuid_new, ns_common_store_user(id))

          shiny$removeUI(selector = paste0("#", ui_id))
          instance <- instances[instances$InstanceId == id, ]
          n_instances <- nrow(instances)

          shiny$insertUI(
            selector = paste0("#", ns("servers")),
            where = "beforeEnd",
            tags$div(class = "col-lg-12", ec2$ui_ec2(ns(ui_id)))
          )

          returnedResults <- ec2$server_ec2(
            id = ui_id,
            instance = instance,
            InstanceTypes = InstanceTypes,
            ns_common_store_user = ns_common_store_user,
            credentials = credentials
          )

          returnedResults()
        }
      })
    }
  )
}
