if (getOption("docker")) {
  Sys.setenv(SSH_KEY_HOME = "/root/.ssh/")
} else {
  Sys.setenv(SSH_KEY_HOME = "./.ssh/")
}
