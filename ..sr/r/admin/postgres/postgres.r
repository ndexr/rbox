#' @export
ui_postgres_table <- function(id = "postgres_table", table_name = "instance_state") {
  box::use(shiny = shiny[tags, HTML])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_postgres_table <- function(id = "postgres_table", table_name = "instance_state") {
  box::use(shiny = shiny[tags, HTML])
  box::use(.. / .. / connections / postgres)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      output$ui <- shiny$renderUI({
        shiny$req(table_name %in% postgres$tables_list())
        tags$div(
          class = "",
          tags$div(
            class = "row",
            tags$div(
              class = "col-12",
            ),
            tags$div(
              class = "col-12 d-flex justify-content-between align-items-center",
              tags$h4(table_name),
              tags$div(
                class = "d-flex align-items-center",
                shiny$actionButton(ns("drop"), "Drop Table", class = "btn btn-primary "),
                shiny$actionButton(ns("refresh"), "Refresh Table", class = "btn-success ")
              )
            ),
            tags$div(
              class = "col-12 scrollmenu p-4",
              shiny$dataTableOutput(ns("table"))
            )
          )
        )
      })

      shiny$observeEvent(input$drop, {
        postgres$table_drop(table_name)
        shiny$showNotification("...poof!")
      })

      output$table <- shiny$renderDataTable({
        shiny$req(input$refresh)
        instance_state <- postgres$table_get(table_name)
        shiny$req(nrow(instance_state) > 0)
        instance_state <- instance_state[!is.na(instance_state$InstanceId), ]
        instance_state$user_data <- NULL
        instance_state
      })
    }
  )
}
