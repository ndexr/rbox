#' @export
ui_manage_security_group_port <- function(id = "manage_security_group_port") {
  box::use(shiny = shiny[tags, HTML])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"), container = function(...) tags$div(class = "col-sm-6", ...))
}

#' @export
server_manage_security_group_port <- function(
    id = "manage_security_group_port", sg) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / .. / inputs / inputs)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        IpPermissions <- sg$IpPermissions
        IpPermissions <- strsplit(IpPermissions, ", ")[[1]]
        #
        out <- tags$div(
          class = "row",
          tags$div(
            class = "col-12",
            tags$h5("Section under construction")
          ),
          tags$div(
            class = "col-12",
            tags$h4("Whitelist"),
            {
              out <- shiny$selectizeInput(ns("ipAddresses"), "Ip Addresses", IpPermissions, IpPermissions, multiple = TRUE, options = list(create = TRUE))
              out
            }
          ),
          tags$div(
            class = "col-sm-2",
            shiny$actionButton(ns("delete"), "Delete Group")
          ),
          tags$div(
            class = "col-sm-2",
            shiny$actionButton(ns("apply"), "Apply Changes")
          )
        )

        tags$div(
          class = "row",
          inputs$card_collapse(
            ns,
            title = paste0(c(sg$GroupName, sg$FromPort, sg$ToPort), collapse = "-"),
            body = out, width = 12
          )
        )
      })


      shiny$observeEvent(input$delete, {
        shiny$showNotification("delete Under Construction")
      })


      shiny$observeEvent(input$apply, {
        shiny$showNotification("apply Under Construction")
      })



      shiny$observeEvent(input$delete, {
        securityGroups <- security_groups$list_security_groups(ns_common_store_user = ns_common_store_user)
        shiny$showNotification("Revoking")
        tryCatch(
          {
            security_groups$security_group_revoke(
              ns_common_store_user = ns_common_store_user,
              GroupName = sg$GroupName, ports = port, ips = input$ipAddresses
            )
          },
          error = function(err) {
            shiny$showNotification(tags$pre(as.character(err)))
          }
        )
      })

      shiny$observeEvent(input$apply, {
        securityGroups <- security_groups$list_security_groups(ns_common_store_user = ns_common_store_user)

        shiny$showNotification("Envoking")

        tryCatch(
          {
            security_groups$security_group_envoke(
              ns_common_store_user = ns_common_store_user,
              GroupName = sg$GroupName, ports = port, ips = input$ipAddresses
            )
          },
          error = function(err) {
            shiny$showNotification(tags$pre(as.character(err)))
          }
        )
      })
    }
  )
}
