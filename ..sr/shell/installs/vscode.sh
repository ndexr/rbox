#!/bin/bash


cat <<EOT >> /etc/systemd/system/code-server.service
[Unit]
Description=code-server
After=nginx.service

[Service]
Type=simple
Environment=PASSWORD=your_password
ExecStart=/usr/bin/code-server --bind-addr rxedn:9050 --user-data-dir /var/lib/code-server --auth none
Restart=always

[Install]
WantedBy=multi-user.target
EOT

# Install vscode server
runuser -l ubuntu -c 'curl -fsSL https://code-server.dev/install.sh | sh'
runuser -l ubuntu -c 'sudo systemctl start code-server@$USER'
runuser -l ubuntu -d 'sudo systemctl enable --now code-server@$USER --bind-addr rxedn:9050'
# code-server --bind-addr 192.168.0.68:8080
