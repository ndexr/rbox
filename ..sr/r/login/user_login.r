#' #' @export
#' ui_user_login <- function(id = "login") {
#'   box::use(shiny = shiny[tags, HTML])
#'   box::use(shinyjs)
#'   ns <- shiny$NS(id)
#'   shiny$uiOutput(ns("ui"))
#' }
#'
#' #' @export
#' server_user_login <- function(id = "login", logged_in = TRUE) {
#'   {
#'     box::use(shiny = shiny[tags, HTML])
#'     box::use(shinyjs)
#'     box::use(shinyvalidate)
#'     box::use(glue)
#'     box::use(jsonlite)
#'     box::use(DBI)
#'     box::use(httr)
#'     box::use(shiny.router)
#'     box::use(.. / aws / secrets / users)
#'     box::use(.. / connections / postgres)
#'     box::use(uuid)
#'     box::use(shinyjs[js])
#'     box::use(shinyWidgets)
#'     box::use(.. / inputs / inputs)
#'     box::use(.. / users / users)
#'     box::use(uuid)
#'     box::use(shinyvalidate)
#'     box::use(shiny.router)
#'     box::use(waiter)
#'     box::use(. / user_login)
#'   }
#'
#'   shiny$moduleServer(
#'     id,
#'     function(input, output, session) {
#'       ns <- session$ns
#'
#'       output$ui <- shiny$renderUI({
#'         tags$div(
#'           id = ns(id),
#'           class = "row",
#'           tags$div(class = "col-xl-4 col-lg-3 col-sm-3"),
#'           tags$div(
#'             class = "col-xl-4 col-lg-6 col-sm-6 well p-4",
#'             tags$h4("Log In", class = "text-center"),
#'             tags$form(
#'               inputs$textInput(ns("email"), label = "su -"),
#'               inputs$textInput(ns("password"), label = "passwd", type = "password"),
#'               tags$div(
#'                 class = "row",
#'                 tags$div(
#'                   class = "col d-flex justify-content-between",
#'                   shiny$actionButton(
#'                     ns("signUp"), "Create an Account",
#'                     class = "btn btn-outline-info"
#'                   ),
#'                   tags$button(
#'                     id = ns("submit"),
#'                     type = "submit",
#'                     class = "btn btn-info action-button",
#'                     "Submit"
#'                   )
#'                 )
#'               )
#'             )
#'           )
#'         )
#'       })
#'
#'       shiny$observeEvent(input$signUp, {
#'         # shiny.router$change_page("user_create")
#'       })
#'
#'       sessionMetadata <- shiny$eventReactive(input$submit, {
#'         w <- waiter$Waiter$new(
#'           id = ns("ui"),
#'           html = tags$img(
#'             src = "./images/spinners/confused-fidget-spinner-fidget-spinner.gif",
#'             class = "img-fluid",
#'             alt = "Confused Fidget Spinner"
#'           )
#'         )
#'
#'         browser()
#'         shiny$showNotification("Checking login status")
#'         session$sendCustomMessage("cookie-get", list(id = ns("cookie")))
#'         if (length(input$cookie) > 0) {
#'           sessionuuid <- input$cookie$sessionuuid
#'
#'           if (!is.null(sessionuuid)) {
#'             shiny$showNotification("checking login state")
#'
#'             con <- postgres$connection_postgres()
#'             on.exit(DBI::dbDisconnect(con))
#'             app_session_start <- DBI::dbGetQuery(con, glue::glue("select * from cookies where value='{sessionuuid}'"))
#'             if (nrow(app_session_start) == 0) {
#'               app_session_start <- data.frame(active = FALSE)
#'             }
#'           } else {
#'             app_session_start <- data.frame(active = FALSE)
#'           }
#'
#'           if (app_session_start$active) {
#'             shiny$showNotification("Already Logged in")
#'           } else {
#'             shiny$showNotification("no active session")
#'             app_session_start <- user_login$handle_login(
#'               ns("handle_login"),
#'               ns, session, input$email,
#'               input$password, uuid$UUIDgenerate()
#'             )
#'           }
#'         } else {
#'
#'         }
#'         app_session_start
#'       })
#'
#'       sessionMetadata
#'     }
#'   )
#' }
#'
#' #' @export
#' console_add_user <- function(username, password) {
#'   box::use(.. / connections / postgres)
#'
#'   console_users <- data.frame(
#'     username = username, password = password
#'   )
#'   postgres$table_create_or_upsert(console_users, "username")
#' }
#'
#'
#' #' @export
#' handle_login <- function(id, ns, session, email, password, uuid) {
#'   box::use(shiny = shiny[tags], waiter)
#'   box::use(.. / users / users)
#'   box::use(bcrypt)
#'   box::use(.. / connections / postgres)
#'
#'   shiny$moduleServer(
#'     id,
#'     function(input, output, session) {
#'       w <- waiter$Waiter$new(
#'         id = ns("ui"),
#'         html = tags$img(
#'           src = "./images/spinners/confused-fidget-spinner-fidget-spinner.gif",
#'           class = "img-fluid",
#'           alt = "Confused Fidget Spinner"
#'         )
#'       )
#'
#'       # log email
#'       app_session_start <- data.frame(email = email, session_id = uuid, time = as.numeric(Sys.time()))
#'       postgres$table_append(app_session_start)
#'       app_session_start <- as.list(app_session_start)
#'
#'       # Check if email exists
#'       users_table <- users$users_table()
#'
#'       if (!email %in% users_table$email) {
#'         shiny$showNotification("not valid")
#'         shiny$req(FALSE)
#'       }
#'
#'       hash <- users_table[users_table$email == email, ]$password
#'       if (!bcrypt$checkpw(password, hash)) {
#'         shiny$showNotification("not valid")
#'         msg <- list(name = "sessionuuid", id = ns("cookie"))
#'         cookies <- data.frame(msg, active = FALSE, email = email)
#'         session$sendCustomMessage("cookie-remove", msg)
#'         postgres$table_create_or_upsert(cookies, where_cols = "name")
#'         shiny$req(FALSE)
#'       } else {
#'         msg <- list(name = "sessionuuid", value = uuid, id = ns("cookie"))
#'         cookies <- data.frame(msg, active = TRUE, email = email)
#'         session$sendCustomMessage("cookie-set", msg)
#'         postgres$table_create_or_upsert(cookies, where_cols = "name")
#'         shiny$showNotification("Lets gooooo!!!!")
#'       }
#'       w$hide()
#'       app_session_start
#'     }
#'   )
#' }
#'
#'
#'
#' if (FALSE) {
#'   box::use(. / user_login)
#'   box::reload(user_login)
#'   # user_login$console_add_user('fdrennan', 'thirdday1')
#' }
