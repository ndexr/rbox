#' @export
user_info_create <- function(user = "ndexr",
                             user_secrets = list(
                               default_region = "us-east-2",
                               account_password = "password",
                               aws_access = Sys.getenv("AWS_ACCESS"),
                               aws_secret = Sys.getenv("AWS_SECRET")
                             )) {
  box::use(jsonlite)
  box::use(. / secrets)
  box::use(uuid)
}




#' #' @export
#' cognito_identity_pool_create <- function(username) {
#'   box::use(.. / client)
#'   box::use(.. / .. / connections / postgres)
#'   client <- client$client("cognito-identity")
#'
#'   idp <- client$create_identity_pool(
#'     IdentityPoolName = "ndexr.com",
#'     AllowUnauthenticatedIdentities = TRUE
#'   )
#'
#'   cognito_identity_pool <- data.frame(
#'     IdentityPoolId = idp$IdentityPoolId,
#'     IdentityPoolName = idp$IdentityPoolName,
#'     time = Sys.time(),
#'     AllowUnauthenticatedIdentities = idp$AllowUnauthenticatedIdentities
#'   )
#'
#'   postgres$table_append(cognito_identity_pool)
#'
#'   cognito_identity_pool
#' }

#' #' @export
#' user_delete <- function(username) {
#'
#' }
# shiny.router$change_page("home")
