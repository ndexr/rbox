#' @export
create_instance <- function(ImageId = "ami-097a2df4ac947655f",
                            InstanceType = "t2.medium",
                            InstanceStorage = 10,
                            user_data = NULL,
                            KeyName = NULL,
                            GroupId = NULL,
                            DryRun = FALSE,
                            DeviceName = "/dev/sda1",
                            ns_common_store_user = NULL) {
  box::use(.. / client)
  box::use(.. / client[resource])
  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
  ec2Res <- resource("ec2", ns_common_store_user = ns_common_store_user)

  response <-
    response <-
    ec2$run_instances(
      ImageId = ImageId,
      InstanceType = InstanceType,
      MinCount = as.integer(1),
      MaxCount = as.integer(1),
      UserData = user_data,
      KeyName = KeyName,
      SecurityGroupIds = list(GroupId),
      BlockDeviceMappings = list(
        list(
          Ebs = list(
            VolumeSize = as.integer(InstanceStorage)
          ),
          DeviceName = DeviceName
        )
      )
    )

  instance <- response$Instances[[1]]
  # InstanceId <- instance_data$Instances[[1]]$InstanceId
}

#' @export
describe_instances <- function(ns_common_store_user = NULL, state_keep = c("starting", "stopping", "stopped", "running")) {
  {
    box::use(purrr)
    box::use(.. / client)
    box::use(dplyr)
    box::use(cli)
    box::use(jsonlite)
  }
  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
  instances <- ec2$describe_instances()
  #
  instances <- purrr$map_dfr(
    instances$Reservations, ~ {
      instance <- .[["Instances"]][[1]]
      df <- as.data.frame(purrr$keep(instance, is.character))
      df$state <- instance$State$Name
      df$SecurityGroups <- jsonlite$toJSON(instance$SecurityGroups)
      df
    }
  )

  if (!"PublicIpAddress" %in% names(instances)) {
    instances$PublicIpAddress <- NA_character_
  }

  cli$cli_h3("Instances")
  if (nrow(instances) == 0) {
    return(data.frame())
  }

  instances <- instances |>
    dplyr$filter(state %in% state_keep)
  # dplyr$select(state, PublicIpAddress, KeyName, InstanceType, InstanceId)

  instances
}

#' @export
instance_state <- function(InstanceId = NULL, ns_common_store_user = NULL) {
  box::use(. / instance_manage[describe_instances])
  instances <- describe_instances(ns_common_store_user = ns_common_store_user)
  state <- instances[instances$InstanceId == InstanceId, ]$state
  state
}
