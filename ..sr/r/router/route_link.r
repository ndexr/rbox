#' @export
route_link <- function(path) {
  box::use(. / route_link)
  paste0("./", route_link$cleanup_hashpath(path))
}

#' @export
cleanup_hashpath <- function(hashpath) {
  hashpath <- hashpath[1]
  if (substr(hashpath, 1, 3) == "#!/" || substr(
    hashpath, 1,
    1
  ) == "?") {
    return(hashpath)
  }
  slicefrom <- 1
  while ({
    substr(hashpath, slicefrom, slicefrom) %in% c(
      "#",
      "!", "/"
    )
  }) {
    slicefrom <- slicefrom + 1
  }
  paste0("#!/", substr(hashpath, slicefrom, nchar(hashpath)))
}
