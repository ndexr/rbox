#' @export
ui_contact <- function(heading = "", subheading = "", ...) {
  box::use(shiny[withTags])
  withTags(
    section(
      class = "page-section", id = "contact",
      div(
        class = "",
        div(
          class = "text-center",
          h2(class = "section-heading text-uppercase", heading),
          h3(class = "section-subheading text-muted", subheading)
        ),
        ...
      )
    )
  )
}
