#' @export
ui_waitlist <- function(id = "waitlist") {
  box::use(shiny = shiny[tags, HTML])
  box::use(.. / inputs / inputs)
  ns <- shiny$NS(id)
  inputs$card_collapse(
    ns,
    title = "Waitlist",
    body = shiny$uiOutput(ns("waitlist")),
    collapsed = TRUE
  )
}

#' @export
server_waitlist <- function(id = "waitlist") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / connections / postgres)
    box::use(.. / connections / redis)
    box::use(.. / connections / ssh)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      output$waitlist <- shiny$renderUI({
        waitlist <- postgres$table_get("waitlist")

        tags$div(
          class = "container",
          tags$table(
            class = "table table-striped",
            tags$thead(
              tags$tr(
                tags$th(
                  scope = "col",
                  "name"
                ),
                tags$th(
                  scope = "col",
                  "email"
                ),
                tags$th(
                  scope = "col",
                  "phone"
                ),
                tags$th(
                  scope = "col",
                  "company"
                )
              )
            ),
            tags$tbody(
              purrr::map(
                split(waitlist, 1:nrow(waitlist)), function(x) {
                  tags$tr(
                    tags$td(x$name),
                    tags$td(x$email),
                    tags$td(x$phone),
                    tags$td(x$company)
                  )
                }
              )
            )
          )
        )
      })
    }
  )
}
