#' @export
list_security_groups <- function(ns_common_store_user = NULL, full = FALSE) {
  box::use(purrr)
  box::use(.. / client)
  box::use(jsonlite)
  box::use(dplyr)
  box::use(tibble)
  # box::use(shiny)
  # ns_common_store_user <- shiny$NS("stoian.andrei.mircea@gmail.com")
  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
  security_groups <- ec2$describe_security_groups()

  # unlist(security_groups$SecurityGroups[[1]])

  if (!full) {
    sgNames <- purrr::map_chr(security_groups$SecurityGroups, function(x) x$GroupName)
    return(sgNames)
  }

  #

  # security_groups$SecurityGroups
  permissions <- purrr$map_dfr(
    security_groups$SecurityGroups, function(x) {
      # stoian.andrei.mircea@gmail.com
      IpPermissions <- unlist(x$IpPermissions)
      if (is.null(IpPermissions)) {
        return(data.frame())
      }
      IpPermissions <- split(
        IpPermissions, cumsum(names(IpPermissions) == "FromPort")
      )

      permissions <- purrr$map_dfr(
        IpPermissions, function(x) {
          #
          print(names(x))
          #
          if (!"FromPort" %in% names(x)) {
            return(data.frame())
          }
          data.frame(
            FromPort = x[["FromPort"]],
            ToPort = x[["ToPort"]],
            IpRanges = x[names(x) == "IpRanges.CidrIp"]
          )
        }
      )
      if (nrow(permissions) == 0) {
        return(data.frame())
      }
      permissions$VpcId <- x$VpcId
      permissions$GroupName <- x$GroupName
      permissions$GroupId <- x$GroupId
      dplyr$as_tibble(permissions)
    }
  )


  permissions <- tibble$tibble(permissions)
  permissions <- dplyr$arrange(permissions, FromPort)

  permissions
}

#' @export
manage_security_groups <- function(GroupName = "ndexr-sg",
                                   Description = "Security Group for EC2 Servers",
                                   delete = FALSE,
                                   ns_common_store_user = NULL) {
  box::use(.. / client)
  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
  if (delete) {
    return(ec2$delete_security_group(GroupName = GroupName))
  }
  ec2$create_security_group(
    GroupName = GroupName,
    Description = "Security Group by NDEXR"
  )
}

#' @export
security_group_envoke <- function(GroupName = NA, ports = NULL, ips = NULL, ns_common_store_user = NULL) {
  box::use(purrr)
  box::use(.. / client)
  box::use(.. / client)
  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
  grid <- expand.grid(ports, ips)
  grid$id <- 1:nrow(grid)

  colnames(grid) <- c("port", "ip", "id")

  grid$port <- as.integer(as.character(grid$port))

  purrr$walk(
    split(grid, grid$id),
    ~ ec2$authorize_security_group_ingress(
      GroupName = GroupName,
      IpProtocol = "tcp",
      CidrIp = paste0(.$ip),
      FromPort = as.integer(as.character(.$port)),
      ToPort = as.integer(as.character(.$port))
    )
  )
}

#' @export
security_group_revoke <- function(GroupName = NA, ports = NULL, ips = NULL, ns_common_store_user = NULL) {
  box::use(purrr)
  box::use(.. / client)
  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
  grid <- expand.grid(ports, ips)
  grid$id <- 1:nrow(grid)

  colnames(grid) <- c("port", "ip", "id")



  out <- purrr$map(
    split(grid, grid$id),
    function(x) {
      ec2$revoke_security_group_ingress(
        GroupName = GroupName,
        IpProtocol = "tcp",
        CidrIp = paste0(as.character(x$ip)),
        FromPort = as.integer(as.character(x$port)),
        ToPort = as.integer(as.character(x$port))
      )
    }
  )

  out
}
