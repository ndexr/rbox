#' @export
ui_ndexrio <- function(id) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(shinyjs)
    box::use(bs4Dash)
    box::use(. / sidebar / sidebar)
    box::use(. / footer / footer)
    box::use(. / users / subscribe)
    box::use(. / bs4dash / bs4dash)
    box::use(. / router)
    box::use(. / head / head)
  }

  ns <- shiny$NS(id)
  shiny$addResourcePath(prefix = "javascript", directoryPath = "./javascript")
  shiny$addResourcePath(prefix = "css", directoryPath = "./css")
  shiny$addResourcePath(prefix = "images", directoryPath = "./images")

  tags$html(
    lang = "en",
    head$ui_head(),
    tags$body(
      id = "page-top",
      bs4dash$bs4_dashboard(
        dark = TRUE,
        header = tags$div(),
        sidebar = bs4Dash$dashboardSidebar(
          collapsed = TRUE, disable = TRUE
        ),
        body = tags$div(sidebar$ui_sidebar(ns("sidebar")), router$app_router(ns)$ui),
        footer = bs4dash$bs4_dashboard_footer()
      ),
      footer$ui_footer(ns("footer")),
      shiny$includeScript("../node_modules/@popperjs/core/dist/umd/popper.min.js"),
      shiny$includeScript("../node_modules/bootstrap/dist/js/bootstrap.min.js"),
      shiny$includeScript("../node_modules/js-cookie/dist/js.cookie.min.js"),
      shiny$includeScript("./javascript/cookies.js"),
      shiny$includeScript("./javascript/inittooltip.js"),
      tags$link(
        rel = "stylesheet",
        href = "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css"
      ),
      shiny$includeCSS("./css/styles.css", rel = "stylesheet")
    )
  )
}

#' @export
server_ndexrio <- function(id) {
  {
    box::use(shiny = shiny[withTags, tags])
    box::use(. / sidebar / sidebar)
    box::use(. / footer / footer)
    box::use(. / ndexrio)
    box::use(. / router)
    box::use(. / sidebar / site_sidebar)
    box::use(. / bs4dash / bs4dash)
    box::use(. / users / subscribe)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      router$app_router(ns)$server(input, output, session)

      footer$server_footer("footer")
      subscribe$server_subscribe("subscribe", frontpage = TRUE)
      sidebar$server_sidebar("sidebar")
    }
  )
}
