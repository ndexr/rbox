#' @export
ui <- function(request) {
  switch(getOption("ndexr_site"),
    "ndexrconsole" = {
      box::use(. / r / ndexrconsole)
      ndexrconsole$ui_ndexrconsole("ndexrconsole")
    },
    "ndexrio" = {
      box::use(. / r / ndexrio)
      ndexrio$ui_ndexrio("ndexrio")
    }
  )
}
