console:
	docker compose --profile=ndexrconsole pull
	docker compose --profile=ndexrconsole build
	docker compose --profile=ndexrconsole up -d --remove-orphans
	docker compose --profile=ndexrconsole up -d --scale console=1

db:
	docker compose --profile=db pull
	docker compose --profile=db build
	docker compose --profile=db up -d --remove-orphans

lf:
	docker compose --profile=local pull
	docker compose --profile=local build
	docker compose --profile=local up -d --remove-orphans
	docker compose --profile=local logs -f

down:
	sudo fuser -k 8000/tcp | echo ok
	docker compose --profile=local --profile=ndexrio --profile=ndexrconsole down --remove-orphans

push:
	R -e "source('./services/console/scripts/gitpush.r')"

logs:
	docker compose --profile=local logs -f

scaleup:
	docker compose --profile=local up -d --scale console=11

scale:
	docker compose --profile=local up -d --scale console=3

scaledown:
	docker compose --profile=local up -d --scale console=1


ndexrio:
	# docker compose --profile=ndexrio pull --ignore-pull-failures
	docker compose --profile=ndexrio build
	# docker compose --profile=ndexrio push
	docker compose --profile=ndexrio up -d --remove-orphans
	docker compose --profile=ndexrio up --scale ndexrio=2 -d

ndexriodown:
	docker compose --profile=ndexrio down --remove-orphans

ndexrconsole:
	docker compose --profile=ndexrconsole pull --ignore-pull-failures
	docker compose --profile=ndexrconsole build
	docker compose --profile=ndexrconsole push
	docker compose --profile=ndexrconsole up -d --remove-orphans
	docker compose --profile=ndexrconsole up --scale console=3 -d
