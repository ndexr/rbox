#' @export
ui_add_user <- function(id = "add_user", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
  }

  ns <- shiny$NS(id)

  shiny$uiOutput(ns("ui"))
}

#' @export
server_add_user <- function(id = "add_user", ec2data, credentials) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(glue[glue])
    box::use(ssh)
    box::use(../../../inputs/inputs)
    box::use(.. / .. / .. / connections / redis)
    box::use(.. / .. / .. / connections / postgres)
    box::use(stringr)
    box::use(ssh_utils = .. / .. / .. / connections / ssh)
    box::use(.. / .. / .. / gpt/utilities / utilities)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      output$ui <- shiny$renderUI({



        redux <- redis$get_state(ec2data$ns_common_store_user("ec2"))
        username <- redis$setDefault(redux$username, "ndexr")
        password <- redis$setDefault(redux$password, "password")
        # credentials$userdata$email

        out <- ssh_utils$ssh_execute(
          ec2data,
          "cat /etc/nginx/sites-enabled/default"
        )[[2]]

        known_ips <- postgres$tables_row_retrieve(table = 'iplookup', where_cols = 'email', credentials$email)

        locations <- stringr$str_extract_all(out, 'location /.+/')

        tags$div(
          class = "container",
          tags$div(
            class = "row",
            tags$div(
              class = "col-12",
              tags$h5("Add a user to the server"),
              inputs$textInput(ns("username"), "Username", username),
              inputs$passwordInput(ns("password"), "Password")
            ),
            tags$div(
              class = "col-12 d-flex justify-content-end my-2",
              shiny$actionButton(ns("addUser"), "Add User", class = "btn btn-primary")
            )
            # tags$div(
            #   class = "col-12",
            #   tags$h5("Allow IP"),
            #   shiny$selectizeInput(ns('ipaddress'), 'Ip Address', stats::setNames(known_ips$ipaddress, known_ips$user)),
            #   shiny$selectizeInput(
            #     ns('locationBlock'), 'Location',
            #     locations[[1]]
            #   ),
            #   shiny$checkboxInput(ns('useGPT'), 'Chat GPT Reformat', value = FALSE)
            # ),
            # tags$div(
            #   class = "col-12 d-flex justify-content-end my-2",
            #   shiny$actionButton(ns("submitIp"), "Submit IP", class='btn-primary')
            # )
          )
        )
      })
      shiny$observeEvent(
        input$submitIp,
        {

          ns_common_store_user <- ec2data$ns_common_store_user

          ec2data <- redis$get_state(ns_common_store_user(ec2data$instance$InstanceId))

          out <- ssh_utils$ssh_execute(
            ec2data,
            "cat /etc/nginx/sites-enabled/default"
          )

          out <- stringr$str_split(out[[2]], input$locationBlock)

          # locationUpdate <- stringr$str_split(out[[1]][2], '{\n')
          # 
          locationUpdate <- stringr$str_split(out[[1]][2], '\\{\n')
          ipaddress <- stringr$str_remove(input$ipaddress, '/.*.$')

          newLine <- paste0('allow ', ipaddress, ';')

          #

          newFile <- paste0(
            c(
              out[[1]][1],
              input$locationBlock,
              newLine,
              paste0(paste0(locationUpdate[[1]], '\n'), collapse = '\n')
            ), collapse = '\n'
          )

          


          if (input$useGPT) {
            newFile <- utilities$chat_gpt_r(
              paste0("Please reformat the following nginx file. Only return the file, do not add any commentary at all. Make sure all allow statements are together. Remove commented out lines.", newFile, collapse = '\n\n'), "gpt-3.5-turbo", uuid::UUIDgenerate(), 'You are an expert nginx administrator.'
            )
          }


          tmp <- tempdir()
          file <- paste0(tmp,'/default')
          readr::write_file(newFile, file)

          shiny$showNotification('Backup old file')
          ssh_utils$ssh_execute(ec2data, command = 'sudo cp /etc/nginx/sites-enabled/default /etc/nginx/default.bak')
          shiny$showNotification('Upload new file')
          ssh_utils$ssh_upload_file(ec2data, file, "/etc/nginx/sites-enabled")
          shiny$showNotification('Restart nginx')
          ssh_utils$ssh_execute(ec2data, 'sudo systemctl restart nginx')
          status <- ssh_utils$ssh_execute(ec2data, 'sudo systemctl status nginx')
          shiny$showModal(
            inputs$modalDialog(
              tags$pre(paste0(c(status, newFile), collapse = "\n"))
            )
          )
        }
      )

      shiny$observeEvent(
        input$addUser,
        {
          #
          ns_common_store_user <- ec2data$ns_common_store_user

          ec2data <- redis$get_state(ns_common_store_user(ec2data$instance$InstanceId))

          if (isFALSE(shiny$isRunning)) {
            shiny$showNotification("Server not running")
            shiny$req(FALSE)
          } else {
            shiny$showNotification("Adding User")
          }

          input <- shiny$reactiveValuesToList(input)

          input <- lapply(input, function(x) gsub(" ", "", x))
          redis$store_state(ec2data$ns_common_store_user("ec2"), input)

          ssh_utils$ssh_execute(
            ec2data,
            c(
              paste0("sudo useradd -m -p $(mkpasswd -m sha-512 ", input$password, ")  -s /bin/bash ", input$username),
              glue("sudo usermod -aG docker {input$username}"),
              glue("sudo usermod -aG sudo {input$username}")
            )
          )

          ec2users <- data.frame(
            username = input$username,
            InstanceId = ec2data$instance$InstanceId,
            time = Sys.time()
          )

          postgres$table_append(ec2users)

          shiny$showNotification("User Added")
        }
      )
    }
  )
}
