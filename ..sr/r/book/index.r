#' @export
ui_index <- function(id = "index") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_index <- function(id = "index") {
  box::use(shiny = shiny[tags])
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        shiny$div("index")
      })
    }
  )
}

if (FALSE) {
  setwd("~/rapp/services/console/src/r/book")

  {
    ui <- function() {
      box::use(shiny = shiny[tags])
      shiny$fluidPage(
        shiny$fluidRow(
          shiny$column(
            12, ui_index()
          )
        )
      )
    }
    server <- function(input, output, session) {
      box::use(shiny = shiny[tags])
      server_index()
    }

    box::use(shiny)
    shiny$shinyApp(ui, server)
  }
}
