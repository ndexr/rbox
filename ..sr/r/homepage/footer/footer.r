#' @export
ui_footer <- function() {
  box::use(shiny = shiny[tags, withTags])
  withTags(
    footer(
      class = "footer py-4",
      div(
        class = "",
        div(
          class = "row align-items-center",
          div(class = "col-lg-4 text-lg-start", "ndexr 2022"),
          div(
            class = "col-lg-4 my-3 my-lg-0",
            a(
              class = "btn btn-dark btn-social mx-2",
              target = "_blank",
              href = "https://homepage.linkedin.com/in/freddydrennan/",
              `aria-label` = "LinkedIn", i(class = "fab fa-linkedin-in")
            )
          ),
          div(
            class = "col-lg-4 text-lg-end",
            a(class = "link-dark text-decoration-none me-3", href = "#!", "Privacy Policy"),
            a(class = "link-dark text-decoration-none", href = "#!", "Terms of Use")
          )
        )
      )
    )
  )
}
