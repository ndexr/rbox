#' @export
ui_ec2_navbar <- function(id = "ec2_navbar", ec2data) {
  box::use(shiny = shiny[tags, HTML])
  box::use(glue[glue])
  box::use(stringr)
  ns <- shiny$NS(id)

  rstudio_url_https <- paste0("https://", ec2data$current_domain, "/rstudio/")
  rstudio_url_http <- paste0("http://", ec2data$instance$PublicIpAddress, ":8787")


  if (stringr$str_detect(ec2data$current_domain, "i-")) {
    tags$p(
      rstudio_url_http
    )
  } else {
    tags$div(
      tags$p(
        rstudio_url_http
      ),
      tags$p(
        rstudio_url_https
      )
    )
  }
}
