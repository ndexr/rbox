#' @export
bs4_dash_deps <- function(tag, options) {
  bs4Dash_version <- as.character(utils::packageVersion("bs4Dash"))
  bs4Dash_deps <- list(
    htmltools::htmlDependency(
      name = "AdminLTE3", version = "3.1.0",
      src = c(file = "AdminLTE3-3.1.0"), script = "adminlte.min.js",
      stylesheet = "adminlte.min.css", package = "bs4Dash"
    )
  )
  shiny::tagList(tag, bs4Dash_deps)
}

#' @export
bs4_dashboard_body <- function(...) {
  box::use(shiny = shiny[tags])
  shiny$tags$div(
    class = "",
    shiny$tags$section(
      tags$div(
        class = "row",
        # tags$div(class='md-offset-2'),
        tags$div(
          ...
        )
      )
    )
  )
}

#' @export
bs4_dashboard_footer <- function(...) {
  box::use(shiny = shiny[tags, HTML])
  shiny$tags$footer(
    `data-fixed` = "false",
    shiny$tags$div(
      ...
    )
  )
}

#' @export
bs4_dashboard <- function(
    header,
    sidebar,
    body,
    controlbar = NULL,
    footer = NULL,
    title = NULL,
    skin = NULL,
    freshTheme = NULL,
    options = NULL,
    fullscreen = TRUE,
    help = FALSE,
    dark = FALSE,
    scrollToTop = FALSE)
{
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(bs4 = . / bs4dash)
  }


  bodyContent <- shiny$tags$div(
    body
  )
  #
  shiny$tagList(
    bs4$bs4_dash_deps(
      # shiny$tags$body(
      #   # `data-help` = ifelse(help, 1, 0),
      #   `data-fullscreen` = ifelse(fullscreen, 1, 0),
      #   `data-dark` = 2,
      #   # `data-scrollToTop` = ifelse(scrollToTop, 2, 1),
      #   # class = bodyCl
      #   bodyContent
      # ),
      bodyContent,
      options = options
    )
  )
}


bs4Dash::dashboardHeader
