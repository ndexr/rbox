#' @export
register_domain_name <- function() {
  box::use(shiny)
  box::use(.. / client)
  box::use(.. / .. / inputs / inputs)
  ns_common_store_user <- shiny$NS("drennanfreddy@gmail.com")
  apigateway <- client$client(service = "swf", ns_common_store_user = ns_common_store_user)
  my_domain <- apigateway$register_domain(
    name = "fdrennan",
    workflowExecutionRetentionPeriodInDays = "0"
  )
  status_code <- my_domain$ResponseMetadata$HTTPStatusCode

  shiny$showModal(
    inputs$modalDialog(
      tags$pre(
        jsonlite::toJSON(my_domain, pretty = T)
      )
    )
  )
}
