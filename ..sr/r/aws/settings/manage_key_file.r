#' @export
ui_manage_key_file <- function(id = "manage_key_file") {
  box::use(shiny = shiny[tags, HTML])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_manage_key_file <- function(id = "manage_key_file", ns_common_store_user, show_rsa = TRUE) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / ec2 / security_groups[list_security_groups])
    box::use(.. / ec2 / security_groups)
    box::use(.. / s3 / s3)
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / client)
    box::use(.. / ec2 / key_pairs)
    box::use(glue)
    box::use(utils)
    box::use(uuid)
    box::use(purrr)
    box::use(.. / client)
    box::use(fs)
    box::use(readr)
    box::use(.. / .. / connections / redis)
    box::use(.. / s3 / s3)
    box::use(.. / secrets / secrets)
    box::use(.. / .. / inputs / inputs)
    box::use(. / manage_key_file)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns



      has_secret <- shiny$reactive({
        has_secret <- secrets$secret_exists(ns_common_store_user)
        if (!has_secret) {
          shiny$invalidateLater(5000)
        }
        has_secret
      })

      shiny$observeEvent(has_secret(), {
        has_secret <- has_secret()
        if (!has_secret) shiny$req(FALSE)
        redux <- redis$get_state(ns_common_store_user("ec2_settings"))
        ec2boto <- client$client("ec2", ns_common_store_user = ns_common_store_user)

        output$ui <- shiny$renderUI({
          # input$refresh
          kp <- ec2boto$describe_key_pairs()
          KeyNames <- purrr$map_chr(kp$KeyPairs, ~ .$KeyName)
          KeyName <- redis$setDefault(redux$KeyName, "ndexrkey")
          if (!length(KeyNames)) KeyNames <- KeyName

          inputs$card_collapse(
            ns,
            title = "SSH", collapsed = TRUE,
            body = {
              tags$form(
                class = "row",
                tags$div(
                  class = "col-sm-8",
                  shiny$textInput(
                    ns("KeyName"),
                    "Key Name",
                    KeyName
                  )
                ),
                tags$div(
                  class = "col-sm-4 d-flex align-items-center justify-content-end",
                  shiny$actionButton(
                    ns("makeKeyPair"), "Create",
                    class = "btn btn-primary"
                  )
                ),
                inputs$title_h5("SSH Keyfile Delete/Download"),
                tags$div(
                  class = "col-12 d-flex justify-content-end",
                  shiny$actionButton(ns("refresh"), "Refresh", class = "btn-primary px-2 py-1")
                ),
                shiny$uiOutput(ns("existingKeyFiles"))
              )
            }
          )
        })
      })

      output$existingKeyFiles <- shiny$renderUI({
        redux <- redis$get_state(ns_common_store_user("ec2_settings"))
        ec2boto <- client$client("ec2", ns_common_store_user = ns_common_store_user)
        kp <- ec2boto$describe_key_pairs()

        #

        if (!length(kp)) {
          return(tags$div("Keyfiles when created will be placed here"))
        }

        KeyNames <- purrr$map_chr(kp$KeyPairs, ~ .$KeyName)

        purrr$map(
          KeyNames, function(x) {
            box::use(uuid)
            uuid <- uuid$UUIDgenerate()
            elemId <- paste0(uuid, x, collapse = "")
            uiout <- manage_key_file$ui_key_file_single(ns(elemId))
            manage_key_file$server_key_file_single(elemId, x, ns_common_store_user)
            uiout
          }
        )
      })

      shiny$observeEvent(c(input$refresh, input$makeKeyPair), {
        redux <- redis$get_state(ns_common_store_user("ec2_settings"))
        ec2boto <- client$client("ec2", ns_common_store_user = ns_common_store_user)

        if (isFALSE(input$KeyName %in% key_pairs$list_key_pair(ns_common_store_user = ns_common_store_user))) {
          tryCatch(
            {
              keyFile <- key_pairs$manage_key_pair(
                input$KeyName, ns_common_store_user
              )
            },
            error = function(err) {
              shiny$showNotification("Try that again")
            }
          )
          shiny$showNotification(glue$glue("{input$KeyName} created and stored"))
        } else {
          shiny$showNotification("Key already exists.")
        }

        output$existingKeyFiles <- shiny$renderUI({
          kp <- ec2boto$describe_key_pairs()
          if (!length(kp)) {
            return(tags$div("Keyfiles when created will be placed here"))
          }

          KeyNames <- purrr$map_chr(kp$KeyPairs, ~ .$KeyName)

          purrr$map(
            KeyNames, function(x) {
              if (x == "ndexrkey") {
                return(tags$div())
              }
              box::use(uuid)
              uuid <- uuid$UUIDgenerate()
              elemId <- paste0(uuid, x, collapse = "")
              uiout <- manage_key_file$ui_key_file_single(ns(elemId))
              manage_key_file$server_key_file_single(elemId, x, ns_common_store_user)
              uiout
            }
          )
        })
      })
    }
  )
}

#' @export
ui_key_file_single <- function(id = "key_file_single") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_key_file_single <- function(id = "key_file_single", KeyName, ns_common_store_user) {
  box::use(shiny = shiny[tags])

  box::use(.. / client)
  box::use(.. / .. / connections / redis)
  box::use(.. / .. / inputs / inputs)
  box::use(.. / secrets / secrets)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      redux <- redis$get_state(ns_common_store_user("ec2_settings"))
      ec2boto <- client$client("ec2", ns_common_store_user = ns_common_store_user)


      output$ui <- shiny$renderUI({
        shiny$div(
          class = "",
          tags$hr(),
          tags$div(
            class = "row mb-2",
            tags$div(
              class = "col-md-6",
              tags$h4(KeyName)
            ),
            tags$div(
              class = "col-md-6 d-flex justify-content-end",
              shiny$actionButton(ns("deleteKeyPair"), tags$i(
                class = "bi fa-lg bi-x"
              ), class = "btn-warning fa fa-lg px-5 py-2 m-2"),
              shiny$downloadButton(ns("retrieveKeyPair"), tags$i(
                class = "bi fa-lg bi-arrow-down"
              ), class = "btn-primary fa fa-lg px-5 py-2 m-2")
            )
          )
        )
      })


      shiny$observeEvent(input$deleteKeyPair, {
        shiny$showModal(
          inputs$card_collapse(
            ns, 'Are you sure?',
            tags$div(
              class='container',
              tags$h5('This is a destructive action. Make sure you store your keyfile before deleting to access your server using ssh, because this deletes the keyfile on your end and mine.'),
              shiny$actionButton(ns('actuallyDelete'), 'Delete',class='btn-warning')
            )
          )
        )
      })

      shiny$observeEvent(
        input$actuallyDelete, {
          redux <- redis$get_state(ns_common_store_user("ec2_settings"))
          ec2boto <- client$client("ec2", ns_common_store_user = ns_common_store_user)

          tryCatch(
            {
              box::use(glue)
              keyClient <- client$client("ec2", ns_common_store_user = ns_common_store_user)
              secrets$secret_delete(ns_common_store_user(KeyName))
              shiny$showNotification(glue$glue("{KeyName} deleted my side"))
              keyClient$delete_key_pair(KeyName = KeyName)
              shiny$showNotification(glue$glue("{KeyName} deleted your side"))
              shiny$showNotification(glue$glue("{KeyName} deleted"))
              shiny::removeUI(selector = paste0("#", id))
            },
            error = function(err) {
              shiny$showNotification(as.character(err))
            }
          )
        }
      )

      output$retrieveKeyPair <- shiny$downloadHandler(
        filename = function() {
          paste(KeyName, "pem", sep = ".")
        },
        content = function(fname) {
          #
          kf <- secrets$secret_get(ns_common_store_user(KeyName))
          box::use(readr)
          readr$write_file(kf, fname)
        },
        contentType = "application/x-pem-file"
      )
    }
  )
}
