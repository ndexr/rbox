#' @export
#' @export
ui_aws_security_group_manager <- function(id = "aws_security_group_manager") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_aws_security_group_manager <- function(id = "aws_security_group_manager") {
  box::use(shiny = shiny[tags])
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        tags$div(
          class = "",
          tags$form(
            class = "mb-4",
            tags$div(
              class = "row mb-3",
              tags$label(
                class = "col-sm-2 col-form-label",
                "Allow IP Address"
              ),
              tags$div(
                class = "col-sm-10",
                tags$input(
                  id = ns("ipAllow"),
                  type = "text",
                  class = "form-control",
                  placeholder = "Enter IP Address"
                )
              ),
              tags$label(
                class = "col-sm-2 col-form-label",
                "At IP Address"
              ),
              tags$div(
                class = "col-sm-10",
                tags$input(
                  id = ns("ipEnter"),
                  type = "text",
                  class = "form-control",
                  placeholder = "Enter At IP Address"
                )
              )
            ),
            tags$div(
              class = "row mb-3",
              tags$label(
                class = "col-sm-2 col-form-label",
                "On Port"
              ),
              tags$div(
                class = "col-sm-6",
                tags$input(
                  id = ns("onPort"),
                  type = "text",
                  class = "form-control",
                  placeholder = "Enter Port(s)"
                )
              ),
              tags$div(
                class = "col-sm-3",
                tags$button(
                  id = ns("submitRules"),
                  type = "submit",
                  class = "btn btn-primary",
                  "Add Rule"
                )
              )
            )
          ),
          tags$table(
            class = "",
            tags$div(
              class = "row",
              tags$div(
                class = "col-md-3",
                "Allow IP Address"
              ),
              tags$div(
                class = "col-md-3",
                "At IP Address"
              ),
              tags$div(
                class = "col-md-3",
                "On Port"
              ),
              tags$div(
                class = "col-md-3",
                "Actions"
              )
            ),
            shiny$uiOutput(ns("portModuleA"), container = function(...) tags$div(class = "", ...))
          )
        )
      })

      output$portModuleA <- shiny$renderUI({
        tags$div(
          class = "row",
          tags$div(
            class = "col-md-3",
            "asdfasdfasdf"
          ),
          tags$div(
            class = "col-md-3",
            "asdfasdfasdf"
          ),
          tags$div(
            class = "col-md-3",
            "asdfasdfasdf"
          ),
          tags$div(
            class = "col-md-3",
            "asdfasdfasdf"
          )
        )
      })
    }
  )
}


#' @export
ui_server_aws_security_group_manager <- function(id = "server_aws_security_group_manager") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_server_aws_security_group_manager <- function(id = "server_aws_security_group_manager") {
  box::use(shiny = shiny[tags])
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        shiny$div("server_aws_security_group_manager")
      })
    }
  )
}
