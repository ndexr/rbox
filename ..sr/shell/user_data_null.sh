#!/bin/bash

# Install docker
LOGFILE='/var/log/ndexr/build.log'
savelog "$LOGFILE"
exec &> >(tee "$LOGFILE")
echo "alias tlog=\"tail -f /var/log/ndexr/build.log\"" >> /home/ubuntu/.bashrc

echo 'No startup script requested.'
