#' @export
ui_rbox_init <- function(id = "rbox_init") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / .. / inputs / inputs)
  }

  ns <- shiny$NS(id)

  tags$div(
    class = "row",
    tags$div(
      class = "col-12 d-flex justify-content-end my-2",
      shiny$actionButton(ns("refresh"), "Refresh", class = "btn-primary")
    ),
    tags$div(
      class = "col-12",
      shiny$uiOutput(ns("mainUI"))
    )
  )
}

#' @export
server_rbox_init <- function(id = "rbox_init", ns_common_store_user, credentials) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(. / security_groups)
    box::use(. / instance_manage)
    box::use(readr = readr[read_file])
    box::use(uuid)
    box::use(stringr)
    box::use(. / security_groups)
    box::use(stats[setNames])
    box::use(. / instance_types_get)
    box::use(. / rbox_init)
    box::use(.. / .. / inputs / inputs)
    box::use(.. / .. / connections / postgres)
    box::use(shiny = shiny[tags, HTML])
    box::use(. / rbox_init)
    box::use(.. / .. / inputs / inputs)
    box::use(.. / .. / connections / redis)
    box::use(.. / ec2 / security_groups[list_security_groups])
    box::use(.. / ec2 / instance_types_get)
    box::use(.. / client)
    box::use(purrr)
    box::use(dplyr)
    box::use(stats)
  }



  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      output$mainUI <- shiny$renderUI({
        input$refresh
        ec2boto <- client$client("ec2", ns_common_store_user = ns_common_store_user)
        kp <- tryCatch(expr = ec2boto$describe_key_pairs(), error = function(err) {
          # shiny$showModal(
          shiny$showNotification("Please create a key file")
          # )
          shiny$req(FALSE)
        })
        KeyNames <- purrr$map_chr(kp$KeyPairs, ~ .$KeyName)

        GroupNames <- tryCatch(expr = {
          list_security_groups(
            ns_common_store_user = ns_common_store_user
          )
        }, error = function(err) {
          shiny$showNotification("Please create a security group")
        })

        InstanceTypes <- instance_types_get$instance_types_get()
        store <- redis$get_state(ns_common_store_user("rbox_init"))

        ImageId <- stats$setNames(c("ami-05eb78ea90ca801dc", "ami-097a2df4ac947655f"), c("ndexr R box", "Ubuntu 22.04: jammy"))

        amis_table <-
          postgres$tables_row_retrieve("email", credentials$email, "amis")

        if (nrow(amis_table)) {
          amis <- amis_table$imageid
          names(amis) <- amis_table$Name
          ImageIds <- c(ImageId, amis)
        } else {
          ImageIds <- ImageId
        }

        KeyName <- redis$setDefault(store$KeyName, KeyNames[[1]])
        GroupName <- redis$setDefault(store$GroupName, GroupNames[[1]])
        machineBuild <- redis$setDefault(store$machineBuild, "bare")

        InstanceType <- redis$setDefault(store$InstanceType, "t3.xlarge")
        InstanceStorage <- redis$setDefault(store$InstanceStorage, 80)

        tags$div(
          tags$form(
            class = "row justify-content-center",
            tags$div(
              class = "col-8",
              inputs$selectizeInput(ns("KeyName"), "Key Name", KeyNames, KeyName, multiple = FALSE)
            ),
            tags$div(
              class = "col-8",
              inputs$selectizeInput(
                ns("GroupName"), "Security Group",
                GroupNames, GroupName,
                multiple = FALSE
              )
            ),
            tags$div(
              class = "col-8",
              inputs$selectizeInput(
                ns("ImageId"), "Amazon Machine Image (AMI)",
                ImageIds,
                ImageId,
                multiple = FALSE,
                options = list(create = TRUE)
              )
            ),
            tags$div(
              class = "col-8",
              inputs$selectizeInput(
                ns("machineBuild"), "Build Type",
                c("general", "bare", "docker"),
                machineBuild,
                multiple = FALSE
              )
            ),
            tags$div(
              class = "col-8",
              inputs$numericInput(
                ns("InstanceStorage"), "Instance Storage",
                min = 50, max = 300, value = 150, step = 10
              )
            ),
            tags$div(
              class = "col-8",
              inputs$selectizeInput(
                ns("InstanceType"), "Instance Type",
                list(
                  "General Purpose" = c(
                    "t2.nano: 1 vCPU, 512 MB RAM" = "t2.nano",
                    "t2.micro: 1 vCPU, 1 GB RAM" = "t2.micro",
                    "t2.small: 1 vCPU, 2 GB RAM" = "t2.small",
                    "t2.medium: 2 vCPU, 4 GB RAM" = "t2.medium",
                    "t2.large: 2 vCPU, 8 GB RAM" = "t2.large",
                    "t2.xlarge: 4 vCPU, 16 GB RAM" = "t2.xlarge",
                    "t2.2xlarge: 8 vCPU, 32 GB RAM" = "t2.2xlarge"
                  ),
                  "Compute" = c(
                    "c6a.medium: 1 vCPU, 2 GB RAM" = "c6a.medium",
                    "c6a.large: 2 vCPU, 4 GB RAM" = "c6a.large",
                    "c6a.xlarge: 4 vCPU, 8 GB RAM" = "c6a.xlarge",
                    "c6a.2xlarge: 8 vCPU, 16 GB RAM" = "c6a.2xlarge",
                    "c6a.4xlarge: 16 vCPU, 32 GB RAM" = "c6a.4xlarge",
                    "c6a.8xlarge: 32 vCPU, 64 GB RAM" = "c6a.8xlarge",
                    "c6a.12xlarge: 48 vCPU, 96 GB RAM" = "c6a.12xlarge",
                    "c6a.16xlarge: 64 vCPU, 128 GB RAM" = "c6a.16xlarge"
                  )
                ),
                InstanceType,
                multiple = FALSE
              )
            )
          ),
          tags$div(
            class = "col-12 d-flex justify-content-end align-items-center",
            shiny$actionButton(ns("createServer"), tags$h5("Start Server"), class = "btn-primary mb-2")
          )
        )
      })



      shiny$observeEvent(input$createServer, {
        box::use(dplyr)

        GroupName <- input$GroupName
        KeyName <- input$KeyName
        ImageId <- input$ImageId
        InstanceType <- input$InstanceType
        InstanceStorage <- input$InstanceStorage

        print(jsonlite::toJSON(shiny::reactiveValuesToList(input)))

        ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
        security_groups <- ec2$describe_security_groups()
        GroupId <- purrr::keep(security_groups$SecurityGroups, function(x) x$GroupName == GroupName)[[1]]$GroupId

        tryCatch(
          {
            fileUserData <- switch(input$machineBuild,
              "general" = "./shell/user_data.sh",
              "bare" = "./shell/user_data_null.sh",
              "docker" = "./shell/user_data_docker.sh",
            )
            user_data <- read_file(fileUserData)

            instance_data <- instance_manage$create_instance(
              ImageId = ImageId,
              InstanceType = InstanceType,
              InstanceStorage = as.integer(InstanceStorage),
              user_data = user_data,
              GroupId = GroupId,
              KeyName = KeyName,
              ns_common_store_user = ns_common_store_user
            )

            instance_state <- postgres$instance_state(
              ImageId = ImageId,
              InstanceType = InstanceType,
              InstanceStorage = as.integer(InstanceStorage),
              user_data = user_data,
              GroupId = GroupId,
              KeyName = KeyName,
              InstanceId = instance_data$InstanceId,
              status = "starting"
              # ns_common_store_user = ns_common_store_user('user')
            )

            shiny$showNotification("Server is starting")

            instance_data
          },
          error = function(err) {
            shiny$showModal(
              inputs$modalDialog(
                size = "xl",
                tags$div(
                  tags$h4("Oops, something went wrong. If you have not opened the pre-launch preferences panel during this session, please do so first before proceeding."),
                  shiny$tags$pre(shiny$tags$code(as.character(err)))
                )
              )
            )
            shiny$req(FALSE)
          }
        )
      })


      shiny$observeEvent(c(
        input$KeyName,
        input$ImageId,
        input$InstanceType,
        input$InstanceStorage,
        input$machineBuild
      ), {
        input <- shiny$reactiveValuesToList(input)
        redis$store_state(ns_common_store_user("rbox_init"), input)
      })


      shiny$observeEvent(input$saveFile, {
        redis$store_state(ns_common_store_user("userData"), shiny$reactiveValuesToList(input))
      })
    }
  )
}
