#' @export
ui_user_create <- function(id = "user_create") {
  box::use(shiny = shiny[tags, HTML])

  if (isFALSE(getOption("require_login"))) {
    return(tags$div("Development mode"))
  }


  box::use(shinyjs)
  ns <- shiny$NS(id)
  tags$div(
    shiny$uiOutput(ns("ui")),
    tags$div(
      class = " my-4 border-1 rounded",
      tags$div(
        class = "row",
        tags$div(
          class = "d-flex justify-content-center col-md-6 offset-md-3",
          shiny$uiOutput(ns("passwordManagement"))
        )
      )
    )
  )
}

#' @export
server_user_create <- function(id = "user_create", with_cookie = FALSE, cookiename = "cognitor", cookie_expire = 7) {
  {
    box::use(.. / users / console_users)
    box::use(utils)
    box::use(.. / inputs / inputs)
    box::use(shiny.router)
    box::use(.. / users / users)
    box::use(shiny = shiny[tags, HTML])
  }

  if (isFALSE(getOption("require_login"))) {
    return()
  }


  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        tags$div(
          id = ns(id),
          class = "row",
          tags$div(class = "col-xl-4 col-lg-3 col-sm-3"),
          tags$div(
            class = "col-xl-4 col-lg-6 col-sm-6 well p-4",
            tags$h4("Create an Account", class = "text-center"),
            tags$form(
              inputs$textInput(ns("name"), label = "Name"),
              inputs$textInput(ns("email"), label = "email"),
              inputs$textInput(ns("phone"), label = "phone"),
              inputs$textInput(ns("company"), label = "company"),
              tags$em("When you hit submit, you will be emailed your access code."),
              tags$div(
                class = "row",
                tags$div(
                  class = "col d-flex justify-content-between",
                  shiny$actionButton(ns("login"), "Go to Login", class = "btn btn-secondary btn-sm"),
                  tags$button(
                    id = ns("submit"),
                    type = "submit",
                    class = "btn btn-primary action-button",
                    "Submit"
                  )
                )
              )
            )
          )
        )
      })

      shiny$observeEvent(
        input$submit,
        {
          #
          output$passwordManagement <- shiny$renderUI({
            console_users$ui_user_password_management(ns("user_password_management"), input$email, login = TRUE, immediate = TRUE)
          })

          console_users$server_user_password_management(
            id = "user_password_management", input$email, NULL, admin = FALSE, login = FALSE, immediate = TRUE
          )
        }
      )

      shiny$observeEvent(input$submit, {
        #
        users$waitlist_table(input$name, input$email, input$phone, input$company)
      })

      shiny$observeEvent(input$login, {
        # shiny.router$change_page("user_login")
      })
    }
  )
}
