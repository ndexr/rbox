#' @export
server <- function(input, output, session) {
  switch(getOption("ndexr_site"),
    "ndexrconsole" = {
      box::use(. / r / ndexrconsole)
      ndexrconsole$server_ndexrconsole("ndexrconsole")
    },
    "ndexrio" = {
      box::use(. / r / ndexrio)
      ndexrio$server_ndexrio("ndexrio")
    }
  )
}
