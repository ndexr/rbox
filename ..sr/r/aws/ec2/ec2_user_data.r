#' #' @export
#' ui_user_data <- function(id = "user_data") {
#'   box::use(shiny = shiny[tags, HTML])
#'   box::use()
#'   ns <- shiny$NS(id)
#'   shiny$uiOutput(ns("ui"))
#' }
#'
#' #' @export
#' server_user_data <- function(id = "user_data", ns_common_store_user) {
#'   {
#'     box::use(shiny = shiny[tags, HTML])
#'     box::use(shinyAce)
#'     box::use(readr = readr[read_file])
#'     box::use(.. / .. / connections / redis)
#'   }
#'
#'   shiny$moduleServer(
#'     id,
#'     function(input, output, session) {
#'       ns <- session$ns
#'
#'
#'     }
#'   )
#' }
