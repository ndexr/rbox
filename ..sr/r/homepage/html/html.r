#' @export
ui_html <- function(...) {
  box::use(shiny[tags])
  tags$html(lang = "en", ...)
}

#' @export
ui_body <- function(...) {
  box::use(shiny[tags])
  tags$body(id = "page-top", ...)
}
