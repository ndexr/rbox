#' @export
usaa_data <- function(path = "usaa.csv", filter) {
  box::use(readr)
  box::use(dplyr)
  box::use(stringr)
  usaa <- readr$read_csv(path)
  out <-
    usaa |>
    dplyr$group_by(Description) |>
    dplyr$summarise(
      amount = sum(round(Amount)),
      n_transactions = dplyr$n()
    ) |>
    dplyr$arrange(amount)

  out <- out[grepl(tolower(filter), tolower(out$Description)), ]

  out
}


#' @export
ui_amounts <- function(id = "amounts") {
  box::use(shiny = shiny[tags])
  box::use(.. / inputs / inputs)

  ns <- shiny$NS(id)

  inputs$card_collapse(
    ns,
    title = "USAA",
    tags$div(
      class = "scrollmenu",
      shiny$uiOutput(ns("navbar")),
      shiny$uiOutput(ns("ui"))
    )
  )
}

#' @export
server_amounts <- function(id = "amounts") {
  box::use(shiny = shiny[tags])
  box::use(. / import)
  # box::reload(import)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      output$navbar <- shiny$renderUI({
        # usaa_data <- import$usaa_data()

        tags$div(
          class = "row",
          tags$div(
            class = "col-12 d-flex justify-content-between",
            shiny$textInput(ns("search"), "Search"),
            shiny$actionButton(ns("go"), "Submit")
          )
        )
        # unique(usaa_data$Description)
      })

      shiny$observeEvent(
        input$go,
        {
          output$ui <- shiny$renderUI({
            usaa_data <- import$usaa_data(filter = input$search)
            tags$table(
              class = "table table-striped",
              style = "overflow-x: auto; overflow-y: auto;",
              tags$thead(
                tags$tr(
                  tags$th(
                    scope = "col",
                    "Description"
                  ),
                  tags$th(
                    scope = "col",
                    "Data"
                  )
                )
              ),
              tags$tbody(
                purrr::map(
                  split(usaa_data, 1:nrow(usaa_data)), function(x) {
                    box::use(. / import)
                    uuid <- uuid::UUIDgenerate()
                    out <- import$ui_transaction(ns(uuid))
                    import$server_transaction(uuid, x)
                    out
                  }
                )
              )
            )
          })
        }
      )
    }
  )
}

#' @export
ui_transaction <- function(id = "transaction") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"), container = function(...) tags$tr(...))
}

#' @export
server_transaction <- function(id = "transaction", x) {
  box::use(shiny = shiny[tags])
  box::use(.. / connections / redis)
  box::use(.. / connections / postgres)
  box::use(glue)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      id <- paste0(c("usaa-transactions-", x$Description), collapse = "")
      ns <- session$ns

      output$ui <- shiny$renderUI({
        state <- redis$get_state(id = id)
        state$email <- redis$setDefault(state$email, "drennanfreddy@gmail.com")
        state$password <- redis$setDefault(state$password, "")
        state$person <- redis$setDefault(state$person, "")
        state$recurring <- redis$setDefault(state$recurring, FALSE)
        state$label <- redis$setDefault(state$label, FALSE)
        # print(state)

        shiny$tagList(
          tags$td(
            tags$h5(x$Description),
            tags$em(glue$glue("N Transactions: {x$n_transactions}")),
            tags$p(glue$glue("Total: {scales::dollar(x$amount)}")),
            tags$p(glue$glue("Average: {scales::dollar(round(x$amount / x$n_transactions, 2))}"))
          ),
          tags$td(
            shiny$textInput(ns("email"), "Email", input$email),
            shiny$textInput(ns("password"), "password", state$password),
            shiny$selectizeInput(ns("person"), "Person",
              choices = c("", "Freddy", "Meggan", "Kids", "Both"),
              selected = state$person
            ),
            shiny$checkboxInput(ns("recurring"), "Recurring", state$recurring),
            tags$div(
              class = "row",
              tags$div(
                class = "col-12",
                shiny$actionButton(ns("update"), "Update", class = "btn-primary"),
                shiny$actionButton(ns("close"), "Close", class = "btn-warning")
              )
            )
          )
        )
      })

      shiny$observeEvent(
        input$update,
        {
          input <- shiny$reactiveValuesToList(input)
          redis$store_state(id = id, data = input)
          usaa_transaction_data <- data.frame(
            description = x$Description,
            amount = x$amount,
            n_transactions = x$n_transactions,
            email = input$email,
            password = input$password,
            person = input$person,
            recurring = input$recurring
          )
          postgres$table_create_or_upsert(usaa_transaction_data, where_cols = "description")
        }
      )

      shiny$observeEvent(input$close, {
        shiny::removeUI(selector = paste0(c("#", ns("ui")), collapse = T))
      })
    }
  )
}
#


if (FALSE) {
  ui <- function() {
    box::use(shiny = shiny[tags])
    ui_amounts()
  }
  server <- function(input, output, session) {
    box::use(shiny = shiny[tags])
    server_amounts()
  }

  box::use(shiny)
  shiny$shinyApp(ui, server)
}
