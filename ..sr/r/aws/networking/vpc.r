#' @export
vpc_create <- function(ns_common_store_user) {
  box::use(.. / client)
  box::use(purrr)
  box::use(.. / .. / aws / ec2 / security_groups)

  # ns_common_store_user <- shiny::NS('fdrennan')
  # AWS_REGION = "us-east-2"
  vpc_client <- client$client("ec2", ns_common_store_user)
  try(vpc_client$create_default_vpc())
  vpcs <- purrr$map_dfr(
    vpc_client$describe_vpcs()$Vpcs,
    function(x) {
      data.frame(
        CidrBlock = x$CidrBlock,
        DhcpOptionsId = x$DhcpOptionsId,
        IsDefault = x$IsDefault,
        State = x$State,
        VpcId = x$VpcId
      )
    }
  )
  sgs <- security_groups$list_security_groups(ns_common_store_user, TRUE)
  sqs_resource <- client$resource("ec2", ns_common_store_user)
}
