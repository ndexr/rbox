#' @export
ui_about <- function() {
  box::use(shiny[withTags])
  withTags(
    section(
      class = "page-section", id = "about",
      div(
        class = "",
        div(
          class = "text-center",
          h2(class = "section-heading text-uppercase", "About"),
          h3(class = "section-subheading text-muted", "Lorem ipsum dolor sit amet consectetur.")
        ),
        ul(
          class = "timeline",
          li(
            div(class = "timeline-image", img(class = "rounded-circle img-fluid", src = "homepage/img/about/1.jpg", alt = "...")),
            div(
              class = "timeline-panel",
              div(
                class = "timeline-heading",
                h4("2009-2011"),
                h4(class = "subheading", "Our Humble Beginnings")
              ),
              div(class = "timeline-body", p(class = "text-muted", "Lorem ipsum dolor sit amet, consectetur adipisicing elit!"))
            )
          ),
          li(
            class = "timeline-inverted",
            div(class = "timeline-image", img(class = "rounded-circle img-fluid", src = "homepage/img/about/2.jpg", alt = "...")),
            div(
              class = "timeline-panel",
              div(
                class = "timeline-heading",
                h4("March 2011"),
                h4(class = "subheading", "An Agency is Born")
              ),
              div(class = "timeline-body", p(class = "text-muted", "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!"))
            )
          ),
          li(
            div(class = "timeline-image", img(class = "rounded-circle img-fluid", src = "homepage/img/about/3.jpg", alt = "...")),
            div(
              class = "timeline-panel",
              div(
                class = "timeline-heading",
                h4("December 2015"),
                h4(class = "subheading", "Transition to Full Service")
              ),
              div(class = "timeline-body", p(class = "text-muted", "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!"))
            )
          ),
          li(
            class = "timeline-inverted",
            div(class = "timeline-image", img(class = "rounded-circle img-fluid", src = "homepage/img/about/4.jpg", alt = "...")),
            div(
              class = "timeline-panel",
              div(
                class = "timeline-heading",
                h4("July 2020"),
                h4(class = "subheading", "Phase Two Expansion")
              ),
              div(class = "timeline-body", p(class = "text-muted", "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!"))
            )
          ),
          li(
            class = "timeline-inverted",
            div(
              class = "timeline-image",
              h4(
                "Be Part",
                br(),
                "Of Our",
                br(),
                "Story!"
              )
            )
          )
        )
      )
    )
  )
}
