#' @export
modal_error <- function(err) {
  box::use(.. / inputs / inputs)
  box::use(shiny)
  shiny$showModal(
    inputs$modalDialog(
      # size = "xl",
      shiny$tags$pre(
        shiny$tags$code(
          as.character(err)
        )
      )
    )
  )
}
