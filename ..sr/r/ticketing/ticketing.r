#' @export
ui_ticketing <- function(id = "ticketing", admin = FALSE) {
  box::use(shiny = shiny[tags])
  box::use(.. / inputs / inputs)

  ns <- shiny$NS(id)

  # if (admin) {
  #   inputs$card_collapse(
  #     ns,
  #     title = "Open Requests",
  #     body = shiny$uiOutput(ns("existingTickets"))
  #   )
  # } else {
  #   inputs$card_collapse(
  #     ns,
  #     title = "Create a Ticket",
  #     body = shiny$uiOutput(ns("makeTicket"))
  #   )
  # }

  tags$div()
}

#' @export
server_ticketing <- function(
    id = "ticketing") {
  box::use(shiny = shiny[tags])
  box::use(.. / connections / postgres)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      output$makeTicket <- shiny$renderUI({
        tags$main(
          class = "",
          # tags$h4(group, class = "display-4 text- center p-3"),
          tags$form(
            tags$div(
              class = "mb-3",
              tags$label(
                `for` = ns("ticketTitle"),
                class = "form-label",
                "Title"
              ),
              tags$input(
                type = "text",
                class = "form-control shiny-bound-input",
                id = ns("ticketTitle"),
                required = ""
              )
            ),
            tags$div(
              class = "mb-3",
              tags$label(
                `for` = ns("ticketContent"),
                class = "form-label",
                "Description"
              ),
              tags$textarea(
                class = "form-control shiny-bound-input",
                id = ns("ticketContent"),
                rows = "3",
                required = ""
              )
            ),
            tags$div(
              class = "mb-3",
              shiny::dateInput(ns("estimatedCompletion"), label = "Estimated Time in Days")
            ),
            tags$div(
              class = "mb-3",
              tags$label(
                `for` = ns("ticketPriority"),
                class = "form-label",
                "Priority"
              ),
              tags$select(
                class = "form-select shiny-bound-input",
                id = ns("ticketPriority"),
                required = "",
                tags$option(
                  value = "",
                  "-- Select Priority --"
                ),
                tags$option(
                  value = "high",
                  "High"
                ),
                tags$option(
                  value = "medium",
                  "Medium"
                ),
                tags$option(
                  value = "low",
                  "Low"
                )
              )
            ),
            tags$button(
              id = ns("createTicket"),
              type = "submit",
              class = "btn btn-primary action-button",
              "Create Ticket"
            )
          )
        )
      })

      shiny$observeEvent(input$createTicket, {
        id <- uuid::UUIDgenerate()


        shiny$req(
          all(
            !is.null(input$ticketTitle),
            !is.null(input$ticketContent),
            !is.null(input$ticketPriority),
            !is.null(input$estimatedCompletion)
          )
        )
        tickets <- data.frame(
          id = id,
          title = input$ticketTitle,
          content = input$ticketContent,
          priority = input$ticketPriority,
          createdAt = Sys.time(),
          estimatedCompletion = input$estimatedCompletion,
          status = "Open"
        )

        postgres$table_create_or_upsert(
          tickets,
          where_cols = "id"
        )

        shiny$showNotification("Ticket Created")
      })


      output$existingTickets <- shiny$renderUI({
        tags$section(
          shiny$actionButton(ns("refreshTickets"), tags$i(
            class = "bi fa-lg bi-arrow-down"
          ), class = "btn-info mb-2"),
          shiny$uiOutput(ns("ticketsList"))
        )
      })

      output$ticketsList <- shiny$renderUI({
        box::use(. / ticketing)
        box::use(purrr)
        input$refreshTickets
        tickets <- postgres$table_get("tickets")
        tickets <- split(tickets, tickets$id)

        ticketsUI <- purrr$map(
          tickets, function(x) {
            comp_id <- uuid::UUIDgenerate()
            out <- tags$div(
              class = "row",
              ticketing$ui_ticket(
                ns(comp_id),
                title = x$title
              )
            )
            ticketing$server_ticket(
              comp_id,
              x
            )
            out
          }
        )

        ticketsUI
      })
    }
  )
}


#' @export
ui_ticket <- function(id = "ticket",
                      title = "Ticket") {
  box::use(shiny = shiny[tags])
  box::use(.. / inputs / inputs)
  ns <- shiny$NS(id)
  inputs$card_collapse(
    ns,
    title = title, body = {
      tags$li(
        class = "list-group-item",
        shiny$uiOutput(ns("ui")),
        shiny$uiOutput(ns("makeTicket"))
      )
    }
  )
}

#' @export
server_ticket <- function(id = "ticket",
                          ticketdata) {
  box::use(shiny = shiny[tags])
  box::use(glue)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      output$ui <- shiny$renderUI({
        box::use(lubridate)

        classname <- switch(ticketdata$status,
          "Open" = "badge bg-primary rounded-pill",
          "Closed" = "badge bg-success rounded-pill",
          "In Progress" = "badge bg-warning rounded-pill"
        )


        # Set the two dates
        estimatedCompletion <- lubridate$ymd(ticketdata$estimatedCompletion)
        createdAt <- lubridate$ymd(
          lubridate$as_date(ticketdata$createdAt)
        )

        # Get the difference in days
        diff_days <- lubridate$as.duration(estimatedCompletion - createdAt) / lubridate$ddays(1)


        tags$div(
          tags$div(
            class = "",
            tags$div(
              class = "d-flex justify-content-between",
              tags$span(
                class = classname,
                ticketdata$priority
              ),
              tags$em(paste0("created: ", lubridate$as_date(createdAt)), class = "ms-2"),
              tags$em(paste0("completion: ", lubridate$as_date(estimatedCompletion)), class = "ms-2"),
              tags$em(glue$glue("days remaining: {diff_days}"))
            ),
            tags$div(
              class = "container mb-3",
              tags$p(ticketdata$content)
            )
          )
        )
      })

      output$makeTicket <- shiny$renderUI({
        tags$main(
          class = "",
          tags$form(
            # tags$div(
            #   class = "mb-3",
            #   shiny::dateInput(ns("estimatedCompletion"),
            #     label = "Estimated Time in Days",
            #     value = ticketdata$estimatedCompletion
            #   )
            # ),
            tags$div(
              class = "mb-3",
              tags$label(
                `for` = ns("ticketPriority"),
                class = "form-label",
                "Priority"
              ),
              tags$select(
                class = "form-select shiny-bound-input",
                id = ns("ticketPriority"),
                required = "",
                tags$option(
                  value = ticketdata$priority,
                  ticketdata$priority
                ),
                tags$option(
                  value = "high",
                  "High"
                ),
                tags$option(
                  value = "medium",
                  "Medium"
                ),
                tags$option(
                  value = "low",
                  "Low"
                )
              )
            ),
            tags$button(
              id = ns("createTicket"),
              type = "submit",
              class = "btn btn-primary action-button",
              "Update Ticket"
            ),
            shiny$actionButton(ns("completed"), "Delete", class = "btn-warning")
          )
        )
      })

      shiny$observeEvent(input$createTicket, {
        id <- uuid::UUIDgenerate()



        # ticketdata$content = input$ticketContent
        ticketdata$priority <- input$ticketPriority
        ticketdata$estimatedCompletion <- Sys.Date() + 7
        tickets <- ticketdata

        box::use(.. / connections / postgres)
        postgres$table_create_or_upsert(
          tickets,
          where_cols = "id"
        )

        shiny$showNotification("Ticket Updated")
      })


      shiny$observeEvent(
        input$completed,
        {
          box::use(.. / connections / postgres)
          postgres$tables_row_remove("id", ticketdata$id, "tickets")
          shiny$removeUI(selector = paste0("#", ns(id)))
        }
      )
    }
  )
}
