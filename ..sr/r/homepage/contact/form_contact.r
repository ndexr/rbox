#' @export
ui_form_contact <- function(id = "form_contact") {
  {
    box::use(shiny = shiny[withTags])
  }
  ns <- shiny$NS(id)
  withTags(
    form(
      id = "contactForm",
      div(
        class = "row align-items-stretch mb-5",
        div(
          class = "col-md-6",
          div(
            class = "form-group",
            input(class = "form-control", id = ns("name"), type = "text", placeholder = "Your Name *")
          ),
          div(
            class = "form-group",
            input(class = "form-control", id = ns("email"), type = "email", placeholder = "Your Email *")
          ),
          div(
            class = "form-group mb-md-0",
            input(class = "form-control", id = ns("phone"), type = "tel", placeholder = "Your Phone *")
          )
        ),
        div(
          class = "col-md-6",
          div(
            class = "form-group form-group-textarea mb-md-0",
            textarea(class = "form-control", id = ns("message"), placeholder = "Your Message *")
          )
        )
      ),
      div(
        class = "text-center", # disabled
        button(class = "btn  btn-xl text-uppercase action-button", id = ns("submitButton"), type = "submit", "Send Message")
      )
    )
  )
}


#' @export
server_form_contact <- function(id = "form_contact") {
  {
    box::use(shiny)
    box::use(jsonlite)
    box::use(shinyvalidate)
    box::use(.. / email / send_email)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      shiny$observeEvent(input$submitButton, {
        data <- shiny$reactiveValuesToList(input)
        send_email$send_email("frontpage inquiry", data)
        shiny$showNotification("Your message has been received. Please allow 48 business hours for a reply.")
      })
    }
  )
}
