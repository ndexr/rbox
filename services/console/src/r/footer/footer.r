#' @export
ui_footer <- function(id) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / router / route_link[route_link])
    box::use(glue)
  }
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_footer <- function(id) {
  {
    box::use(glue)
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / router / route_link[route_link])
    box::use(.. / users / ip_address)
    box::use(.. / sidebar / site_sidebar)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      output$ui <- shiny$renderUI({
        tags$div(
          class = "navbar d-flex justify-content-center",
          tags$footer(
            class = "row p-3",
            tags$div(
              class = "col-12 d-flex flex-column justify-content-around align-items-center flex-sm-row text-center  py-4 my-4 border-top ",
              ip_address$ui_user_ip(ns("user_ip")),
              tags$ul(
                class = "list-unstyled d-flex justify-content-center align-items-center",
                tags$li(
                  class = "ms-3",
                  shiny$tags$a(class = "badge badge-info", href = "https://www.linkedin.com/in/freddydrennan/", target = "_blank", shiny$icon("linkedin", class = "fa p-2"))
                ),
                tags$li(
                  class = "ms-3",
                  shiny$tags$a(class = "badge badge-info", href = "https://github.com/fdrennan", target = "_blank", shiny$icon("github", class = "fa p-2"))
                ),
                tags$li(
                  class = "ms-3",
                  shiny$tags$a(class = "badge badge-info", href = "https://gitlab.com/fdrennan", target = "_blank", shiny$icon("gitlab", class = "fa p-2"))
                ),
                tags$li(
                  class = "ms-3",
                  shiny$tags$a(class = "badge badge-info", href = "https://gitlab.com/fdrennan/rapp/-/pipelines", target = "_blank", shiny$icon("heart", class = "fa p-2"))
                )
              )
            ),
            tags$div(
              class = "col-12 d-flex flex-column justify-content-around align-items-center flex-sm-row text-center  py-4 my-4 border-top ",
              tags$p(
                "© 2023 NDEXR. All rights reserved."
              )
            )
          )
        )
      })

      ip_address$server_user_ip("user_ip")
      site_sidebar$server_site_sidebar("site_sidebar")


      shiny$observeEvent(input$dockertoggle, {
        options(docker = !getOption("docker"))
        output$dockerstatus <- shiny$renderUI({
          tags$em(
            glue$glue('docker: {getOption("docker")}')
          )
        })
      })

      shiny$observeEvent(input$subscribe, {
        # box::use(.. / connections / postgres)
        # newsletter <- data.frame(
        #   email = input$newsletter,
        #   info = input$info
        # )
        #
        # postgres$table_append(newsletter)
        # shiny$showNotification("Thanks!")
        # box::use(.. / gmail / gmail)
        # gmail$send_email(
        #   from = "fdrennan@ndexr.com",
        #   to = "fdrennan@ndexr.com",
        #   subject = input$newsletter,
        #   body = input$info,
        #   key = NULL,
        #   data_list = NULL
        # )
      })
    }
  )
}
