#' @export
ui_sidebar <- function(id = "sidebar") {
  box::use(shiny = shiny[tags, icon])
  box::use(shinyjs)
  box::use(shiny.router[route_link])
  box::use(. / sidebar)
  ns <- shiny$NS(id)
  #
  out <- tags$div(
    tags$nav(
      tags$div(
        class = "d-flex justify-content-between align-items-center",
        tags$a(
          href = "javascript:history.go(0);window.location.href = 'https://ndexr.io';",
          class = "nav-link rounded",
          tags$img(
            src = "./images/ndexrlogo.png",
            alt = "Brand Logo",
            class = "img-fluid",
            style = "max-width: 50%;"
          )
        ),
        tags$a(
          href = "javascript:history.go(0);window.location.href = 'https://console.ndexr.io';",
          class = "hoverable nav-link   p-2 me-3",
          tags$i(
            class = "bi fa-3x bi-arrow-repeat"
          )
        ),
        shiny$includeScript("javascript/sidebar.js")
      )
    )
  )

  out
}

#' @export
server_sidebar <- function(id) {
  box::use(shiny)
  box::use(shinyjs = shinyjs[js])
  shiny$moduleServer(
    id,
    function(input, output, session) {

    }
  )
}
