#' @export
ui_email_send <- function(id = "email_send") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  tags$div(
    class = "panel panel-default",
    tags$div(
      class = "panel-body",
      tags$form(
        tags$div(
          class = "form-group",
          tags$label(
            `for` = ns("emailTo"),
            "To:"
          ),
          tags$input(
            type = "email",
            class = "form-control",
            id = ns("emailto"),
            placeholder = "Enter email address"
          )
        ),
        tags$div(
          class = "form-group",
          tags$label(
            `for` = ns("inputSubject"),
            "Subject:"
          ),
          tags$input(
            type = "text",
            class = "form-control",
            id = ns("inputSubject"),
            placeholder = "Enter subject"
          )
        ),
        tags$div(
          class = "form-group",
          tags$label(
            `for` = ns("body"),
            "Body:"
          ),
          tags$textarea(
            class = "form-control",
            id = ns("body"),
            rows = "5",
            placeholder = "Enter email content"
          )
        ),
        tags$button(
          id = ns("submit"),
          type = "submit",
          class = "btn btn-primary action-button",
          "Send Email"
        )
      )
    )
  )
}

#' @export
server_email_send <- function(id = "email_send") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(glue)
    box::use(.. / gmail / gmail)
    box::use(bcrypt)
    box::use(.. / connections / postgres)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      shiny$observeEvent(
        input$submit,
        {
          #
          gmail$send_email(
            from = "fdrennan@ndexr.com",
            to = input$emailto,
            subject = input$inputSubject,
            body = input$body,
            key = NULL,
            data_list = NULL,
            stripe_env = "STRIPE_LIVE_SK"
          )

          shiny$showNotification("Sent!")
        }
      )
    }
  )
}
