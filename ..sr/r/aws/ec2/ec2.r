#' @export
ui_ec2 <- function(id = "ec2") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(glue)
  }

  ns <- shiny$NS(id)

  shiny$uiOutput(ns("ec2Panel"), container = function(...) {
    tags$div(
      class = "row", ...
    )
  })
}


#' @export
server_ec2 <- function(id = "ec2", instance, InstanceTypes,
                       ns_common_store_user, credentials) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(glue)
    box::use(dplyr)
    box::use(.. / client)
    box::use(. / instance_manage)
    box::use(jsonlite)
    box::use(.. / .. / connections / redis)
    box::use(shinyjs)
    box::use(glue = glue[glue])
    box::use(ssh)
    box::use(fs)
    box::use(. / ec2)
    # box::use(. / modifystate)
    box::use(purrr)
    box::use(DT)
    box::use(. / ec2_backend)
    box::use(.. / .. / state / setDefault[setDefault])
    box::use(.. / .. / connections / postgres)
    box::use(postgres_ui = .. / .. / connections / postgres / postgres)
    box::use(.. / .. / connections / redis)
    box::use(. / workflows / logs)
    box::use(dplyr)
    box::use(.. / .. / inputs / inputs)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      ec2data <- shiny$reactive({
        input$refreshInstance

        box::use(curl[nslookup])

        InstanceId <- instance$InstanceId
        ec2data <- redis$get_state(ns_common_store_user(InstanceId))
        if (is.null(ec2data)) ec2data <- list()
        ec2data$nickname <- redis$setDefault(ec2data$nickname, "nickname")

        ec2con <- client$client("ec2", ns_common_store_user = ns_common_store_user)


        box::use(.. / ec2 / security_groups[list_security_groups])
        security_groups <- list_security_groups(ns_common_store_user = ns_common_store_user)
        if ("domain_https" %in% tryCatch(postgres$tables_list(), error = function(err) "")) {
          domain_https <- postgres$table_get("domain_https")
          domain_https <- dplyr$arrange(domain_https, dplyr$desc(time))
          domain_https <- dplyr$filter(domain_https, InstanceId %in% instance$InstanceId)
          if (nrow(domain_https) > 0) {
            current_domain <- domain_https$domain[[1]]
          } else {
            current_domain <- instance$InstanceId
          }
        } else {
          current_domain <- instance$InstanceId
        }
        shiny$showNotification(glue("Retrieving {current_domain}"))
        ec2data$username <- "ubuntu"
        ec2data$elastic_ips <- ec2_backend$ec2_list_elastic_ip(ns_common_store_user)
        ec2data$hosted_zones <- ec2_backend$route53_list_hosted_zones(ns_common_store_user)
        ec2data$instance <- instance
        ec2data$ec2users <- {
          if ("ec2users" %in% postgres$tables_list()) {
            ec2users <- postgres$table_get("ec2users")
            dplyr$filter(ec2users, InstanceId %in% instance$InstanceId)
          }
        }
        ec2data$current_domain <- current_domain
        ec2data$ns_common_store_user <- ns_common_store_user
        ec2data$isRunning <- instance$state == "running"
        ec2data$security_groups <- security_groups
        #
        redis$store_state(ns_common_store_user(ec2data$instance$InstanceId), ec2data)

        output$ec2Panel <- shiny$renderUI({
          inputs$card_collapse(
            collapsed = TRUE,
            ns, title = ec2data$current_domain,
            body = shiny$uiOutput(ns("instanceUI"), container = function(...) {
              tags$div(
                ...
              )
            })
          )
        })

        ec2data
      })


      output$instanceUI <- shiny$renderUI({
        shiny$req(ec2data())
        box::use(dplyr)
        ec2data <- ec2data()
        out <- logs$ui_logs(ns("logs"), ec2data, credentials)
        logs$server_logs("logs", ec2data, credentials)
        out
      })



      ec2data
    }
  )
}
