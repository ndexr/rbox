#' @export
app_router <- function(ns) {
  box::use(shiny.router)
  box::use(. / users / subscribe)

  shiny.router$make_router(
    shiny.router$route("", subscribe$ui_subscribe(ns("subscribe"), frontpage=TRUE))
    # shiny.router$route("debug", debug$ui_debug(ns("main")))
    # shiny.router$route("terminal", terminal$ui_terminal(ns("terminal")))
  )
}
