#' @export
ui_head <- function() {
  box::use(shiny = shiny[tags])
  box::use(shinyjs)
  tags$head(
    tags$meta(charset = "utf-8"),
    tags$meta(
      name = "viewport",
      content = "width=device-width, initial-scale=1"
    ),
    tags$title("ndexr"),
    tags$meta(name = "author", content = "Freddy Drennan"),
    tags$meta(name = "description", content = "r apps on aws"),
    tags$link(rel = "icon", type = "image/png", href = "./www/img/ndexrsym.png"),
    shinyjs$useShinyjs()
  )
}
