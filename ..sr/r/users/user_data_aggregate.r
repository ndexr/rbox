#' @export
user_data_aggregate <- function(raw = FALSE, live = getOption("docker", FALSE)) {
  box::use(.. / connections / postgres)
  box::use(dplyr)
  box::use(.. / aws / cognito / cognito)
  box::use(.. / payments / stripe)
  box::use(purrr)

  cognito_users <- cognito$cognito_users()
  customers <- stripe$stripe_customers(live)
  charges <- stripe$stripe_charges(live)

  stripe <- dplyr::full_join(dplyr::rename(customers, customer = customer_id), charges)
  if (raw) {
    return(
      list(
        cognito_users = cognito_users,
        stripe = stripe,
        charges = charges
      )
    )
  }

  out <- dplyr$full_join(cognito_users, stripe)
}
