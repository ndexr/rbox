#' @export
ui_send_command <- function(id = "send_command", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(ssh)
    box::use(jsonlite)
    box::use(tidyr)
    box::use(glue[glue])
    box::use(purrr)
    box::use(.. / .. / .. / connections / redis)
    box::use(.. / .. / .. / connections / ssh)
    box::use(.. / .. / .. / inputs / inputs)
    # box::use(.. / .. / .. / connections / s)
    # box::use(.. / .. / .. / state / setDefault[setDefault])
  }

  ns <- shiny$NS(id)


  system_info <- list(
    "uname", "uname -a", "uname -v", "uname -r", "uname -m", "sudo lshw", "sudo lshw -short",
    "lscpu", "lsblk", "lsblk -a", "sudo fdisk -l", "sudo dmidecode -t memory",
    "sudo dmidecode -t system", "sudo dmidecode -t processor"
  )

  docker_commands <- list(
    "docker system prune -a --force"
  )
  disk <- list("df -h")
  cat_commands <- list("cat /etc/nginx/nginx.conf", "cat /etc/nginx/sites-enabled/default")
  ssh_commands <- list("sudo cp ./shared/etc/ssh/sshd_config /etc/ssh/sshd_config", "sudo /etc/init.d/ssh restart")
  services <- tidyr$expand_grid(!!!list("sudo systemctl", c("status", "restart", "stop", "enable", "disable"), c("nginx", "rstudio-server", "shiny-server", "glances")))
  services <- purrr$pmap_chr(services, paste)

  ls <- tidyr$expand_grid(!!!list("ls", c("/", "/root", "/home", "/home/ubuntu"))) |> purrr$pmap_chr(paste)

  commandChoices <- list(
    # logs = "tail -100 /var/log/ndexr/build.log",
    cat = cat_commands,
    disk = disk,
    docker = docker_commands,
    ssh = ssh_commands,
    services = services,
    ls = ls,
    system_info = system_info
  )


  tags$div(
    class = "",
    tags$div(
      class = "row",
      tags$div(
        class = "col-12 mb-2",
        inputs$selectizeInput(
          ns("commandText"),
          label = "Send Command",
          choices = commandChoices, selected = "",
          options = list(create = TRUE)
        )
      )
    ),
    tags$div(
      class = "row",
      tags$div(
        class = "col-12 mb-2",
        tags$label(
          class = "control-label",
          id = "commandTextTwo",
          `for` = ns("commandTextTwo"),
          "Command Text"
        ),
        tags$textarea(
          id = ns("commandTextTwo"),
          class = "form-control"
        )
      )
    ),
    tags$div(
      class = "row mb-3",
      tags$div(
        class = "col-12 d-flex justify-content-between",
        shiny$actionButton(
          inputId = ns("copy-btn"),
          label = "Copy", class = "btn-warning mb-3",
          icon = shiny$icon("copy")
        ),
        shiny$actionButton(ns("sendCommand"), "Send", class = "btn btn-primary")
      )
    ),
    shiny$uiOutput(ns("terminal")),
    tags$script({
      HTML(
        paste0(
          "const copyBtn = document.querySelector('#", ns("copy-btn"), "');
                      const textToCopy = document.querySelector('#", ns("terminal"), "');

                      copyBtn.addEventListener('click', () => {
                        navigator.clipboard.writeText(textToCopy.innerText).then(() => {
                          copyBtn.innerText = 'Copied!';
                          setTimeout(() => {
                            copyBtn.innerText = 'Copy to clipboard';
                          }, 2000);
                        });
                      });"
        )
      )
    })
  )
}

#' @export
server_send_command <- function(id = "send_command", ec2data = NULL) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(ssh)
    box::use(jsonlite)
    box::use(tidyr)
    box::use(glue[glue])
    box::use(purrr)
    box::use(.. / .. / .. / connections / redis)
    box::use(.. / .. / .. / connections / ssh)
    box::use(.. / .. / .. / inputs / inputs)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      chatText <- shiny$eventReactive(
        input$sendCommand,
        {
          ns_common_store_user <- ec2data$ns_common_store_user
          ec2data <- redis$get_state(ns_common_store_user(ec2data$instance$InstanceId))

          codeText <- tryCatch(
            {
              ssh$ssh_execute(ec2data, paste0(input$commandText, input$commandTextTwo))
            },
            error = function(err) {
              as.character(err)
            }
          )

          output$terminal <- shiny$renderUI({
            inputs$card_collapse(
              ns,
              title = "Output",
              tags$div(
                class = "row",
                tags$div(
                  class = "col-12 p-3",
                  lapply(
                    codeText, function(x) {
                      tags$code(tags$pre(x))
                    }
                  )
                )
              )
            )
          })

          paste0(unlist(codeText, recursive = TRUE), collapse = "\n")
        }
      )

      shiny$observe({
        chatText()
      })

      chatText
    }
  )
}
