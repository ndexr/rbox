#' @export
ui_logs <- function(id = "logs", ec2data, credentials) {
  box::use(shiny = shiny[tags, HTML])
  box::use(.. / modifystate)
  box::use(.. / .. / .. / inputs / inputs)

  ns <- shiny$NS(id)

  shiny$uiOutput(ns("indicators"))
}

#' @export
server_logs <- function(id = "logs", ec2data, credentials) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(glue[glue])
    box::use(ssh)
    box::use(shinyWidgets)
    box::use(.. / .. / .. / connections / ssh)
    box::use(.. / .. / .. / inputs / inputs)
    box::use(.. / .. / .. / connections / redis)
    box::use(stringr)
    box::use(shiny = shiny[tags, HTML])
    box::use(glue[glue])
    box::use(ssh)
    box::use(stats)
    box::use(purrr)
    box::use(shinyWidgets)
    box::use(.. / .. / .. / connections / ssh)
    box::use(.. / .. / .. / inputs / inputs)
    box::use(.. / workflows / https)
    box::use(.. / workflows / add_user)
    box::use(.. / workflows / gitlab)
    box::use(.. / workflows / docker)
    box::use(.. / .. / .. / gpt / gpt)
    box::use(.. / ui / navbar)
    box::use(.. / modifystate)
    box::use(.. / workflows / settings)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      ns_common_store_user <- ec2data$ns_common_store_user
      uuid <- uuid::UUIDgenerate()

      importState <- redis$get_state(ns_common_store_user(ec2data$instance$InstanceId))
      if (length(importState)) {
        shiny$showNotification("Importing state")
        ec2data <- importState
      } else {
        shiny$showNotification("Storing for first time")
        redis$store_state(ns_common_store_user(ec2data$instance$InstanceId), ec2data)
      }


      command <- "cat /var/log/ndexr/build.log"


      if (ec2data$instance$state == "running") {
        stdout <- tryCatch(
          {
            shiny$showNotification(command)
            ssh$ssh_execute(ec2data, command = command)[[2]]
          },
          error = function(err) {
            "logfile not found"
          }
        )
      } else {
        stdout <- "logfile not found"
      }



      indicators <- c(
        "NDEXR_STARTED", "NGINX_COMPLETE", "GLANCES_COMPLETE",
        "DOCKER_COMPLETE", "RSTUDIO_COMPLETE", "VSCODE_COMPLETE"
      )

      indicators <- stats$setNames(indicators, indicators)
      indicators <- purrr$map(indicators, function(x) stringr$str_detect(stdout, x))

      is_complete <- function(class, check) {
        ifelse(
          check,
          paste(class, "text-primary"),
          paste(class, "text-info")
        )
      }

      output$indicators <- shiny$renderUI({
        tags$div(
          class = "accordion",
          id = "indicatorsAccordion",
          shiny$uiOutput(ns("settingsIndicator")),
          shiny$uiOutput(ns("stateIndicator")),
          shiny$uiOutput(ns("runningRequired"))
        )
      })

      if (ec2data$instance$state == "running") {
        box::use(. / send_command)
        output$runningRequired <- shiny$renderUI({
          out <-
            tags$div(
              shiny$uiOutput(ns("sshIndicator")),
              shiny$uiOutput(ns("logsIndicator")),
              shiny$uiOutput(ns("nginxIndicator")),
              shiny$uiOutput(ns("rstudioIndicator")),
              shiny$uiOutput(ns("gptIndicator")),
              shiny$uiOutput(ns("deployIndicator"))
              # shiny$uiOutput(ns("dockerIndicator"))
            )
          out
        })
      }

      output$gptIndicator <- shiny$renderUI({
        shiny$renderUI({
          tags$div(
            class = "accordion-item",
            tags$h2(
              class = "accordion-header",
              id = "gptHeader",
              tags$button(
                class = "accordion-button",
                class = is_complete("accordion-button collapsed", indicators$NDEXR_STARTED),
                type = "button",
                `data-bs-toggle` = "collapse",
                `data-bs-target` = "#gptCollapse",
                `aria-expanded` = "false",
                `aria-controls` = "gptCollapse",
                "cGPT"
              )
            ),
            tags$div(
              id = "gptCollapse",
              class = "accordion-collapse collapse",
              `aria-labelledby` = "gptHeader",
              `data-bs-parent` = "#indicatorsAccordion",
              tags$div(
                class = "accordion-body",
                {
                  uuid <- uuid::UUIDgenerate()
                  out <- gpt$ui_gpt(ns(paste0("gpt", uuid)), ec2data)
                  gpt$server_gpt(paste0("gpt", uuid), ns_common_store_user, ec2data)
                  out
                }
              )
            )
          )
        })
      })


      output$sshIndicator <- shiny$renderUI({
        shiny$renderUI({
          tags$div(
            class = "accordion-item",
            tags$h2(
              class = "accordion-header",
              id = "sshHeader",
              tags$button(
                class = "accordion-button",
                class = is_complete("accordion-button collapsed", indicators$NDEXR_STARTED),
                type = "button",
                `data-bs-toggle` = "collapse",
                `data-bs-target` = "#sshCollapse",
                `aria-expanded` = "false",
                `aria-controls` = "sshCollapse",
                "Execute SSH Command"
              )
            ),
            tags$div(
              id = "sshCollapse",
              class = "accordion-collapse collapse",
              `aria-labelledby` = "sshHeader",
              `data-bs-parent` = "#indicatorsAccordion",
              tags$div(
                class = "accordion-body",
                {
                  out <- send_command$ui_send_command(ns("send_command"), ec2data)
                  send_command$server_send_command(ec2data = ec2data)
                  out
                }
              )
            )
          )
        })
      })

      output$settingsIndicator <- shiny$renderUI({
        shiny$renderUI({
          tags$div(
            class = "accordion-item",
            tags$h2(
              class = "accordion-header",
              id = "settingsHeader",
              tags$button(
                class = "accordion-button",
                class = is_complete("accordion-button collapsed", indicators$NDEXR_STARTED),
                type = "button",
                `data-bs-toggle` = "collapse",
                `data-bs-target` = "#settingsCollapse",
                `aria-expanded` = "false",
                `aria-controls` = "settingsCollapse",
                "Server Metadata"
              )
            ),
            tags$div(
              id = "settingsCollapse",
              class = "accordion-collapse collapse",
              `aria-labelledby` = "settingsHeader",
              `data-bs-parent` = "#indicatorsAccordion",
              tags$div(
                class = "accordion-body",
                {
                  out <- settings$ui_settings(ns(paste0("settings", uuid)), ec2data)
                  settings$server_settings(paste0("settings", uuid), ec2data)
                  out
                }
              )
            )
          )
        })
      })


      output$stateIndicator <- shiny$renderUI({
        shiny$renderUI({
          tags$div(
            class = "accordion-item",
            tags$h2(
              class = "accordion-header",
              id = "stateHeader",
              tags$button(
                class = is_complete("accordion-button collapsed", indicators$NDEXR_STARTED),
                type = "button",
                `data-bs-toggle` = "collapse",
                `data-bs-target` = "#stateCollapse",
                `aria-expanded` = "false",
                `aria-controls` = "stateCollapse",
                "Modify Server State and Instance Size"
              )
            ),
            tags$div(
              id = "stateCollapse",
              class = "accordion-collapse collapse",
              `aria-labelledby` = "stateHeader",
              `data-bs-parent` = "#indicatorsAccordion",
              tags$div(
                class = "accordion-body",
                {
                  out <- modifystate$ui_modifystate(ns("modifystate"), ec2data)
                  modifystate$server_modifystate("modifystate", ec2data)
                  out
                }
              )
            )
          )
        })
      })

      output$logsIndicator <- shiny$renderUI({
        tags$div(
          class = "accordion-item",
          tags$h2(
            class = "accordion-header",
            id = "ndexrHeader",
            tags$button(
              class = is_complete("accordion-button collapsed", indicators$NDEXR_STARTED),
              type = "button",
              `data-bs-toggle` = "collapse",
              `data-bs-target` = "#ndexrCollapse",
              `aria-expanded` = "false",
              `aria-controls` = "ndexrCollapse",
              "Installation Logs"
            )
          ),
          tags$div(
            id = "ndexrCollapse",
            class = "accordion-collapse collapse",
            `aria-labelledby` = "ndexrHeader",
            `data-bs-parent` = "#indicatorsAccordion",
            tags$div(
              class = "accordion-body",
              shiny$uiOutput(
                ns("installLogs")
              )
            )
          )
        )
      })

      output$nginxIndicator <- shiny$renderUI({
        #
        tags$div(
          class = "accordion-item",
          tags$h2(
            class = "accordion-header",
            id = "nginxHeader",
            tags$button(
              class = is_complete("accordion-button active ", indicators$NGINX_COMPLETE),
              type = "button",
              `data-bs-toggle` = "collapse",
              `data-bs-target` = "#nginxCollapse",
              `aria-expanded` = "false",
              `aria-controls` = "nginxCollapse",
              "First Steps"
            )
          ),
          tags$div(
            id = "nginxCollapse",
            class = "accordion-collapse collapse",
            `aria-labelledby` = "nginxHeader",
            `data-bs-parent` = "#indicatorsAccordion",
            tags$div(
              class = "accordion-body",
              navbar$ui_ec2_navbar(ns("ec2_navbar"), ec2data),
              {
                out <- https$ui_https(ns(paste0("https", uuid)), ec2data, credentials)
                https$server_https(paste0("https", uuid), ec2data, ns_common_store_user, credentials)
                out
              }
            )
          )
        )
      })


      output$dockerIndicator <- shiny$renderUI({
        tags$div(
          class = "accordion-item",
          tags$h2(
            class = "accordion-header",
            id = "dockerHeader",
            tags$button(
              class = is_complete("accordion-button collapsed", indicators$DOCKER_COMPLETE),
              type = "button",
              `data-bs-toggle` = "collapse",
              `data-bs-target` = "#dockerCollapse",
              `aria-expanded` = "false",
              `aria-controls` = "dockerCollapse",
              "Container Management"
            )
          ),
          tags$div(
            id = "dockerCollapse",
            class = "accordion-collapse collapse",
            `aria-labelledby` = "dockerHeader",
            `data-bs-parent` = "#indicatorsAccordion",
            tags$div(
              class = "accordion-body",
              {
                out <- gitlab$ui_gitlab(ns(paste0("gitlab", uuid)), ec2data)
                gitlab$server_gitlab(paste0("gitlab", uuid), ec2data)
                out
              }
              # docker$ui_docker(ns(paste0("docker", uuid)), ec2data)
            )
          )
        )
      })
      # docker$server_docker(paste0("docker", uuid), ec2data)

      output$rstudioIndicator <- shiny$renderUI({
        tags$div(
          class = "accordion-item",
          tags$h2(
            class = "accordion-header",
            id = "rstudioHeader",
            tags$button(
              class = is_complete("accordion-button collapsed", indicators$RSTUDIO_COMPLETE),
              type = "button",
              `data-bs-toggle` = "collapse",
              `data-bs-target` = "#rstudioCollapse",
              `aria-expanded` = "false",
              `aria-controls` = "rstudioCollapse",
              "User Management"
            )
          ),
          tags$div(
            id = "rstudioCollapse",
            class = "accordion-collapse collapse",
            `aria-labelledby` = "rstudioHeader",
            `data-bs-parent` = "#indicatorsAccordion",
            tags$div(
              class = "accordion-body",
              add_user$ui_add_user(ns(paste0("add_user", uuid)), ec2data)
            )
          )
        )
      })

      output$deployIndicator <- shiny$renderUI({
        tags$div(
          class = "accordion-item",
          tags$h2(
            class = "accordion-header",
            id = "deployHeader",
            tags$button(
              class = is_complete(
                "accordion-button collapsed",
                all(indicators$DOCKER_COMPLETE, indicators$RSTUDIO_COMPLETE)
              ),
              type = "button",
              `data-bs-toggle` = "collapse",
              `data-bs-target` = "#deployCollapse",
              `aria-expanded` = "false",
              `aria-controls` = "deployCollapse",
              "Launch Template"
            )
          ),
          tags$div(
            id = "deployCollapse",
            class = "accordion-collapse collapse",
            `aria-labelledby` = "deployHeader",
            `data-bs-parent` = "#indicatorsAccordion",
            tags$div(
              class = "accordion-body",
              tags$pre("git clone https://gitlab.com/ndexr/shiny-starter.git", class = "text-dark"),
              tags$pre("cd shiny-starter && make local", class = "text-dark")
            )
          )
        )
      })


      add_user$server_add_user(paste0("add_user", uuid), ec2data, credentials)

      output$installLogs <- shiny$renderUI({
        tags$div(
          tags$div(
            id = ns("instlogpanel"),
            style = "height: 500px; overflow-y: scroll;",
            tags$pre(
              tags$code(
                as.character(paste0(stdout))
              )
            )
          ),
          tags$div(
            tags$script(
              paste0("var element = document.getElementById(\"", ns("instlogpanel"), "\");"),
              paste0("element.scrollTop = element.scrollHeight;")
            )
          )
        )
      })

      redis$store_state(ns_common_store_user(ec2data$instance$InstanceId), ec2data)
    }
  )
}
