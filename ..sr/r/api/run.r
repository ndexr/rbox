box::use(plumber)

is_docker <- isTRUE(getOption("docker"))

if (is_docker) apiHost <- "0.0.0.0" else "127.0.0.1"
plumber$options_plumber(port = 7635, apiHost = apiHost)

pr <- plumber$pr("plumber.r")

pr <- plumber::plumb("plumber.r")

pr$run()
