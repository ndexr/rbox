#' @export
ui_portfolio <- function(original_class = "col-lg-4 col-sm-6 mb-4") {
  box::use(shiny[withTags])
  withTags(
    section(
      class = "page-section bg-light", id = "portfolio",
      div(
        class = "",
        div(
          class = "text-center",
          h2(class = "section-heading text-uppercase", "Portfolio"),
          h3(class = "section-subheading text-muted", "Lorem ipsum dolor sit amet consectetur.")
        ),
        div(
          class = "row",
          div(
            class = original_class,
            div(
              class = "portfolio-item",
              a(
                class = "portfolio-link", `data-bs-toggle` = "modal", href = "#portfolioModal1",
                div(
                  class = "portfolio-hover",
                  div(class = "portfolio-hover-content", i(class = "fas fa-plus fa-3x"))
                ),
                img(class = "img-fluid", src = "homepage/img/portfolio/1.jpg", alt = "..."),
              ),
              div(
                class = "portfolio-caption",
                div(class = "portfolio-caption-heading", "Threads"),
                div(class = "portfolio-caption-subheading text-muted", "Illustration")
              )
            )
          )
        )
      )
    )
  )
}

#' @export
ui_portfolio_modal <- function() {
  box::use(shiny[withTags])
  withTags(
    div(
      class = "portfolio-modal modal fade", id = "portfolioModal1", tabindex = "-1", role = "dialog", `aria-hidden` = "true",
      div(
        class = "modal-dialog",
        div(
          class = "modal-content",
          div(class = "close-modal", `data-bs-dismiss` = "modal", img(src = "homepage/img/close-icon.svg", alt = "Close modal")),
          div(
            class = "",
            div(
              class = "row justify-content-center",
              div(
                class = "col-lg-8",
                div(
                  class = "modal-body",
                  h2(class = "text-uppercase", "Project Name"),
                  p(class = "item-intro text-muted", "Lorem ipsum dolor sit amet consectetur."),
                  img(class = "img-fluid d-block mx-auto", src = "homepage/img/portfolio/1.jpg", alt = "..."),
                  p("Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!"),
                  ul(
                    class = "list-inline",
                    li(
                      strong("Client:"),
                      "Threads"
                    ),
                    li(
                      strong("Category:"),
                      "Illustration"
                    )
                  ),
                  button(
                    class = "btn  btn-xl text-uppercase", `data-bs-dismiss` = "modal", type = "button",
                    i(class = "fas fa-xmark me-1"),
                    "Close Project"
                  )
                )
              )
            )
          )
        )
      )
    )
  )
}
