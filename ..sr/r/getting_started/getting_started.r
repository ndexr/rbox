#' @export
ui_getting_started <- function(id = "getting_started") {
  box::use(shiny = shiny[tags, HTML])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"), container = function(...) tags$div(class = "col-12", ...))
}

#' @export
server_getting_started <- function(id = "getting_started", ns_common_store_user) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(fs)
    box::use(utils)
    box::use(.. / inputs / inputs)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        instruction_menu <- tags$div(
          class = "row",
          tags$div(
            class = "col-12 mb-3",
            tags$h5("Template", class = "font-weight-light"),
            tags$div(class = "border border-light bg-light mb-3"),
            shiny$downloadLink(ns("downloadData"), "Download", class = "link-light")
          )
        )

        tags$div(
          class = "row",
          inputs$card_collapse(
            ns,
            title = "Instructions",
            footer = NULL,
            body = instruction_menu,
            width = 12
          )
        )
      })

      output$downloadData <- shiny$downloadHandler(
        filename = function() {
          paste("output", "zip", sep = ".")
        },
        content = function(fname) {
          template_files <- fs$dir_ls("customer_template/", recurse = T)
          utils$zip(zipfile = fname, files = template_files)
        },
        contentType = "application/zip"
      )
    }
  )
}
