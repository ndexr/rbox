#' @export
ui_https <- function(id = "https", ec2data, credentials) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(shinyFiles)
    box::use(.. / domain)
    box::use(.. / .. / .. / inputs / inputs)
  }

  ns <- shiny$NS(id)
  hosted_zones <- ec2data$hosted_zones

  

  if (stringr::str_detect(ec2data$current_domain, '^i-')) {
    tags$div(
      class = "container",
      tags$div(
        class = "row",
        tags$div(
          class = "col-12 flex-col justify-content-center",
          tags$p("After purchasing your desired domain through Amazon’s Route 53 service, the next step is to connect it to your server. Keep in mind that it may take some time for AWS to approve your domain and make it visible within the service. Once it’s approved, you should be able to see it and proceed with configuring HTTPS."),
          inputs$selectizeInput(
            ns("Domain"), "Domain", "www.",
            choices = c("www.", "db.", "rstudio.", "git.", "gitlab.", "about.", "postgres.", "redis."),
            options = list(create = TRUE)
          ),
          inputs$selectizeInput(ns("hostedZone"), NULL, choices = hosted_zones$Name, multiple = FALSE),
          inputs$textInput(ns("email"), "Email for Certbot", credentials$email),
          shiny$uiOutput(ns("response"))
        ),
        tags$div(
          class = "col-12 d-flex justify-content-end",
          shiny$actionButton(ns("updateDomain"),
                             "Connect",
                             class = "btn btn-primary my-2"
          )
        )
      )
    )
  } else {
    tags$div()
  }

}

#' @export
server_https <- function(id = "https", ec2data, ns_common_store_user, credentials) {
  {
    box::use(ssh)
    box::use(glue)
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / .. / .. / connections / postgres)
    box::use(.. / .. / .. / connections / redis)
    box::use(.. / domain)
    box::use(purrr)
    box::use(ssh_utils = .. / .. / .. / connections / ssh)
    box::use(readr)
    box::use(stringr)
    box::use(.. / ec2_backend)
    box::use(.. / .. / .. / inputs / inputs)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      domainOutput <- shiny$eventReactive(
        input$updateDomain,
        {
          shiny$showNotification("Beginning HTTPS Configuration")

          shiny$req(input$hostedZone)

          ns_common_store_user <- ec2data$ns_common_store_user
          hosted_zones <- ec2data$hosted_zones
          elastic_ips <- ec2data$elastic_ips
          has_elastic <- ec2data$instance$PublicIpAddress %in% elastic_ips$PublicIp

          na_elastic_ip <- is.na(elastic_ips$InstanceId)
          elastic_ip_available <- any(na_elastic_ip)

          if (all(!has_elastic, !elastic_ip_available)) {
            shiny$showNotification("Allocating ElasticIp")
            shiny$showNotification(ec2data$instance$PublicIpAddress)
            tryCatch(ec2_backend$ec2_allocate_elastic_ip(), error = function(err) {
              shiny$showNotification(as.character(err))
              shiny$req(FALSE)
            })
            elastic_ips <- ec2_backend$ec2_list_elastic_ip()
            na_elastic_ip <- is.na(elastic_ips$InstanceId)
            server_ip <- elastic_ips[na_elastic_ip, ]
            ec2_backend$ec2_associate_elastic_ip(
              ns_common_store_user = ns_common_store_user,
              InstanceId = ec2data$instance$InstanceId,
              server_ip$AllocationId
            )
          } else {
            if (has_elastic) {
              shiny$showNotification("Elastic IP already set")
              server_ip <- elastic_ips[elastic_ips$PublicIp %in% ec2data$instance$PublicIpAddress, ]
            } else {
              shiny$showNotification("Associating elastic ip")
              server_ip <- elastic_ips[is.na(elastic_ips$InstanceId), ][1, ]
              ec2_backend$ec2_associate_elastic_ip(
                ns_common_store_user = ns_common_store_user,
                InstanceId = ec2data$instance$InstanceId,
                server_ip$AllocationId
              )
            }
          }

          hostedZone <- sub("[.]$", "", input$hostedZone)
          Domain <- input$Domain
          if (stringr$str_detect(Domain, "www")) {
            Domain <- c(Domain, "")
          }
          shiny$showNotification("Updating hosted zones")
          ec2_backend$route53_update_hosted_zones(
            ns_common_store_user = ns_common_store_user,
            Domain = paste0(
              Domain,
              hostedZone
            ),
            HostedZoneId = hosted_zones[hosted_zones$Name == input$hostedZone, ]$Id,
            ElasticIp = server_ip$PublicIp
          )

          ec2data$instance$PublicIpAddress <- server_ip$PublicIp
          shiny$showNotification(server_ip$PublicIp)
          redis$store_state(ns_common_store_user(ec2data$instance$InstanceId), ec2data)


          list(
            HostedZoneId = hosted_zones[hosted_zones$Name == input$hostedZone, ]$Id,
            ElasticIp = server_ip$PublicIp,
            email = input$email
          )
        }
      )


      shiny$observeEvent(
        domainOutput(),
        {
          print(domainOutput())

          hostedZone <- sub("[.]$", "", input$hostedZone)
          domain <- input$Domain

          email <- domainOutput()$email

          try(file.remove("/root/.ssh/known_hosts"))
          try(file.remove("~/.ssh/known_hosts"))

          ec2data <- redis$get_state(ns_common_store_user(ec2data$instance$InstanceId))
          PublicIpAddress <- ec2data$instance$PublicIpAddress
          shiny$showNotification(PublicIpAddress)
          shiny$showNotification("Passing files")
          nginx_conf <- readr$read_file("./shared/etc/nginx/nginx.conf")
          nginx_default <- readr$read_file("./shared/etc/nginx/sites-enabled/default")

          if (stringr$str_detect(domain, "www")) {
            # remove_www <- stringr$str_remove(domain, 'www.')

            nginx_default <- stringr$str_replace_all(
              nginx_default, "ndexr.com",
              paste0(c("", domain), hostedZone, collapse = " ")
            )
          } else {
            nginx_default <- stringr$str_replace_all(nginx_default, "ndexr.com", domain)
          }



          readr$write_file(nginx_default, "./shared/etc/nginx/sites-enabled-update/default")



          ssh_utils$ssh_upload_file(ec2data, "./shared/etc/nginx/nginx.conf", "/etc/nginx")
          ssh_utils$ssh_upload_file(ec2data, "./shared/etc/nginx/sites-enabled-update/default", "/etc/nginx/sites-enabled")

          shiny$showNotification("sudo systemctl restart nginx")
          ssh_utils$ssh_execute(ec2data, "sudo systemctl restart nginx")

          try(file.remove("/root/.ssh/known_hosts"))
          try(file.remove("~/.ssh/known_hosts"))

          for (time in 10:1) {
            shiny$showNotification(glue$glue("Sleeping for {time} for seconds"))
            Sys.sleep(1)
          }

          # Domain <- domainOutput()$Domain
          if (stringr$str_detect(domain, "www")) {
            # remove_www <- stringr$str_remove(domain, 'www.')
            # domains <- paste(paste0("-d ", hostez), collapse = ' ')
            dmn <- paste0(c("www.", ""), hostedZone)
            domains <- paste0("-d ", dmn, collapse = " ")
            certbot_command <- glue$glue("sudo certbot --nginx {domains} --non-interactive --agree-tos -m {email}")
          } else {
            dmn <- paste0(domain, hostedZone)
            domains <- paste0("-d ", dmn, collapse = " ")

            certbot_command <- glue$glue("sudo certbot --nginx {domains} --non-interactive --agree-tos -m {email}")
          }


          certbot_text <- ssh_utils$ssh_execute(ec2data, certbot_command)

          domain_https <- data.frame(
            time = Sys.time(),
            ElasticIp = domainOutput()$ElasticIp,
            domain = dmn[[1]],
            InstanceId = ec2data$instance$InstanceId
          )
          postgres$table_append(domain_https)

          shiny$removeModal()

          shiny$showModal(
            inputs$modalDialog(
              tags$div(
                class = "row",
                tags$pre(
                  paste0("\n", certbot_text, collapse = "\n\n")
                )
              )
            )
          )
        }
      )
    }
  )
}
