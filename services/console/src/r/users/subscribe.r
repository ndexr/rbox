#' @export
ui_subscribe <- function(id, frontpage = FALSE) {
  box::use(shinyjs)
  box::use(shiny = shiny[tags, HTML])
  box::use(.. / inputs / inputs)

  ns <- shiny$NS(id)

  # tags$body(
  if (frontpage) {
    shiny$uiOutput(ns("ui"))
  } else {
    inputs$card_collapse(
      ns, "Introduction", shiny$uiOutput(ns("ui"))
    )
  }
}

#' @export
server_subscribe <- function(id, frontpage = FALSE) {
  {
    box::use(shiny = shiny[tags, HTML])
  }
  shiny$moduleServer(
    id, function(input, output, session) {
      output$ui <- shiny$renderUI({
        tags$div(
          class = "container",
          tags$div(
            class = "row",
            tags$div(
              class = "col-12",
              tags$div(
                class = "row",
                tags$div(
                  class = "col-12",
                  tags$h1(
                    class = "display-4 text-primary",
                    tags$i(class = "mb-2 bi bi-laptop"),
                    "Welcome to ndexr"
                  ),
                  tags$p(
                    class = "lead",
                    "ndexr is a powerful platform that replicates R application workflows."
                  )
                )
              )
            ),
            tags$div(
              class = "col-md-4",
              tags$div(
                class = "row p-2",
                tags$h2(
                  class = "text-info",
                  tags$i(
                    class = "mb-2 bi bi-cloud"
                  ),
                  "Simplified Cloud Computing"
                ),
                tags$div(
                  class = "col-12",
                  tags$p(
                    class = "text-muted",
                    "One of the challenges developers face is setting up and managing cloud infrastructure for their applications. ndexr simplifies this process by providing a user-friendly interface and tools specifically designed for R developers. It allows developers to easily create and manage remote servers, eliminating the need to worry about complex cloud configurations. By abstracting away the complexities of cloud computing, ndexr enables developers to focus on their code and application development, making the overall workflow more efficient."
                  )
                )
                # tags$div(
                #   class = "col-sm-4",
                #   tags$p(
                #     class = "text-center",
                #     tags$img(
                #       src = "./images/images.jpg", class = "img rounded img-fluid"
                #     )
                #   )
                # )
              )
            ),
            tags$div(
              class = "col-sm-4",
              tags$p(
                class = "text-center",
                tags$img(
                  src = "./images/servers.jpg", class = "img rounded img-fluid"
                )
              )
            ),
            tags$div(
              class = "col-md-4",
              tags$div(
                class = "row",
                tags$h2(
                  class = "text-warning",
                  tags$i(
                    class = "mb-2 bi bi-graph-up"
                  ),
                  "Modular and Scalable Application Development"
                ),
                tags$div(
                  class = "col-12",
                  tags$p(
                    class = "text-muted",
                    "Building modular and scalable applications is crucial for developers, but it can be a time-consuming and intricate process. ndexr offers pre-configured applications and templates, including popular tools like vscode and rstudio, which significantly streamline the development process. Additionally, it provides a template for R Shiny applications using modern modular design patterns. This assists developers in creating the proper structure for scalable applications, not only in terms of serving capacity but also in harnessing the full potential of Shiny. By providing these resources, ndexr helps developers save time and effort in building robust and scalable web applications."
                  )
                )
              )
            ),
            tags$div(
              class = "row",
              tags$h2(
                class = "text-primary",
                tags$i(
                  class = "mb-2 bi bi-eye"
                ),
                "Personalized Control and Cost Efficiency"
              ),
              tags$p(
                class = "text-muted",
                "ndexr recognizes the importance of giving developers control over their work. It enables users to deploy their applications behind their own domain, ensuring a personalized and professional online presence. Moreover, ndexr offers cost-effective solutions by optimizing server usage. Developers can manage their remote servers and selectively turn them on or off to avoid unnecessary costs. The platform allows developers to work within a budget-friendly framework, ensuring that server fees do not become a financial burden. By providing control over deployment and optimizing costs, ndexr empowers developers to work efficiently and effectively without compromising their budget."
              ),
              tags$ul(
                class = "list-unstyled",
                tags$li(
                  tags$i(
                    class = "mb-2 bi bi-cloud-check-fill fs-3 bg-light rounded-circle text-primary me-3 p-2"
                  ),
                  "Provision pre-built servers with a deeply organized framework for developing modern R applications"
                ),
                tags$li(
                  tags$i(
                    class = "mb-2 bi bi-palette-fill fs-3 bg-light rounded-circle text-primary me-3 p-2"
                  ),
                  "Utilize professional Shiny templates for minimal regret long term from bad design decisions"
                ),
                tags$li(
                  tags$i(
                    class = "mb-2 bi bi-people-fill fs-3 bg-light rounded-circle text-primary me-3 p-2"
                  ),
                  "Gain access to secure user management, groups, conditional rendering, and integrated payment features through consultation"
                ),
                tags$li(
                  tags$i(
                    class = "mb-2 bi bi-shield-lock-fill fs-3 bg-light rounded-circle text-primary me-3 p-2"
                  ),
                  "SSH Key Manager and Security Group Manager"
                ),
                tags$li(
                  tags$i(
                    class = "mb-2 bi bi-laptop-fill fs-3 bg-light rounded-circle text-primary me-3 p-2"
                  ),
                  "Launch, clone, and restore your servers and set up a configurable domain from your mobile."
                ),
                tags$li(
                  tags$i(
                    class = "mb-2 bi bi-eye fs-3 bg-light rounded-circle text-primary me-3 p-2"
                  ),
                  "Build your career by learning how to manage optimized R workflows"
                )
              )
            )
          ),
          {
            if (frontpage) {
              tags$section(
                class = "bg-light",
                tags$div(
                  class = "container text-center p-3",
                  tags$h2(
                    "Get started here!"
                  ),
                  tags$p(
                    class = "lead",
                    "Join the individual developers, consultants, and small businesses who are already benefiting from ndexr."
                  ),
                  tags$a(
                    class = "btn btn-primary btn-lg",
                    href = "https://console.ndexr.io",
                    role = "button",
                    "Get Inside"
                  )
                )
              )
            }
          }
        )
      })
    }
  )
}


#' @export
stripe_product_create <- function(name) {
  box::use(reticulate)
}

#' @export
make_plan <- function(plan_name = "Basic", price = 28, payment_link, descriptors) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(glue)
    box::use(purrr)
  }
  tags$div(
    class = "card mb-4 box-shadow",
    tags$div(
      class = "card-header",
      tags$h4(
        class = "my-0 font-weight-normal",
        plan_name
      )
    ),
    tags$h1(
      class = "card-title",
      tags$div(
        class = "d-flex align-items-center justify-content-center",
        tags$div(glue$glue("${price}"), class = "display-5 text-dark"),
        tags$small(
          class = "text-muted",
          "/ mo"
        )
      )
    ),
    tags$div(
      class = "card-body",
      tags$ul(
        class = "mt-3 mb-4",
        purrr$map(
          descriptors, function(x) {
            tags$li(class = "text-left", x)
          }
        )
      )
    ),
    tags$div(
      class = "card-footer",
      tags$a(href = payment_link, target = "_blank", glue$glue("Buy {plan_name} Plan"), class = "btn")
    )
  )
}
