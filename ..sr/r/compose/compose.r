#' @export
docker_compose_service <- function(service_name, ...) {
  argg <- c(as.list(environment()), list(...))
  argg$service_name <- NULL
  argg <- list(argg)
  names(argg) <- service_name
  argg
}

#' @export
docker_compose_template <- function(appname = "ndexr-template",
                                    nginx = TRUE,
                                    port = 9000,
                                    domain = "local") {
  {
    box::use(yaml)
    box::use(. / compose)
    box::use(glue)
    # box::reload(compose)
    box::use(shiny)
    box::use(readr)
    box::use(stringr)
  }


  ns <- shiny$NS(appname)(domain)
  ns <- shiny$NS(ns)
  id_nginx <- ns("nginx")
  id_app <- ns("console")

  nginx <- compose$docker_compose_service(
    service_name = id_nginx,
    container_name = id_nginx,
    image = glue$glue("registry.gitlab.com/fdrennan/rapp:{id_nginx}"),
    build = list(context = "./nginx"),
    volumes = c(
      "/etc/letsencrypt:/etc/letsencrypt",
      "/var/www/certbot:/var/www/certbot"
    ),
    ports = list(glue$glue("{port}:80")),
    network_mode = "bridge",
    restart = "unless-stopped",
    depends_on = list(id_app),
    links = list(id_app),
    profiles = list(domain),
    command = c("nginx-debug", "-g", "daemon off;")
  )

  template <- compose$docker_compose_service(
    service_name = id_app,
    container_name = id_app,
    image = glue$glue(
      "registry.gitlab.com/fdrennan/rapp:{id_app}"
    ),
    build = list(context = glue$glue("./.")),
    volumes = list(".aws:/root/.aws"),
    network_mode = "bridge",
    restart = "unless-stopped",
    profiles = list(domain),
    command = c("R", "-e", "shiny::runApp('/app/src')")
  )

  composeTest <- list(
    version = "3.9",
    services = c(nginx, template)
  )


  dir <- glue$glue("templates/{appname}")

  dir.create(glue$glue("{dir}/nginx/sites-enabled"), recursive = T)

  readme <- glue$glue("docker compose --profile={domain} up -d --remove-orphans")
  readr$write_file(readme, glue$glue("{dir}/README"))

  nginx_conf <- "user www-data;\nworker_processes auto;\npid /run/nginx.pid;\ninclude /etc/nginx/modules-enabled/*.conf;\n\nevents {\n\tworker_connections 768;\n}\n\nhttp {\n\tsendfile on;\n\ttcp_nopush on;\n\ttcp_nodelay on;\n\tkeepalive_timeout 65;\n\ttypes_hash_max_size 2048;\n\tserver_tokens off;\n  resolver 127.0.0.11 ipv6=off;\n\n\tserver_names_hash_bucket_size 64;\n\tserver_name_in_redirect off;\n\n\tinclude /etc/nginx/mime.types;\n\tdefault_type application/octet-stream;\n\n  map $http_upgrade $connection_upgrade {\n     default upgrade;\n     ''      close;\n  }\n\n\taccess_log /var/log/nginx/access.log;\n\terror_log /var/log/nginx/error.log;\n\n\tgzip on;\n\n\tgzip_vary on;\n\tgzip_proxied any;\n\tgzip_comp_level 6;\n\tgzip_buffers 16 8k;\n\tgzip_http_version 1.1;\n\tgzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;\n\n\tinclude /etc/nginx/sites-enabled/*;\n}\n\n\n"
  readr$write_file(nginx_conf, glue$glue("{dir}/nginx/nginx.conf"))

  nginx_default <- "server {\n\n        listen          80;\n        proxy_set_header Host $host;\n        proxy_set_header X-Real-IP $remote_addr;\n        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;\n\n        location / {\n            proxy_pass http://console:8000/;\n            proxy_http_version 1.1;\n            proxy_set_header Upgrade $http_upgrade;\n            proxy_set_header Connection $connection_upgrade;\n            proxy_read_timeout 20d;\n            proxy_buffering off;\n        }\n\n        location /template/ {\n            proxy_pass http://template:8000/;\n            proxy_http_version 1.1;\n            proxy_set_header Upgrade $http_upgrade;\n            proxy_set_header Connection $connection_upgrade;\n            proxy_read_timeout 20d;\n            proxy_buffering off;\n        }\n\n\n    }\n"
  nginx_default <- stringr$str_replace_all(nginx_default, "console", id_app)
  readr$write_file(nginx_default, glue$glue("{dir}/nginx/sites-enabled/default"))

  nginx_dockerfile <- "FROM nginx:latest\n\nCOPY nginx.conf /etc/nginx/nginx.conf\nCOPY sites-enabled /etc/nginx/sites-enabled\n\n\n"
  readr$write_file(nginx_dockerfile, glue$glue("{dir}/Dockerfile"))

  yaml$write_yaml(composeTest, file = glue$glue("{dir}/docker-compose.yaml"))
}

if (FALSE) {
  box::use(. / compose)
  box::reload(compose)
  compose$docker_compose_template(appname = "testapp", port = 9100, domain = "ndexr")
}
