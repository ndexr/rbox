#' @export
ui_manage_payments <- function(id = "manage_payments") {
  box::use(shiny = shiny[tags])
  box::use(.. / inputs / inputs)
  ns <- shiny$NS(id)
  inputs$card_collapse(
    ns,
    title = "Manage Subscription",
    body = tags$div(
      class = "container-fluid",
      id = ns("mainpanel"),
      tags$div(
        class = "col-12",
        shiny$uiOutput(ns("body"))
      ),
      tags$div(
        class = "col-12",
        shiny$uiOutput(ns("result"))
      )
    )
  )
}

#' @export
server_manage_payments <- function(id = "manage_payments", email, live = TRUE) {
  box::use(shiny = shiny[tags])
  box::use(. / stripe)
  box::use(.. / inputs / inputs)

  box::use(.. / connections / postgres)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      customer_ids <- stripe$stripe_customer_ids(emails = email)



      output$body <- shiny$renderUI({
        customer_subscriptions <- postgres$tables_row_retrieve("customer_id", customer_ids$customer_id, "subscription", showNotification = FALSE, dbname = "stripe")

        if (nrow(customer_subscriptions)) {
          out <- tags$div(
            class = "col-md-12",
            tags$p("Thank you for your support.", class = "text-center"),
            if (customer_subscriptions$subscription_id == "lifetime") {
              tags$div(
                class = "text-center",
                "You have free, lifetime access to ndexr. There is nothing else you need to do at the moment."
              )
            } else {
              tags$div(
                id = ns("cancelbutton"),
                class = "text-center",
                shiny$actionButton(ns("cancel"), "Cancel Access", class = "btn-warning")
              )
            }
          )
        } else {
          out <- shiny$uiOutput(ns("paymentOutputs"))
        }

        out
      })




      output$paymentOutputs <- shiny$renderUI({
        tags$div(
          class = "row justify-content-center",
          id = ns("cardpanel"),
          tags$div(
            class = "col-md-12",
            tags$div(
              tags$h5("Get Inside"),
              tags$h5("$350 / one time payment")
            )
          ),
          tags$div(
            class = "col-md-12",
            inputs$textInput(ns("number"), "Credit Card Number", value = {
              if (!getOption("docker")) {
                4242424242424242
              }
            }),
            tags$div(
              class = "row",
              shiny$column(
                width = 4,
                inputs$selectizeInput(
                  ns("exp_month"),
                  "Expiry Month", c(
                    "01", "02", "03", "04", "05", "06", "07",
                    "08", "09", "10", "11", "12"
                  ),
                  NULL
                )
              ),
              shiny$column(
                width = 4,
                inputs$selectizeInput(
                  ns("exp_year"),
                  "Expiry Year",
                  {
                    current_year <- lubridate::year(Sys.Date())
                    as.character(current_year:(current_year + 25))
                  },
                  NULL
                )
              ),
              shiny$column(
                width = 4,
                inputs$textInput(ns("cvc"), "CVC", value = {
                  if (!getOption("docker")) {
                    123
                  }
                })
              )
            )
          ),
          tags$div(
            class = "col-12",
            shiny::numericInput(ns("amountDollars"), "Amount (USD)", value = 350, min = 20, max = 1500)
          ),
          tags$div(
            class = "col-12 p-2",
            shiny$actionButton(ns("purchaseSubscription"),
              label = tags$div(
                class = "d-flex justify-content-center align-items-center",
                shiny$icon("cc-stripe", class = "fa fa-2x m-2"), tags$h5("Subscribe")
              ),
              width = "100%",
              class = "btn btn-primary"
            )
          ),
          tags$em("By continuing, you authorize ndexr to debit your card today, once, for the amount above.")
        )
      })
      shiny$observeEvent(input$purchaseSubscription, {
        shiny$showNotification('Checking')
        customer_subscriptions <- postgres$tables_row_retrieve("customer_id", customer_ids$customer_id, "subscription", showNotification = FALSE, dbname = "stripe")

        if (nrow(customer_subscriptions)) {

          shiny$showNotification("You're in.")
        } else {
          tryCatch(
            {
              shiny$showNotification('Starting stripe_payment_method_attach')
              # attach payment method given
              payment_method <- stripe$stripe_payment_method_attach(
                customer_id = customer_ids$customer_id,
                number = as.character(input$number),
                exp_month = as.integer(input$exp_month),
                exp_year = as.integer(input$exp_year),
                cvc = as.character(input$cvc),
                live = live
              )

              shiny$showNotification('Starting stripe_connection')
              con <- stripe$stripe_connection(live)
              shiny$showNotification('Starting PaymentIntent')
              out <- con$PaymentIntent$create(
                customer = payment_method$customer_id,
                amount = as.integer(input$amountDollars * 100),
                currency = "usd",
                payment_method = payment_method$payment_method_id,
                description = "Payment for serviced",
                receipt_email = "drennanfreddy@gmail.com",
                statement_descriptor = "ndexr"
                # price="price_1NE8qGIAcofnB5CRahRDtHsS"
              )
              #
              shiny$showNotification('Starting confirm')
              confirm <- out$confirm()
              shiny$showNotification('Success')
              subscription <- data.frame(
                subscription_id = paste0("one time"),
                customer_id = payment_method$customer_id,
                live = live,
                date_created = Sys.Date(),
                datetime_created = Sys.time()
              )
              postgres$table_create_or_upsert(subscription, "customer_id", dbname = "stripe")


              # plan_id <- ifelse(getOption("docker"), "plan_NzJdlZLQ5XcCNS", "plan_NyWWRoiPBcwTEL")
              # create a subscription
              # subscription <- stripe$stripe_subscription_start(
              #   customer_id = payment_method$customer_id,
              #   plan_id = plan_id, # 59.00 plan
              #   payment_method_id = payment_method$payment_method_id,
              #   live = live
              # )

              shiny::removeUI(
                selector = paste0(c("#", ns("cardpanel")), collapse = "")
              )

              shiny::insertUI(
                selector = paste0(c("#", ns("mainpanel")), collapse = ""),
                ui = tags$div(
                  id = ns("cancelbutton"),
                  class = "col-12 text-center",
                  "Thank you for your business. I'm happy to help. What happens next is you and I have a conversation. In the next week or so, I will reach out to you."
                )
              )
            },
            error = function(err) {
              output$result <- shiny$renderUI({
                tags$pre(
                  as.character(err)
                )
              })
            }
          )
        }
      })


      shiny$observeEvent(
        input$cancel,
        {
          shiny$showModal(
            inputs$modalDialog(
              easyClose = TRUE,
              tags$div(
                class = "container",
                tags$h4("You will no longer have access to ndexr, are you sure?"),
                shiny$actionButton(ns("reallySure"), "Yes I'm sure", class = "btn-danger")
              )
            )
          )
        }
      )
      shiny$observeEvent(
        input$reallySure,
        {
          box::use(.. / gmail / gmail)
          customer_ids <- stripe$stripe_customer_ids(emails = email)
          customer_subscriptions <- postgres$tables_row_retrieve("customer_id", customer_ids$customer_id, "subscription", showNotification = FALSE, dbname = "stripe")
          con <- stripe$stripe_connection(live)
          # tryCatch(
          #   con$Subscription$delete(customer_subscriptions$subscription_id),
          #   error = function(err) shiny$showNotification(tags$pre(as.character(err)))
          # )
          tryCatch(
            postgres$tables_row_remove("subscription_id", customer_subscriptions$subscription_id, "subscription", showNotification = FALSE, dbname = "stripe"),
            error = function(err) shiny$showNotification(tags$pre(as.character(err)))
          )
          shiny$showModal(
            inputs$modalDialog(
              tags$h5("Hey, I really appreciate you trying out the service and everything. If there is something I can do to help you out, or otherwise make the experience better, I'd really love to know. I also like SNL, so enjoy the meme. <3")
            )
          )


          shiny::removeUI(
            selector = paste0(c("#", ns("cardpanel")), collapse = "")
          )

          shiny::removeUI(
            selector = paste0(c("#", ns("cancelbutton")), collapse = "")
          )
          shiny::insertUI(
            selector = paste0(c("#", ns("mainpanel")), collapse = ""),
            ui = tags$div(
              class = "col-12 text-center",
              tags$img(
                src = "./images/debbie-downer-womp-womp.gif",
                class = "img img-fluid"
              )
            )
          )
        }
      )
    }
  )
}
