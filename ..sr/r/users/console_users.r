#' @export
ui_console_users <- function(id = "console_users") {
  box::use(shiny = shiny[tags, HTML])
  box::use(.. / inputs / inputs)
  box::use(. / console_users)

  ns <- shiny$NS(id)
  tags$div(
    inputs$card_collapse(
      ns,
      title = "Members",
      body = shiny$uiOutput(ns("console_users")),
      collapsed = TRUE
    )
  )
}

#' @export
server_console_users <- function(id = "console_users") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / connections / postgres)

    box::use(mcu = . / console_users)
    box::use(. / email)
    box::use(. / console_users)
    box::use(uuid)
    box::use(dplyr)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$console_users <- shiny$renderUI({
        console_users <- postgres$table_get("console_users")
        box::use(.. / aws / cognito / cognito)
        cognito_users <- cognito$cognito_users()

        box::use(.. / users / user_data_aggregate)
        box::use(.. / users / user / user)
        box::use(purrr)
        box::use(uuid)

        # userdata <- user_data_aggregate$user_data_aggregate(raw = TRUE, live = getOption("docker", FALSE))
        emails <- cognito_users$email

        #
        output$console_users <- shiny$renderUI({
          purrr$map(
            emails, function(email) {
              id <- paste0(uuid$UUIDgenerate(), email)
              out <- user$ui_user(ns(id))
              user$server_user(id, email, cognito_users)
              out
            }
          )
        })

        email$server_email_send("email")
      })
    }
  )
}
