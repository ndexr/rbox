#' @export
ec2_instance_create <- function(ImageId = "ami-097a2df4ac947655f",
                                InstanceType = "t2.medium",
                                min = 1,
                                max = 1,
                                KeyName = Sys.getenv("AWS_PEM"),
                                SecurityGroupId = Sys.getenv("AWS_SECURITY_GROUP"),
                                InstanceStorage = 10,
                                DeviceName = "/dev/sda1",
                                user_data = NA,
                                ns_common_store_user = NULL) {
  # box::use(paws[ec2])
  box::use(. / client)
  box::use(readr[read_file])
  box::use(. / state / updateState[updateState])
  box::use(base64[encode])
  box::use(utils[browseURL])
  box::use(. / s3)
  try(aws$ec2_instance_destroy(ns_common_store_user = ns_common_store_user))
  # s3$s3_upload_proj()

  base_64_user_data <- read_file(encode("./shell/install_docker_ec2.sh"))
  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
  response <-
    ec2_backend$run_instances(
      ImageId = ImageId,
      InstanceType = InstanceType,
      MinCount = as.integer(min),
      MaxCount = as.integer(max),
      UserData = base_64_user_data,
      KeyName = KeyName,
      SecurityGroupIds = list(SecurityGroupId),
      BlockDeviceMappings = list(
        list(
          Ebs = list(
            VolumeSize = as.integer(InstanceStorage)
          ),
          DeviceName = DeviceName
        )
      )
    )
  instanceData <- list(response$Instances[[1]])
  InstanceId <- response$Instances[[1]]$InstanceId
  names(instanceData) <- InstanceId
  updateState(instanceData, "aws-ec2")
  # remote_public_ip <- getOption("domain")
  sleep_seconds <- 10
  #

  ready_for_association <- FALSE
  while (isFALSE(ready_for_association)) {
    ready_for_association <- tryCatch(
      "running" == ec2_backend$describe_instance_status(
        InstanceId,
        ns_common_store_user
      )[[1]][[1]]$InstanceState$Name,
      error = function(err) {
        Sys.sleep(1)
        FALSE
      }
    )
  }
  ec2_backend$associate_address(
    InstanceId = InstanceId,
    PublicIp = getOption("ec2.nginx.conf")
  )
  system("pkill chrome")
  browseURL("https://us-east-2.console.aws.amazon.com/ec2/")
  browseURL(getOption("domain"))
  response
}

#' @export
ec2_instance_destroy <- function(ns_common_store_user = NULL) {
  box::use(. / connections / storr)
  box::use(paws[ec2])
  ec2 <- ec2()
  con <- storr$connection_storr()
  ids <- con$get("aws-ec2")
  lapply(ids, function(x) {
    try(ec2_backend$terminate_instances(x$InstanceId, ns_common_store_user = ns_common_store_user))
  })
  con$del("aws-ec2")
}

#' @export
ec2_instance_stop <- function(ns_common_store_user = NULL) {
  box::use(. / connections / storr)
  box::use(paws[ec2])
  client <- box::use(. / aws / app / box / client)
  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
  con <- storr$connection_storr()
  ids <- con$get("aws-ec2")
  lapply(ids, function(x) {
    try(ec2_backend$stop_instances(x$InstanceId, ns_common_store_user))
  })
}

#' @export
ec2_instance_start <- function(ns_common_store_user = NULL) {
  box::use(. / connections / storr)
  box::use(paws[ec2])
  box::use(. / aws / app / box / client)
  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)
  con <- storr$connection_storr()
  ids <- con$get("aws-ec2")
  lapply(ids, function(x) {
    try(ec2_backend$start_instances(x$InstanceId, ns_common_store_user))
  })
}


#' @export
ec2_list_elastic_ip <- function(ns_common_store_user = NULL) {
  box::use(.. / client)
  box::use(purrr)
  box::use(. / ec2_backend)
  # ec2_backend$allocate_address(Domain='vpc')
  # ec2_backend$associate_address()
  # ec2_backend$describe_addresses()

  client_ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)

  ipaddresses <- client_ec2$describe_addresses()
  elastic_ips <- purrr$map_dfr(
    ipaddresses$Addresses,
    function(x) {
      data.frame(
        InstanceId = ec2_backend$if_is_null(x$InstanceId),
        PublicIp = ec2_backend$if_is_null(x$PublicIp),
        Domain = ec2_backend$if_is_null(x$Domain),
        NetworkBorderGroup = ec2_backend$if_is_null(x$NetworkBorderGroup),
        AllocationId = ec2_backend$if_is_null(x$AllocationId)
      )
    }
  )

  # elastic_ips <- elastic_ips[is.na(elastic_ips$InstanceId),]$PublicIp

  # if (length(elastic_ips)) {
  #   # shiny$showNotification("Dropping elastic ip")
  #   purrr$walk(elastic_ips, function(x) {
  #     client_ec2$release_address(PublicIp=x)
  #   })
  # }

  elastic_ips
}

#' @export
ec2_allocate_elastic_ip <- function(ns_common_store_user = NULL) {
  box::use(.. / client)
  box::use(purrr)
  box::use(. / ec2_backend)

  ec2 <- client$client("ec2")

  elastic_ips <- ec2$allocate_address(Domain = "vpc")
  elastic_ips
}

#' @export
ec2_associate_elastic_ip <- function(ns_common_store_user = NULL, InstanceId, AllocationId) {
  box::use(.. / client)
  box::use(purrr)
  box::use(. / ec2_backend)

  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)

  elastic_ips <- ec2$associate_address(AllocationId = AllocationId, InstanceId = InstanceId)
  elastic_ips
}

#' @export
ec2_release_elastic_ip <- function(ns_common_store_user = NULL, AllocationId) {
  box::use(.. / client)
  box::use(purrr)
  box::use(. / ec2_backend)

  ec2 <- client$client("ec2", ns_common_store_user = ns_common_store_user)

  ec2$release_address(AllocationId = AllocationId)
}

#' @export
route53_list_hosted_zones <- function(ns_common_store_user = NULL) {
  box::use(.. / client)
  box::use(. / ec2_backend)
  box::use(purrr)

  route53 <- client$client("route53", ns_common_store_user = ns_common_store_user)

  hosted_zones <- route53$list_hosted_zones()

  purrr$map_dfr(
    hosted_zones$HostedZones,
    function(x) {
      # print(x)
      data.frame(
        Id = ec2_backend$if_is_null(x$Id),
        Name = ec2_backend$if_is_null(x$Name),
        CallerReference = ec2_backend$if_is_null(x$CallerReference),
        ConfigComment = ec2_backend$if_is_null(x$Config$Comment),
        ConfigPrivateZone = ec2_backend$if_is_null(x$Config$PrivateZone)
      )
    }
  )
}

#' @export
if_is_null <- function(x) {
  if (is.null(x)) {
    return(NA_character_)
  }
  x
}

#' @export
route53_update_hosted_zones <- function(ns_common_store_user = NULL, Domain = NULL, ElasticIp = NULL, HostedZoneId = NULL) {
  box::use(.. / client)

  box::use(purrr)

  route53 <- client$client("route53", ns_common_store_user = ns_common_store_user)

  purrr$map(
    Domain,
    function(x) {
      route53$change_resource_record_sets(
        ChangeBatch = list(
          "Changes" = list(
            list(
              Action = "UPSERT",
              ResourceRecordSet = list(
                Name = x,
                ResourceRecords = list(
                  list(
                    Value = ElasticIp
                  )
                ),
                TTL = 300L,
                Type = "A"
              )
            )
          ),
          Comment = "Web Server"
        ),
        HostedZoneId = HostedZoneId
      )
    }
  )
}

#' @export
ec2_elastic_ip_reset <- function(ns_common_store_user = NULL) {
  box::use(. / ec2_backend)
  box::use(purrr)
  box::use(. / instance_manage)
  box::use(.. / .. / connections / redis)
  box::use(shiny)
  ns <- shiny$NS("default")

  instances <- instance_manage$describe_instances(
    ns_common_store_user = ns_common_store_user,
    describe_instances = describe_instances
  )
  instances <- instances[instances$state == "running", ]
  elastic_ips <- ec2_backend$ec2_list_elastic_ip(ns_common_store_user = ns_common_store_user)
  purrr$walk(elastic_ips$AllocationId, ~ ec2_backend$ec2_release_elastic_ip(., ns_common_store_user = ns_common_store_user))

  ec2_backend$ec2_allocate_elastic_ip()
  elastic_ips <- ec2_backend$ec2_list_elastic_ip()

  ec2_backend$ec2_associate_elastic_ip(
    InstanceId = instances$InstanceId[1],
    elastic_ips$AllocationId[1]
  )

  hosted_zones <- ec2_backend$route53_list_hosted_zones()

  ec2_backend$route53_update_hosted_zones(
    Domain = c("www.ndexr.com", "ndexr.com"),
    HostedZoneId = hosted_zones[hosted_zones$Name == "ndexr.com.", ]$Id,
    ElasticIp = elastic_ips$PublicIp[1]
  )

  elastic_ips <- ec2_backend$ec2_list_elastic_ip()
  elastic_ips
}
