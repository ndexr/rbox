#' @export
textInput <- function(id, label = "@", value = "", type = "text") {
  box::use(shiny = shiny[tags, HTML])
  tags$div(
    class = "input-group mb-3",
    tags$span(
      class = "input-group-text",
      `for` = id,
      # id = id,
      label
    ),
    tags$input(
      id = id,
      type = type,
      class = "form-control",
      placeholder = value,
      `aria-label` = value,
      `aria-describedby` = id
    )
  )
}

#' @export
textAreaInput <- function(inputId, label, value = "", width = NULL, height = NULL,
                          cols = NULL, rows = NULL, placeholder = NULL, resize = NULL) {
  box::use(shiny)
  out <- shiny$textAreaInput(
    inputId, label, value, width, height,
    cols, rows, placeholder, resize
  )
  out$attribs$class <- NULL
  out
}

#' @export
actionButton <- function(id, content, class) {
  box::use(shiny[tags])
  class <- paste0(c("btn action-button me-2 ", class))
  tags$button(
    id = id,
    type = "button",
    class = class,
    content
  )
}

#' @export
numericInput <- function(inputId, label, value, min = NA, max = NA, step = NA,
                         width = NULL) {
  box::use(shiny = shiny[tags])
  out <- shiny$numericInput(
    inputId, label, value, min, max, step,
    width = width
  )
  out$attribs$class <- NULL
  out
}

#' @export
sliderInput <- function(
    inputId, label, min, max, value, step = NULL, round = FALSE,
    ticks = TRUE, animate = FALSE, width = NULL, sep = ",", pre = NULL,
    post = NULL, timeFormat = NULL, timezone = NULL, dragRange = TRUE) {
  box::use(shiny = shiny[tags])

  out <- shiny$sliderInput(
    inputId, label, min, max, value, step, round,
    ticks, animate, width, sep, pre,
    post, timeFormat, timezone, dragRange
  )
  out$attribs$class <- NULL
  out
}

#' @export
card_collapse <- function(ns,
                          title = "",
                          footer = NULL,
                          body = NULL,
                          sidebar = NULL,
                          status = NULL,
                          collapsible = TRUE,
                          collapsed = TRUE,
                          closable = FALSE,
                          maximizable = TRUE,
                          refresh = FALSE) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(glue)
    box::use(bs4Dash)
    box::use(. / inputs)
  }

  # if (length(title)==1) {
  #   if (title == "") title <- "Click + to Open"
  # }

  hash <- function(x) paste0("#", x)
  uuid <- uuid::UUIDgenerate()
  inputs$bs4_card(
    body,
    id = ns(uuid), sidebar = sidebar, status = status,
    collapsible = collapsible, collapsed = collapsed, closable = closable,
    maximizable = maximizable,
    title = title, footer = footer
  )
}




#' @export
bs4_card <-
  function(..., title = NULL,
           footer = NULL,
           status = "secondary",
           solidHeader = FALSE,
           background = NULL,
           height = NULL,
           collapsible = TRUE,
           collapsed = FALSE,
           closable = FALSE,
           maximizable = FALSE,
           icon = NULL,
           gradient = FALSE,
           boxToolSize = "sm",
           elevation = 2,
           headerBorder = TRUE,
           label = NULL,
           dropdownMenu = NULL,
           sidebar = NULL,
           id = NULL) {
    if (is.null(status)) {
      solidHeader <- TRUE
    }
    box::use(. / inputs)
    box::use(shiny = shiny[tags])

    nullsData <- list(
      # title = title,
      status = status,
      solidHeader = solidHeader,
      background = background,
      # width = width,
      height = height,
      collapsible = collapsible,
      closable = closable,
      maximizable = maximizable,
      gradient = gradient
    )

    props <- bs4Dash:::dropNulls(nullsData)
    cardCl <- bs4Dash:::setBoxClass(
      status, solidHeader, collapsible, collapsed,
      elevation, gradient, background, sidebar
    )
    style <- bs4Dash:::setBoxStyle(height, sidebar)
    cardToolTag <- NULL
    cardToolTag <- shiny::tags$div()
    cardToolTag <- shiny::tagAppendChildren(
      cardToolTag, label,
      inputs$createBoxTools(
        collapsible,
        collapsed, closable,
        maximizable,
        sidebar,
        dropdownMenu,
        boxToolSize,
        status,
        background,
        solidHeader
      )
    )
    # cardToolTag
    cardToolTag <- shiny::tagAppendAttributes(cardToolTag, class = "d-flex justify-content-end")
    headerTag <- shiny::tags$h5(title)

    headerTag <- tags$div(
      class = "row ",
      tags$div(
        class = "col-10",
        headerTag
      ),
      tags$div(
        class = "col-2",
        cardToolTag
      )
    )
    bodyTag <- shiny::tags$div(class = "card-body container-fluid", style = style, ...)
    footerTag <- if (!is.null(footer)) {
      shiny::tags$div(class = "card-footer", footer)
    }
    cardTag <- shiny::tags$div(class = cardCl, id = id)
    cardTag <- shiny::tagAppendChildren(
      cardTag, tags$div(
        headerTag
      ), bodyTag,
      footerTag
    )
    shiny::tags$div(
      class = "my-1",
      cardTag,
      shiny::tags$script(
        type = "application/json",
        `data-for` = id,
        jsonlite::toJSON(
          x = props,
          auto_unbox = TRUE,
          json_verbatim = TRUE
        )
      )
    )
  }

#' @export
selectizeInput <- function(inputId, ..., options = NULL, width = NULL) {
  box::use(shiny)
  out <- shiny$selectizeInput(
    inputId, ...,
    options = options, width = width
  )
  out$attribs$class <- NULL
  out
}

#' @export
textInput <- function(inputId, label, value = "", width = NULL, placeholder = NULL) {
  box::use(shiny)
  out <- shiny$textInput(inputId, label, value, width, placeholder)
  out$attribs$class <- NULL
  out
}

#' @export
passwordInput <- function(inputId, label, value = "", width = NULL, placeholder = NULL) {
  box::use(shiny)
  out <- shiny$passwordInput(inputId, label, value, width, placeholder)
  out$attribs$class <- NULL
  out
}


#' @export
modalDialog <- function(..., title = NULL,
                        footer = shiny::modalButton("Dismiss"),
                        size = c("m", "s", "l", "xl"),
                        easyClose = TRUE,
                        fade = TRUE) {
  box::use(shiny = shiny[tags])
  size <- match.arg(size)
  tags$div(
    id = "shiny-modal",
    class = "modal",
    tabindex = "-1",
    `data-backdrop` = "static",
    `data-bs-backdrop` = "static",
    `data-keyboard` = "true",
    `data-bs-keyboard` = "true",
    tags$div(
      class = "modal-dialog",
      class = switch(size,
        s = "modal-sm",
        m = NULL,
        l = "modal-lg",
        xl = "modal-xl"
      ), tags$div(
        class = "modal-content",
        if (!is.null(title)) {
          tags$div(class = "modal-header", tags$h4(
            class = "modal-title",
            title
          ))
        }, tags$div(class = "modal-body", ...),
        if (!is.null(footer)) {
          tags$div(class = "modal-footer bg-dark", footer)
        }
      )
    ),
    tags$script(shiny$HTML("if (window.bootstrap && !window.bootstrap.Modal.VERSION.match(/^4\\./)) {\n         var modal = new bootstrap.Modal(document.getElementById('shiny-modal'));\n         modal.show();\n      } else {\n         $('#shiny-modal').modal().focus();\n      }"))
  )
}

#' @export
createBoxTools <- function(collapsible, collapsed, closable, maximizable, sidebar,
                           dropdownMenu, boxToolSize, status, background, solidHeader) {
  validStatuses <- c("primary", "secondary", "info", "success", "warning", "danger")
  box::use(glue)
  status <- if (is.null(status)) status <- "secondary"
  btnClass <- glue$glue("btn rounded btn-{status} fa fa-lg")

  collapseTag <- NULL
  if (collapsible) {
    collapseTag <- shiny::tags$button(
      class = btnClass, type = "button",
      `data-card-widget` = "collapse",
      shiny::icon("bars", class = "fa-md")
    )
  }
  closableTag <- NULL
  if (closable) {
    closableTag <- shiny::tags$button(
      class = btnClass, `data-card-widget` = "remove",
      type = "button", shiny::icon("xmark", class = "fa-md")
    )
  }
  maximizableTag <- NULL
  if (maximizable) {
    maximizableTag <- shiny::tags$button(
      type = "button",
      class = btnClass, `data-card-widget` = "maximize",
      shiny::icon("up-right-and-down-left-from-center", class = "fa-md")
    )
  }
  sidebarToolTag <- NULL
  if (!is.null(sidebar)) {
    sidebar[[1]]$attribs$class <- btnClass
    sidebarToolTag <- sidebar[[1]]
  }
  dropdownMenuToolTag <- NULL
  if (!is.null(dropdownMenu)) {
    dropdownMenu$children[[1]]$attribs$class <- paste0(
      btnClass,
      " dropdown-toggle"
    )
    dropdownMenuToolTag <- dropdownMenu
  }
  bs4Dash:::dropNulls(list(
    dropdownMenuToolTag, closableTag,
    maximizableTag, sidebarToolTag, collapseTag
  ))
}


#' @export
title_h5 <- function(text) {
  box::use(shiny = shiny[tags])
  tags$div(
    class = "mb-3",
    tags$h5(text, class = "display-5"),
    tags$div(class = "p-1 bg-secondary rounded")
  )
}
