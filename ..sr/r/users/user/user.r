#' @export
ui_user <- function(id = "user") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_user <- function(id = "user", email, userdata) {
  box::use(shiny = shiny[tags])
  box::use(jsonlite)
  box::use(glue)
  box::use(.. / .. / inputs / inputs)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      output$ui <- shiny$renderUI({
        tags$div(
          class = "row border p-2",
          tags$div(
            class = "col-12",
            email
          ),
          tags$div(
            class = "col-12",
            shiny$dataTableOutput(ns("transactionTable"))
          ),
          tags$div(
            class = "col-12 d-flex align-items-center justify-content-start",
            shiny$actionButton(ns("showDetails"), "Review Transactions", class = "btn-primary")
          )
        )
      })


      # stripeData <- shiny$reactive({
      #   stripe <- userdata$stripe
      #   stripe <- stripe[stripe$email == email, ]
      # })

      # output$userMonitor <- shiny$renderUI({
      #   # n_payments <- nrow(stripeData())
      #   tags$b(
      #     glue$glue("Transactions: {n_payments}")
      #   )
      # })

      shiny$observeEvent(input$showDetails, {
        
        output$transactionTable <- shiny$renderDataTable({
          userdata[userdata$email == email, ]
        })
      })
    }
  )
}
