#' @export
ui_template <- function(id = "template") {
  box::use(shiny[withTags])
  box::use(shiny)
  ns <- shiny$NS(id)
  withTags(
    section(
      class = "page-section", id = "template",
      div(
        class = "",
        shiny$plotOutput(ns("plot"))
      )
    )
  )
}

#' @export
server_template <- function(id = "template") {
  box::use(shiny)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      output$plot <- shiny$renderPlot(
        plot(datasets::mtcars)
      )
    }
  )
}
