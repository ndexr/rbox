#' @export
ui_aws <- function(id = "aws") {
  {
    box::use(shiny = shiny[tags, HTML])
  }
  ns <- shiny$NS(id)


  shiny$uiOutput(ns("awsbody"))
}



#' @export
server_aws <- function(id = "aws", credentials) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(dplyr)
    box::use(purrr)
    box::use(glue)
    box::use(shinyjs)
    box::use(utils)
    box::use(uuid)
    box::use(. / ec2 / ec2)

    box::use(. / client)
    box::use(readr[read_file])
    box::use(. / ec2 / rbox_init)
    box::use(. / ec2 / current_servers)
    box::use(.. / connections / redis)
    box::use(.. / login / user_create)
    box::use(.. / getting_started / getting_started)
    box::use(. / secrets / secrets)
    box::use(.. / users / console_users)
    box::use(.. / users / ip_address)
    box::use(.. / inputs / inputs)
    box::use(. / settings / settings)
    box::use(. / ec2 / amis)
  }

  uuid <- uuid$UUIDgenerate()

  ns_common_store_user <- shiny$NS(credentials$email)
  userId <- stringr::str_remove_all(credentials$email, "@.*")
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      output$awsbody <- shiny$renderUI({
        out <- tags$div(
          tags$div(
            class = "mb-3",
            tags$h5("R Box Manager"),
            tags$div(class = "p-1 bg-secondary rounded")
          ),
          inputs$card_collapse(
            ns,
            title = "Launch",
            body = rbox_init$ui_rbox_init(ns("rbox_init"))
          ),
          inputs$card_collapse(
            ns,
            title = "Active",
            body = current_servers$ui_current_servers(ns("current_servers"))
          ),
          inputs$card_collapse(
            ns,
            title = "Backup",
            body = amis$ui_amis(ns("amis"))
          ),
        )

        shiny$observe({
          has_secret <- secrets$secret_exists(ns_common_store_user)

          if (!has_secret) {
            shiny$invalidateLater(5000)
            shiny$showNotification("AWS Panel Waiting for AWS Credentials.")
          } else {
            shiny$showNotification("Loading AWS Panel")

            amis$server_amis("amis", ns_common_store_user, credentials)
            settings$server_aws_settings("aws_settings", ns_common_store_user)
            user_create$server_user_create("user_create")
            rbox_init$server_rbox_init("rbox_init", ns_common_store_user, credentials)

            current_servers$server_current_servers("current_servers", ns_common_store_user, credentials)
          }
        })

        out
      })
    }
  )
}
