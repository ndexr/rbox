#' @export
aws_cost_get <- function(from = NA, to = NA, ns_common_store_user = NULL) {
  {
    box::use(lubridate)
    box::use(. / client)
    box::use(purrr)
  }

  if (any(is.na(from), is.na(to))) {
    from <- as.character(lubridate$floor_date(Sys.Date(), unit = "month"))
    to <- as.character(lubridate$ceiling_date(Sys.Date(), unit = "month"))
  } else {
    from <- as.character(from)
    to <- as.character(to)
  }

  costs <- client$client("ce", ns_common_store_user)

  results <- costs$get_cost_and_usage(
    TimePeriod = list(
      Start = from,
      End = to
    ),
    Granularity = "DAILY",
    Metrics = list("UnblendedCost", "UsageQuantity", "BlendedCost")
  )


  purrr$map_df(
    results$ResultsByTime,
    function(x) {
      data.frame(
        start = x$TimePeriod$Start,
        unblended_cost = as.numeric(x$Total$UnblendedCost$Amount),
        blended_cost = as.numeric(x$Total$BlendedCost$Amount),
        usage_quantity = as.numeric(x$Total$UsageQuantity$Amount)
      )
    }
  )
}

#' @export
ui_aws_costs <- function(id = "aws_costs") {
  {
    box::use(.. / inputs / inputs)
    box::use(shiny = shiny[tags, HTML])
  }
  ns <- shiny$NS(id)
  shiny::addResourcePath(prefix = "images", directoryPath = "images")
  inputs$card_collapse(
    ns,
    "AWS Bill (month)",
    body = shiny$uiOutput(ns("costsPanel"))
  )
}

#' @export
server_aws_costs <- function(id = "aws_costs", ns_common_store_user) {
  box::use(shiny = shiny[tags, HTML])
  box::use(ggplot2)
  box::use(. / costs)

  box::use(lubridate)
  box::use(scales)
  box::use(ggthemes)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns



      output$costsPanel <- shiny$renderUI({
        shiny$plotOutput(ns("ui"), width = "100%")
      })

      output$ui <- shiny$renderPlot({
        # shiny$req(aws_costs)
        box::use(ggthemes)


        aws_costs <- tryCatch({
          aws_costs <- costs$aws_cost_get(ns_common_store_user = ns_common_store_user)
        }, error = function(err) {
          shiny$showNotification("Please visit he cost explorer in AWS to enable cost data here.")
          shiny$req(FALSE)
        })


        ggplot2$ggplot(aws_costs, ggplot2$aes(x = lubridate$day(start), y = blended_cost)) +
          ggplot2$geom_col() +
          ggplot2$xlab(label = "Day of Month") +
          ggplot2$ylab(label = "US Dollar") +
          ggplot2$scale_y_continuous(labels = scales$dollar_format()) +
          ggplot2$ggtitle(
            paste0(month.name[lubridate$month(Sys.time())], " spend to date - ", paste0("$", round(sum(aws_costs$blended_cost), 2)))
          ) +
          ggthemes$theme_economist(horizontal = TRUE, base_size = 22)
      })
    }
  )
}
