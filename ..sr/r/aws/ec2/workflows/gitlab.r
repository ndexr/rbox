#' @export
ui_gitlab <- function(id = "gitlab", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(readr)
    box::use(ssh)
    box::use(glue[glue])
    box::use(stringr)
    box::use(ssh_utils = .. / .. / .. / connections / ssh)
    box::use(.. / .. / .. / connections / redis)
    box::use(.. / .. / .. / inputs / inputs)
  }
  ns <- shiny$NS(id)
  ns_common_store_user <- ec2data$ns_common_store_user
  uuid <- uuid::UUIDgenerate()

  ec2data <- redis$get_state(ns_common_store_user(ec2data$instance$InstanceId))

  redux <- redis$get_state(ns_common_store_user("gitlab"))
  runner <- redis$setDefault(redux$runner, "myrunner")
  runner_tags <- redis$setDefault(redux$runner_tags, "prod")
  url <- redis$setDefault(redux$url, "https://gitlab.com/")

  inputs$card_collapse(
    ns = ns,
    "Gitlab Runner Config",
    body = tags$div(
      class = "well",
      shiny$textInput(ns("runner"), "Runner Name", runner),
      shiny$textInput(ns("token"), "Gitlab Runner Token"),
      shiny$textInput(ns("runner_tags"), "Tags", runner_tags),
      shiny$textInput(ns("url"), "Gitlab URL", url),
      shiny$uiOutput(ns("ui"))
    ),
    footer = tags$div(
      class = "row",
      tags$div(
        class = "col-12 d-flex justify-content-between",
        shiny$actionButton(ns("stop"), "Stop Runner", class = "btn btn-primary "),
        shiny$actionButton(ns("start"), "Start Runner", class = "btn btn-primary ")
      )
    )
  )
}

#' @export
server_gitlab <- function(id = "gitlab", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(readr)
    box::use(ssh)
    box::use(glue[glue])
    box::use(stringr)
    box::use(ssh_utils = .. / .. / .. / connections / ssh)
    box::use(.. / .. / .. / connections / redis)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      shiny$observeEvent(input$update, {
        ns_common_store_user <- ec2data$ns_common_store_user
        redis$store_state(
          ns_common_store_user("gitlab"),
          shiny$reactiveValuesToList(input)
        )
      })




      shiny$observeEvent(
        input$start,
        {
          shiny$req(input$runner_tags)
          shiny$req(input$token)
          shiny$req(input$url)

          shiny$showNotification("Starting Runner")
          ns_common_store_user <- ec2data$ns_common_store_user

          ec2data <- redis$get_state(ns_common_store_user(ec2data$instance$InstanceId))

          if (!grepl("gitlab-runner", runnerStatus())) {
            startRunner <- ssh_utils$ssh_execute(ec2data, "docker run -d --name gitlab-runner --restart always -v /etc/gitlab-runner:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest")
            shiny$showNotification("Runner started")
          } else {
            ssh_utils$ssh_execute(ec2data, "docker container start gitlab-runner")
            shiny$showNotification("Runner already running")
          }

          shiny$showNotification("Configuring Runner")

          ns_common_store_user <- ec2data$ns_common_store_user

          ec2data <- redis$get_state(ns_common_store_user(ec2data$instance$InstanceId))


          output$ui <- shiny$renderUI({
            cmd <- glue(
              'docker exec gitlab-runner gitlab-runner register --non-interactive --url "{input$url}" --registration-token "{input$token}" --executor "docker" --docker-image docker:latest --description "{input$runner}" --maintenance-note "Free-form maintainer notes about this runner" --tag-list "{input$runner_tags}" --run-untagged="true" --locked="false" --access-level="not_protected"'
            )

            ssh_utils$ssh_execute(
              ec2data,
              "sudo rm -rf /etc/gitlab-runner/config.toml"
            )

            out <- ssh_utils$ssh_execute(ec2data, cmd)

            config_toml <- ssh_utils$ssh_execute(
              ec2data,
              "sudo cat /etc/gitlab-runner/config.toml"
            )[[2]]


            config_toml <- stringr$str_replace_all(
              config_toml,
              '\\["\\/cache"\\]',
              '["/var/run/docker.sock:/var/run/docker.sock", "/cache"]'
            )

            config_toml <- stringr$str_replace_all(
              config_toml,
              "privileged = false", "privileged = true"
            )

            config_path <- tempfile(fileext = "/config.toml")
            dir.create(fs::path_dir(config_path))
            readr$write_file(config_toml, config_path)
            ssh_utils$ssh_upload_file(ec2data, config_path, "/etc/gitlab-runner")
            tags$div(
              class = "row",
              tags$div(
                class = "col-12",
                lapply(
                  ssh_utils$ssh_response_parse(out),
                  function(x) {
                    tags$pre(
                      class = "p-3",
                      tags$code(
                        x
                      )
                    )
                  }
                )
              )
            )
          })
        }
      )

      runnerStatus <- shiny$reactive({
        ns_common_store_user <- ec2data$ns_common_store_user
        paste0(ssh_utils$ssh_execute(ec2data, "docker container ls -a"), collapse = "\n")
      })

      shiny$observeEvent(
        input$stop,
        {
          shiny$showNotification("Stopping Runner")
          ns_common_store_user <- ec2data$ns_common_store_user

          ec2data <- redis$get_state(ns_common_store_user(ec2data$instance$InstanceId))

          if (grepl("gitlab-runner", runnerStatus())) {
            startRunner <- ssh_utils$ssh_execute(ec2data, "docker container stop gitlab-runner")
            shiny$showNotification("Runner stopped")
          } else {
            shiny$showNotification("Runner already stoped")
          }
        }
      )
    }
  )
}
