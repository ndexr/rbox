#' @export
ui_signin_container <- function(heading = "", subheading = "", ...) {
  box::use(shiny[withTags])
  withTags(
    section(
      class = "page-section", id = "signin",
      div(
        class = "",
        div(
          class = "text-center",
          h2(class = "section-heading text-uppercase ", heading),
          h3(class = "section-subheading text-muted ", subheading)
        ),
        ...
      )
    )
  )
}


#' @export
ui_signin <- function(id = "signin") {
  {
    box::use(shiny = shiny[withTags])
    box::use(shinyjs)
  }
  ns <- shiny$NS(id)
  withTags(
    div(
      form(
        id = "signinForm",
        div(
          class = "row align-items-stretch",
          div(
            class = "col-md-6",
            div(
              class = "  mb-5",
              shiny$h4("Sign In", class = "text-center"),
              shinyjs$disabled(
                div(
                  class = "form-group",
                  shiny$tags$input(class = "form-control", id = ns("username"), type = "username", placeholder = "Your Username")
                )
              ),
              shinyjs$disabled(
                div(
                  class = "form-group",
                  shiny$tags$input(class = "form-control", id = ns("password"), type = "password", placeholder = "Your Password")
                )
              )
            ),
            div(
              class = "text-center my-5",
              shinyjs$disabled(
                shiny$tags$button(
                  class = "btn  btn-lg text-uppercase action-button",
                  id = ns("signin"), type = "submit", "Sign In"
                )
              )
            )
          ),
          div(
            class = "col-md-6",
            shiny$h4("Get Inside", class = "text-center "),
            div(
              class = " ",
              div(
                class = "form-group",
                shinyjs$disabled(
                  shiny$tags$input(
                    class = "form-control",
                    id = ns("testusername"),
                    type = "testusername",
                    placeholder = "Pick a Username"
                  )
                )
              ),
              div(
                class = "form-group",
                shinyjs$disabled(
                  shiny$tags$input(
                    class = "form-control",
                    id = ns("testemail"),
                    type = "testemail",
                    placeholder = "Your Email"
                  )
                )
              ),
              div(
                class = "form-group",
                shinyjs$disabled(
                  shiny$tags$input(class = "form-control", id = ns("token"), type = "password", placeholder = "Special Access Token (optional)")
                )
              )
            ),
            div(
              class = "text-center mt-5",
              shinyjs$disabled(
                shiny$tags$button(
                  class = "btn  btn-lg text-uppercase action-button",
                  id = ns("signup"), type = "submit", "Submit"
                )
              )
            )
          )
        )
      )
    )
  )
}


#' @export
server_signin <- function(id = "signin") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(jsonlite)
    box::use(shinyvalidate)
    box::use(shinyjs[js])
    box::use(glue[glue])
    box::use(jsonlite)
    box::use(purrr)
  }
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      isLoggedIn <- shiny$reactive({
        session$sendCustomMessage("cookie-get", list(id = ns("cookie")))
        shiny$req(length(input$cookie) > 0)
        logintime <- as.numeric(input$cookie$logintime)
        logintime <- as.POSIXct(logintime, origin = "1970-01-01")
        logged_in_minutes_ago <- difftime(Sys.time(), logintime, units = "mins")
        shiny$req(logged_in_minutes_ago < 60)
      })


      shiny$observeEvent(input$signin, {
        msg <- list(
          name = "logintime",
          value = as.numeric(Sys.time()),
          id = ns("cookie")
        )
        session$sendCustomMessage("cookie-set", msg)
      })

      isLoggedIn
    }
  )
}
