#' @export
instance_types_get <- function() {
  box::use(jsonlite)
  box::use(httr)
  box::use(fs)
  box::use(purrr)
  box::use(dplyr)
  box::use(rvest)
  box::use(janitor)
  box::use(stringr)
  box::use(stats[setNames])
  # base_url <- 'https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/us-east-1/index.json'
  # priceindex <- paste0(url=base_url, '/offers/v1.0/aws/index.json')
  # priceindex <- httr$GET(base_url)
  instance_types <- rvest$read_html("https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-types.html")
  instance_types <- rvest$html_table(instance_types)
  current_cpu <- dplyr$bind_rows(instance_types)
  current_cpu <- dplyr$filter(current_cpu, !is.na(current_cpu$Type))
  cpu_types <- stringr$str_split(current_cpu$Sizes, " [|] ")
  InstanceTypes <- setNames(cpu_types, current_cpu$Type)
  #
  # InstanceTypes$C6a
  InstanceTypes <- list(general = c(InstanceTypes$T2, InstanceTypes$T3), compute = c(InstanceTypes$C4, InstanceTypes$C5, InstanceTypes$C6a), memory = c(InstanceTypes$M4, InstanceTypes$M5))


  # current_cpu <- janitor$clean_names(current_cpu)
  # current_cpu <- current_cpu[!is.na(current_cpu$type),]
  # names() = c('instance_type', 'vcpus', 'memory_gib')
  # current_cpu <- dplyr$transmute(current_cpu, instance_type, vcpus=default_v_cp_us, memory_gib=memory_gi_b)
  # current_cpu <- dplyr$filter(current_cpu, !is.na(vcpus))
}
