#' @export
ui_notes <- function(id = "notes", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / .. / .. / inputs / inputs)
    box::use(.. / .. / .. / connections / redis)
  }
  ns <- shiny$NS(id)

  ns_common_store_user <- ec2data$ns_common_store_user
  PublicIpAddress <- ec2data$instance$PublicIpAddress
  noteid <- paste0("notes", PublicIpAddress)

  redux <- redis$get_state(ns_common_store_user(noteid))
  textbox <- redis$setDefault(redux$textbox, "enter notes here")


  inputs$card_collapse(
    ns, "Notes",
    tags$div(
      class = "row",
      shiny$uiOutput(ns("ui")),
      inputs$textAreaInput(ns("textbox"), NULL, textbox, width = "100%")
    ),
    footer = shiny$actionButton(ns("save"), "Save", class = "btn btn-primary")
  )
}

#' @export
server_notes <- function(id = "notes", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(.. / .. / .. / inputs / inputs)
    box::use(.. / .. / .. / connections / redis)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      shiny$observeEvent(input$save, {
        ns_common_store_user <- ec2data$ns_common_store_user
        PublicIpAddress <- ec2data$instance$PublicIpAddress
        noteid <- paste0("notes", PublicIpAddress)
        redis$store_state(ns_common_store_user(noteid), shiny$reactiveValuesToList(input))
        shiny$showNotification("Note stored")
      })
    }
  )
}
