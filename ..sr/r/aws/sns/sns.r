#' @export
sns_send <- function(ns_common_store_user,
                     PhoneNumber = 2549318313, Message = "test run", countryCode = "+1") {
  box::use(glue)
  box::use(.. / client)
  sns <- client$client("sns", ns_common_store_user)
  sns$publish(
    PhoneNumber = paste0(countryCode = countryCode, PhoneNumber, collapse = ""),
    Message = Message
  )
}
