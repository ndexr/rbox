#' @export
ui_box_updater <- function(id = "box_updater") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_box_updater <- function(id = "box_updater") {
  box::use(shiny = shiny[tags])
  box::use(fs)
  box::use(. / boxupdater)
  box::use(purrr)
  box::use(dplyr)
  box::use(readr)
  box::use(stringr)
  box::use(glue)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      srcfiles <- fs$dir_info("/home/ndexr/rapp/services/console/src/r", regexp = "[.]r$", recurse = TRUE)
      srcfiles <- dplyr$transmute(
        srcfiles,
        path = fs$path_real(path),
        filetext = purrr$map_chr(path, function(x) {
          filetext <- readr$read_file(x)
        })
      )
      srcfiles <- purrr$map_dfr(
        split(srcfiles, 1:nrow(srcfiles)),
        function(x) {
          box_declarations <- stringr$str_extract_all(
            x$filetext,
            "box::.*"
          )[[1]]

          #
          declarations <- data.frame(
            box_declarations = box_declarations,
            has_parent = stringr$str_count(box_declarations, ".. /"),
            has_root = stringr$str_count(box_declarations, "\\(. /")
          )

          declarations <- declarations |>
            dplyr$group_by(box_declarations) |>
            dplyr$mutate(
              # declarations,
              box_declarations = dplyr$case_when(
                sum(has_parent, has_root) > 0 ~ stringr$str_remove_all(box_declarations, "box::use\\("),
                TRUE ~ box_declarations
              ),
              box_declarations = dplyr$case_when(
                sum(has_parent, has_root) > 0 ~ stringr$str_remove_all(box_declarations, "\\)"),
                TRUE ~ box_declarations
              ),
              contains_relative = (has_parent + has_root) > 0
            )

          x <- dplyr$bind_cols(x, declarations)
        }
      )

      srcfiles <-
        srcfiles |>
        dplyr$filter(contains_relative)

      output$ui <- shiny$renderUI({
        purrr$map(
          split(srcfiles, srcfiles$path),
          function(x) {
            if (nrow(x)) {
              uuid <- uuid::UUIDgenerate()
              out <- boxupdater$ui_box_updater(ns(uuid))
              boxupdater$server_boxupdater_move_and_update_file(uuid, file = x, files = srcfiles)
              tags$div(
                x$path[[1]],
                out
              )
            }
          }
        )
      })
    }
  )
}

#' @export
ui_boxupdater_move_and_update_file <- function(id = "boxupdater_move_and_update_file") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_boxupdater_move_and_update_file <- function(id = "boxupdater_move_and_update_file", file, files) {
  box::use(shiny = shiny[tags])
  box::use(purrr)
  box::use(readr)
  box::use(stringr)
  box::use(fs)
  box::use(dplyr)
  box::use(glue)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      data <- shiny$reactive({
        out <- dplyr$mutate(
          file,
          path,
          filetext,
          dirs = fs$path_dir(path),
          fname = fs$path_file(path),
          # imports = as.character(fs$path_rel(box_declarations, dirs[[1]])),
          imports = paste0(fs$path_abs(fs$path_rel(paste0(dirs, "/", box_declarations))), ".r"),
          imports = stringr$str_remove_all(imports, " "),
          imports_real = fs$path_real(imports),
          path_exists = fs$file_exists(imports),
          ref_r_path = stringr$str_remove_all(paste0(box_declarations, ".r"), " ")
          # rel_path <- fs$path_rel(box_declarations, dirs)
        )

        moveTo <- unique(fs$path_rel(fs$path_dir(files$path), getwd()))

        list(data = out, moveTo = moveTo)
      })

      output$ui <- shiny$renderUI({
        moveTo <- data()$moveTo

        tags$div(
          class = "row",
          tags$div(
            class = "col-12 d-flex justify-content-around",
            shiny$selectizeInput(ns("moveTo"), NULL, c(files$path)),
            shiny$selectizeInput(ns("moveTo"), NULL, c(moveTo))
          ),
          tags$div(class = "col-12", {
            shiny$uiOutput(ns("individualImports"))
          })
        )
      })

      output$individualImports <- shiny$renderUI({
        shiny$req(input$moveTo)

        data <- data()$data

        data$dir_new <- paste0(c(getwd(), input$moveTo), collapse = "/")
        data$dir_new <- fs$path_real(data$dir_new)
        data$fname_new <- paste0(c(data$dir_new, data$fname), collapse = "/")


        tags$div(
          class = "container bg-light p-4 border-top",
          tags$div(
            class = "row",
            tags$div(
              class = "col-12",
              tags$table(
                class = "table",
                tags$thead(
                  tags$th(scope = "col", "Path"),
                  tags$th(scope = "col", "Move To"),
                  tags$th(scope = "col", "Imports")
                ),
                purrr$map(
                  split(data, 1:length(data$path)),
                  function(x) {
                    box::use(. / boxupdater)
                    uuid <- uuid::UUIDgenerate()
                    x$new_declaration <- fs::path_rel(x$imports, x$dir_new)
                    x$new_declaration <- styler::style_text(x$new_declaration)
                    out <- boxupdater$ui_line_refactor(ns(uuid))
                    boxupdater$server_line_refactor(uuid, x, input$moveTo, files)
                    out
                  }
                )
              )
              # shiny$uiOutput(ns('refactorInfo'))
            )
          )
        )
      })
    }
  )
}

#' @export
ui_line_refactor <- function(id = "line_refactor") {
  box::use(shiny = shiny[tags])
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"), container = function(...) tags$tr(...))
}

#' @export
server_line_refactor <- function(id = "line_refactor", x, moveTo, files) {
  box::use(shiny = shiny[tags])
  box::use(glue)
  box::use(purrr)
  box::use(dplyr)
  box::use(fs)
  box::use(dplyr)
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns

      output$ui <- shiny$renderUI({
        shiny::tagList(
          tags$td(
            fs$path_rel(x$path)
          ),
          tags$td(
            moveTo
          ),
          tags$td(
            x$imports
          )
        )
      })

      # output$refactorInfo <- shiny$renderUI({

      #
      #   tags$div(
      #     tags$p(
      #       "Moving", tags$code(x$fname), "to", tags$code(x$fname_new)
      #     ),
      #     tags$p(
      #       "Updating ", tags$code(x$box_declarations), "to", tags$code(x$new_declaration)
      #     )
      #   )
      # })
    }
  )
}

ui <- function() {
  box::use(shiny = shiny[tags])
  box::use(. / boxupdater)
  box::reload(boxupdater)
  shiny$fluidPage(
    boxupdater$ui_box_updater()
  )
}
server <- function(input, output, session) {
  box::use(shiny = shiny[tags])
  box::use(. / boxupdater)
  box::reload(boxupdater)
  boxupdater$server_box_updater()
}

box::use(shiny)

# options(shiny.launch.browser = .rs.invokeShinyPaneViewer)
shiny$shinyApp(ui, server)
