#' @export
ui_cognito <- function(id) {
  box::use(cognitoR)
  box::use(shiny)
  box::use(shinyjs)
  ns <- shiny$NS(id)
  shiny$fluidRow(
    shiny$uiOutput(ns("who"), inline = TRUE),
    # shiny$actionButton(ns("logout"), "Log Out", class = 'btn-warning'),
    # class = "user-logged",
    cognitoR$cognito_ui(ns("cognito")),
    shiny$uiOutput(ns("content")),
    cognitoR$logout_ui(ns("logout"))
  )
}

#' @export
server_cognito <- function(id) {
  box::use(shiny)
  box::use(cognitoR)

  shiny$moduleServer(
    id,
    function(input, output, session) {
      cognitomod <- shiny$callModule(cognitoR$cognito_server, "cognito")

      logoutmod <- shiny$callModule(
        cognitoR$logout_server,
        "logout",
        shiny$reactive(cognitomod$isLogged),
        sprintf("You are logged in as '%s'", cognitomod$userdata$email)
      )

      shiny$observeEvent(logoutmod(), {
        cognitomod$logout()
      })

      shiny$observeEvent(cognitomod$isLogged, {
        if (cognitomod$isLogged) {
          userdata <- cognitomod$userdata
          output$content <- shiny$renderUI({
            shiny$tags$p(paste("User: ", unlist(userdata$username)))
          })
        }
      })

      cognitomod
    }
  )
}
