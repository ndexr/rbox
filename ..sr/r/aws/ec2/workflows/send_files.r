#' @export
ui_send_files <- function(id = "send_files", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
  }
  ns <- shiny$NS(id)
  ns_common_store_user <- ec2data$ns_common_store_user

  shiny$uiOutput(ns("ui"), container = function(...) tags$div(class = "col-12", ...))
}

#' @export
server_send_files <- function(id = "send_files", ec2data) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(ssh)
    box::use(fs)
    box::use(purrr)
    box::use(.. / .. / .. / connections / redis)
    box::use(glue)
    box::use(ssh_utils = .. / .. / .. / connections / ssh)
    box::use(.. / .. / .. / inputs / inputs)
  }

  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      shiny$observeEvent(
        input$sendFiles,
        {
          ns_common_store_user <- ec2data$ns_common_store_user

          ec2data <- redis$get_state(ns_common_store_user(ec2data$instance$InstanceId))


          output$ui <- shiny$renderUI({
            tags$div(
              class = "row",
              inputs$card_collapse(
                ns, "Send Files",
                body = tags$div(
                  class = "",
                  shiny$selectizeInput(
                    ns("filesToSend"),
                    label = "Send Files",
                    choices = c(
                      fs$dir_ls("./shared", recurse = T, type = "file")
                    ),
                    options = list(create = TRUE), multiple = TRUE
                  )
                ),
                footer = shiny$actionButton(ns("sendFiles"), "Send", class = "btn btn-primary")
              )
            )
          })


          tryCatch(
            {
              box::use(ssh_utils = .. / .. / .. / connections / ssh)
              filesToSend <- sub("^[.]", "", input$filesToSend)
              filesToSend <- paste0("/home/ubuntu", filesToSend)
              dirsToCreate <- paste(unique(fs$path_dir(filesToSend)))

              ssh_utils$ssh_execute(ec2data = ec2data, command = paste("mkdir -p", dirsToCreate))

              #
              purrr$walk2(
                input$filesToSend, filesToSend,
                function(x, y) {
                  filename <- fs::path_file(y)
                  shiny$showModal(inputs$modalDialog(
                    glue$glue("File stored at {y}")
                  ))
                  ssh_utils$ssh_upload_file(ec2data = ec2data, from = x, to = y)
                }
              )

              shiny$showNotification("Files transferred")
            },
            error = function(err) {
              shiny$showNotification(tags$div(
                class = "row",
                tags$div(
                  class = "col-12",
                  tags$p(
                    as.character(err)
                  )
                )
              ))

              shiny$req(FALSE)
            }
          )
        }
      )
    }
  )
}
