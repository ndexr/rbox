#' @export
make_router <- function(default, ..., page_404 = shiny::tags$h1("")) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(. / router_internal)
    box::use(. / route)
  }

  routes <- c(default, ...)
  root <- names(default)[1]
  routes <- c(routes, route$route(404, page_404))
  router <- list(root = root, routes = routes)
  list(
    ui = router_internal$router_ui_internal(router),
    server = router_internal$router_server_internal(router)
  )
}

#' @export
router_server_internal <- function(router) {
  box::use(. / router_internal)
  router_internal$create_router_callback(router$root, router$routes)
}
#' @export
create_router_callback <- function(default, ..., page_404 = shiny::tags$h1("")) {
  {
    box::use(. / router_internal)
    box::use(. / route)
  }

  # route$route()
  routes <- c(default, ...)
  root <- names(default)[1]
  if (!PAGE_404_ROUTE %in% names(routes)) {
    routes <- c(routes, route$route(PAGE_404_ROUTE, page_404))
  }

  router <- list(root = root, routes = routes)




  list(
    ui = router_internal$router_ui_internal(router),
    server = router_internal$router_server_internal(router)
  )
}


#' @export
router_ui_internal <- function(router) {
  {
    box::use(. / router_internal)
    box::use(shiny)
  }
  #

  js_file <- file.path("javascript", "shiny.router.js")
  css_file <- file.path("css", "shiny.router.css")
  out <- list(
    shiny$singleton(
      shiny$withTags(
        shiny$tags$head(
          shiny$tags$script(type = "text/javascript", src = js_file),
          shiny$tags$link(rel = "stylesheet", href = css_file)
        )
      )
    ),
    shiny$tags$div(
      id = "router-page-wrapper",
      lapply(
        router$routes, function(route) route$ui
      )
    )
  )

  out
}

#' @export
router_server_internal <- function(router) {
  box::use(. / router_internal)
  router_internal$create_router_callback(router$root, router$routes)
}

# shiny.router:::router_server_internal
#' @export
create_router_callback <- function(root, routes = NULL) {
  function(input, output, session = shiny$getDefaultReactiveDomain(), ...) {
    box::use(. / route_link)
    box::use(. / router_internal)
    box::use(shiny)
    #
    session$userData$shiny.router.page <-
      shiny$reactiveVal(list(path = root, query = NULL, unparsed = root))

    session$userData$shiny.router.url_hash <- shiny$reactiveVal("#!/")
    shiny$observeEvent(shiny$getUrlHash(), {
      session$userData$shiny.router.url_hash(route_link$cleanup_hashpath(shiny$getUrlHash()))
    })

    routes <- routes[!names(routes) == ""]


    if (!is.null(routes)) {
      lapply(routes, function(route) {
        route$server(
          input,
          output, session, ...
        )
      })
    }
    shiny$observeEvent(
      ignoreNULL = FALSE, ignoreInit = FALSE,
      eventExpr = c(router_internal$get_url_hash(session), session$clientData$url_search),
      handlerExpr = {
        new_hash <- shiny$getUrlHash(session)
        new_query <- session$clientData$url_search
        cleaned_hash <- route_link$cleanup_hashpath(new_hash)
        if (cleaned_hash == "#!/") {
          cleaned_hash <- route_link$cleanup_hashpath(root)
        }
        cleaned_url <- sprintf("%s%s", new_query, cleaned_hash)
        parsed <- router_internal$parse_url_path(cleaned_url)
        parsed$path <- ifelse(parsed$path == "", root,
          parsed$path
        )
        is_path_valid <- if (is.null(routes)) {
          !is.null(input$routes) && !(parsed$path %in%
            c(input$routes, "404"))
        } else {
          !router_internal$valid_path(routes, parsed$path)
        }
        if (is_path_valid) {
          change_page(PAGE_404_ROUTE, mode = "replace")
        } else if (new_hash != cleaned_hash) {
          change_page(cleaned_hash, mode = "replace")
        } else {
          session$userData$shiny.router.page(list(
            path = parsed$path,
            query = parsed$query, unparsed = new_hash
          ))
        }
      }
    )
    shiny$observeEvent(session$userData$shiny.router.page(), {
      page_path <- session$userData$shiny.router.page()$path
      session$sendCustomMessage("switch-ui", page_path)
    })
  }
}

#' @export
cleanup_hashpath <- function(hashpath) {
  hashpath <- hashpath[1]
  if (substr(hashpath, 1, 3) == "#!/" || substr(
    hashpath, 1,
    1
  ) == "?") {
    return(hashpath)
  }
  slicefrom <- 1
  while (substr(hashpath, slicefrom, slicefrom) %in% c(
    "#",
    "!", "/"
  )) {
    slicefrom <- slicefrom + 1
  }
  paste0("#!/", substr(hashpath, slicefrom, nchar(hashpath)))
}

#' @export
change_page <- function(page, session = shiny::getDefaultReactiveDomain(),
                        mode = "push") {
  shiny::updateQueryString(cleanup_hashpath(page), mode, session)
}

#' @export
get_url_hash <- function(session = shiny::getDefaultReactiveDomain()) {
  session$userData$shiny.router.url_hash()
}

#' @export
valid_path <- function(routes, path) {
  (path %in% names(routes))
}

#' @export
parse_url_path <- function(url_path) {
  url_query_pos <- gregexpr("?", url_path, fixed = TRUE)[[1]][1]
  url_hash_pos <- gregexpr("#", url_path, fixed = TRUE)[[1]][1]
  url_has_query <- url_query_pos[1] > -1
  url_has_hash <- url_hash_pos[1] > -1
  extracted_url_parts <- sub("^/|/$", "", strsplit(url_path,
    split = "\\?|#!|#"
  )[[1]])
  path <- ""
  query <- NULL
  if (url_has_query && url_has_hash) {
    if (url_query_pos < url_hash_pos) {
      query <- extracted_url_parts[2]
      path <- extracted_url_parts[3]
    } else {
      query <- extracted_url_parts[3]
      path <- extracted_url_parts[2]
    }
  } else if (!url_has_query && url_has_hash) {
    path <- extracted_url_parts[2]
  } else {
    query <- extracted_url_parts[2]
  }
  if (is.na(path)) {
    path <- ""
  }
  if (!is.null(query)) {
    query <- shiny::parseQueryString(query, nested = TRUE)
  }
  parsed <- list(path = path, query = query)
  parsed
}
