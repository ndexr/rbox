#' @export
ui_user_secrets <- function(id = "user_secrets") {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(dplyr)
    box::use(purrr)
    box::use(glue)
    box::use(shinyjs)
    box::use(readr[read_file])
    box::use(.. / .. / inputs / inputs)
  }

  ns <- shiny$NS(id)

  inputs$card_collapse(ns,
    "AWS",
    body = shiny$uiOutput(ns("ui")),
    footer = NULL
  )
}



#' @export
server_user_secrets <- function(id = "user_secrets", ns_common_store_user) {
  {
    box::use(shiny = shiny[tags, HTML])
    box::use(. / secrets)
    box::use(dplyr)
    box::use(purrr)
    box::use(glue)
    box::use(shinyjs)
    box::use(utils)
    box::use(jsonlite)
    box::use(. / user_secrets)
    box::use(.. / .. / inputs / inputs)
  }


  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns


      output$ui <- shiny$renderUI({
        tags$div(
          class = "row",
          tags$div(
            class = "col-12 mb-3",
            inputs$card_collapse(
              ns, "AWS Policy for ndexr",
              tags$pre('{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Action": [
				"ec2:*",
				"route53:*",
				"ce:*"
			],
			"Effect": "Allow",
			"Resource": "*"
		}
	]
}')
            )
          ),
          tags$div(
            class='col-12',
            inputs$card_collapse(
              ns, "Existing Credentials",
              tags$div(
                class='row',
                tags$div(
                  class = "col-12 mb-3",
                  tags$h5("AWS CLI Credentials"),
                  tags$div(
                    tags$label(
                      `for` = ns("AWS_ACCESS"),
                      class = "form-label",
                      "Access"
                    ),
                    tags$input(
                      type = "password",
                      id = ns("AWS_ACCESS"),
                      class = "form-control",
                      `aria-describedby` = "passwordHelpBlock"
                    ),
                    tags$div(
                      id = ns("passwordHelpBlock"),
                      class = "form-text",
                      "Create an account at AWS and go to IAM."
                    )
                  )
                ),
                tags$div(
                  class = "col-12 mb-3",
                  tags$label(
                    `for` = ns("AWS_SECRET"),
                    class = "form-label",
                    "Secret"
                  ),
                  tags$input(
                    type = "password",
                    id = ns("AWS_SECRET"),
                    class = "form-control",
                    `aria-describedby` = ns("passwordHelpBlock")
                  )
                ),
                tags$em("ndexr currently builds in us-east-2."),
                tags$div("Your credentials are retrieved from AWS secrets each time access to your account is required."),
                tags$div(
                  class = "col-12 mb-3",
                  tags$label(
                    `for` = ns("REGION"),
                    class = "form-label",
                    "Region"
                  ),
                  tags$input(
                    type = "text",
                    id = ns("REGION"),
                    class = "form-control",
                    value = "us-east-2"
                  ),
                  tags$em("default regions outside of of us-east-2 are not yet tested"),
                  tags$div(
                    class = "col-12 mb-3 my-1 d-flex justify-content-between",
                    shiny$actionButton(ns("delete"), "Delete", class = "btn btn-warning  me-1"),
                    shiny$actionButton(ns("describe"), "Describe", class = "btn btn-warning  me-1"),
                    shiny$actionButton(ns("submit"), "Submit", class = "btn btn-primary ")
                  ),
                  tags$div(
                    class = "col-12",
                    shiny$uiOutput(ns("secretsTable"))
                  )
              )
            )
          )
          )
        )
      })



      shiny$observeEvent(input$delete, {
        tryCatch(
          {
            secrets$secret_delete(ns_common_store_user("master"))
            output$secretsTable <- shiny$renderUI({
              user_secrets$awsSecretsTable(class = "row", ns_common_store_user)
            })
            shiny$showNotification("Secret Deleted")
          },
          error = function(err) {
            shiny$showNotification(tags$code(as.character(err)))
          }
        )
      })

      shiny$observeEvent(input$submit, {
        tryCatch(
          {
            SecretString <- jsonlite$toJSON(shiny$reactiveValuesToList(input))
            # SecretString$REGION <- "us-east-2"
            secrets$secret_create(ns_common_store_user("master"), SecretString = SecretString)
            shiny$showNotification("Secret Created")
            output$secretsTable <- shiny$renderUI({
              user_secrets$awsSecretsTable(class = "row", ns_common_store_user)
            })
            shiny$showNotification(paste0(ns_common_store_user("master"), " stored"))
          },
          error = function(err) {
            shiny$showNotification(tags$code(as.character(err)))
          }
        )
      })

      shiny$observeEvent(input$describe, {
        tryCatch(
          {
            output$secretsTable <- shiny$renderUI({
              user_secrets$awsSecretsTable(class = "row", ns_common_store_user)
            })
          },
          error = function(err) {
            shiny$showNotification(tags$code(as.character(err)))
          }
        )
      })
    }
  )
}


#' @export
awsSecretsTable <- function(class = "col-12 mb-3", ns_common_store_user) {
  box::use(shiny = shiny[tags, HTML])
  box::use(. / secrets)

  secret_info <- tryCatch(
    {
      secret_info <- secrets$secret_describe(ns_common_store_user("master"))
    },
    error = function(err) list()
  )


  tags$div(
    class = class,
    tags$table(
      class = "table",
      tags$tbody(
        tags$tr(
          tags$th(scope = "row", "Name"),
          tags$td(secret_info$Name)
        ),
        tags$tr(
          tags$th(scope = "row", "Created"),
          tags$td(secret_info$CreatedDate)
        ),
        tags$tr(
          tags$th(scope = "row", "Last Accessed"),
          tags$td(secret_info$LastAccessedDate)
        )
      )
    )
  )
}
