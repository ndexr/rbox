#' @export
cognito_users <- function(live = getOption("docker", FALSE)) {
  box::use(.. / client)
  cognito <- client$client("cognito-idp")
  users <- cognito$list_users(
    UserPoolId = "us-east-2_mpb00Em8J"
  )
  box::use(purrr)
  tryCatch_na <- function(x) {
    tryCatch(x, error = function(err) NA_character_)
  }
  out <- purrr$map_dfr(
    users$Users,
    function(x) {
      #
      user_df <- data.frame(
        user = x$Username,
        user_created = as.character(x$UserCreateDate),
        last_modified = as.character(x$UserLastModifiedDate),
        enabled = x$Enabled,
        status = x$UserStatus,
        email_verified = tryCatch_na(x = x$Attributes[[2]]$Value),
        phone_verified = tryCatch_na(x = x$Attributes[[3]]$Value),
        phone = tryCatch_na(x = x$Attributes[[4]]$Value),
        email = tryCatch_na(x = x$Attributes[[5]]$Value)
      )
    }
  )
  out
}


if (FALSE) {
  box::use(. / cognito)
  box::reload(cognito)
  cognito$cognito_users()
}
