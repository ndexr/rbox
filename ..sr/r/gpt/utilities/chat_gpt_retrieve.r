#' @export
chat_gpt_retrieve <- function(
    id_session_chat = NULL, username = NULL) {
  box::use(.. / .. / connections / postgres)
  box::use(dplyr)
  con <- postgres$connection_postgres()
  chatgptconvo <- dplyr$tbl(con, "chatgptconvo")
  if (!is.null(id_session_chat)) {
    chatgptconvo <- chatgptconvo |> dplyr$filter(id_session_chat == !!id_session_chat)
  }
  if (!is.null(username)) {
    chatgptconvo <- chatgptconvo |> dplyr$filter(username == !!username)
  }

  chatgptconvo <- dplyr$collect(chatgptconvo)
  chatgptconvo
}
