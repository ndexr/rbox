#' @export
ui_gpt <- function(id, ec2data = NULL) {
  box::use(shiny = shiny[tags, HTML])
  box::use(.. / inputs / inputs)
  box::use(.. / aws / ec2 / workflows / send_command)
  box::use(.. / aws / ec2 / workflows / send_files)

  ns <- shiny$NS(id)
  # out <-

  if (!is.null(ec2data)) {
    tags$div(
      class = "row",
      shiny$uiOutput(ns("ui"))
    )
  } else {
    inputs$card_collapse(
      ns,
      title = "GPT",
      body = {
        tags$div(
          tags$div(class = "p-1 bg-secondary rounded mb-3"),
          tags$div(
            class = "row",
            shiny$uiOutput(ns("ui"))
          )
        )
      }
    )
  }
}

#' @export
server_gpt <- function(id = "gpt", ns_common_store_user = ns_common_store_user, ec2data = NULL) {
  box::use(shiny = shiny[tags, HTML])
  shiny$moduleServer(
    id,
    function(input, output, session) {
      {
        box::use(waiter)
        box::use(shinyjs)
        box::use(. / utilities / utilities)
        box::use(.. / connections / postgres)
        box::use(markdown)
        box::use(waiter)
        box::use(. / utilities / chat_gpt_retrieve)
        box::use(uuid)
        box::use(glue)
        box::use(dbplyr)
        box::use(.. / inputs / inputs)
        box::use(dplyr)
        box::use(.. / aws / ec2 / workflows / send_command)
        box::use(.. / aws / ec2 / workflows / send_files)
      }



      id_session_chat <- shiny$reactive({
        input$startSession
        shiny$showNotification("Refreshed")
        id_session_chat <- uuid$UUIDgenerate()

        id_session_chat
      })

      ns <- session$ns

      output$ui <- shiny$renderUI({
        box::use(shiny = shiny[tags, HTML])
        box::use(waiter)
        box::use(shinyjs)
        box::use(.. / inputs / inputs)

        out <- shiny$div(
          class = "row",
          waiter$useWaiter(),
          shinyjs$useShinyjs(),
          tags$div(
            class = "col-md-6",
            tags$div(
              tags$div( #
                class = "mb-3",
                tags$label(
                  `for` = ns("subjectInput"),
                  class = "form-label",
                  "Subject"
                ),
                tags$input(
                  type = "text",
                  class = "form-control",
                  id = ns("subjectInput"),
                  placeholder = "Enter subject"
                )
              ),
              tags$form(
                tags$div(
                  class = "mb-3",
                  tags$label(
                    `for` = "conversation",
                    class = "form-label",
                    "Conversation:"
                  ),
                  tags$textarea(
                    id = ns("prompt"),
                    class = "form-control shiny-bound-input",
                    rows = "6"
                  )
                )
              )
            )
          ),
          tags$div(
            class = "col-md-6",
            if (!is.null(ec2data)) {
              tags$div(
                send_files$ui_send_files(ns("send_files"), ec2data),
                send_command$ui_send_command(ns("send_command"), ec2data)
              )
            }
          ),
          tags$div(
            id = ns("chat-container"),
            tags$div(
              id = ns("chat-history"),
              shiny$dataTableOutput(ns("chatThread"))
            ),
            tags$div(
              id = ns("chat-input"),
              tags$div(
                class = "row",
                tags$div(
                  class = "col-12",
                  shiny$checkboxInput(ns("markdown"), "Toggle Markdown / HTML output", TRUE),
                  shiny$checkboxInput(ns("useModal"), "Toggle Modal", TRUE)
                )
              ),
              tags$div(
                class = "row",
                tags$div(
                  class = "col-12 d-flex justify-content-between",
                  shiny$actionButton(
                    inputId = ns("startSession"),
                    label = "Refresh", class = "btn-info mb-3",
                    icon = shiny$icon("refresh")
                  ),
                  shiny$actionButton(
                    inputId = ns("history"),
                    label = "History", class = "btn-info mb-3",
                    icon = shiny$icon("table")
                  ),
                  shiny$actionButton(
                    inputId = ns("copy-btn"),
                    label = "Copy", class = "btn-primary mb-3",
                    icon = shiny$icon("copy")
                  ),
                  shiny$actionButton(
                    inputId = ns("submit"),
                    label = "Send", class = "btn-warning mb-3",
                    icon = shiny$icon("paper-plane")
                  )
                )
              ),
              tags$script({
                HTML(
                  paste0(
                    "const copyBtn = document.querySelector('#", ns("copy-btn"), "');
                      const textToCopy = document.querySelector('#", ns("chatHistory"), "');

                      copyBtn.addEventListener('click', () => {
                        navigator.clipboard.writeText(textToCopy.innerText).then(() => {
                          copyBtn.innerText = 'Copied!';
                          setTimeout(() => {
                            copyBtn.innerText = 'Copy to clipboard';
                          }, 2000);
                        });
                      });"
                  )
                )
              }),
              shiny$uiOutput(ns("chatHistory")),
              shiny$dataTableOutput(ns("chatTable")),
              tags$div(class = "mb-3")
            )
          )
        )

        out
      })


      # send_files$server_send_files("send_files", ec2data)
      inputCommand <- send_command$server_send_command("send_command", ec2data)

      chatGPT <- shiny$eventReactive(input$submit, {
        # shiny$req(id_session_chat())

        inputCommand <- tryCatch(inputCommand(), error = function(err) "")
        id_session_chat <- id_session_chat()
        username <- ns_common_store_user("gpt")
        w <- waiter$Waiter$new(
          id = ns("chat-history"),
          html = tags$img(
            src = "./images/spinners/confused-fidget-spinner-fidget-spinner.gif",
            class = "img-fluid",
            alt = "Confused Fidget Spinner"
          )
        )
        w$show()

        # inputIngest <- paste0(input$subject, )
        # Response
        prompt <- paste0(c(input$prompt, inputCommand), collapse = "\n")
        chatGPT <- utilities$chat_gpt_r(
          prompt, "gpt-3.5-turbo", id_session_chat, input$subjectInput
        )
        chatgptconvo <- data.frame(
          id_session_prompt = uuid$UUIDgenerate(),
          username = username,
          id_session_chat = id_session_chat,
          prompt = prompt,
          time = Sys.time(),
          chatGPT = chatGPT,
          label = "default",
          subject = input$subjectInput
        )

        postgres$table_append(chatgptconvo)


        w$hide()
        chatGPT
      })

      shiny$observeEvent(input$history, {
        id_session_chat <- id_session_chat()

        chatgptconvo <- chat_gpt_retrieve$chat_gpt_retrieve(id_session_chat)

        if (nrow(chatgptconvo) != 0) {
          box::use(dplyr)
          output$chatTable <- shiny$renderDataTable({
            out <- chat_gpt_retrieve$chat_gpt_retrieve(id_session_chat)
            dplyr$select(out, prompt, chatGPT)
          })
        } else {
          shiny$showNotification("No history")
        }
      })

      output$chatHistory <- shiny$renderUI({
        shiny$req(chatGPT())
        chatGPT <- chatGPT()

        if (input$useModal) {
          shiny$showModal(
            inputs$modalDialog(
              if (input$markdown) {
                shiny$HTML(markdown::mark(text = chatGPT, format = "html"))
              } else {
                shiny$HTML(chatGPT)
              }
            )
          )
        } else {
          if (input$markdown) {
            shiny$HTML(markdown::mark(text = chatGPT, format = "html"))
          } else {
            shiny$HTML(chatGPT)
          }
        }
      })
    }
  )
}
